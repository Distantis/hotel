<?
require "Includes/class.phpmailer.php";
require "Includes/class.smtp.php";




if(!function_exists("envio_mail")){
	function envio_mail($mail_to,$copy_to,$subject,$message_html,$message_alt,$esVeciOnRequest=false,$esOTSi=false,$esTouravion=false){
		require_once('Control.php');
		require_once('../Connections/db1.php');
		$mail = new Phpmailer();
		$mail->PluginDir = "Includes/";
		$mail->Mailer = "smtp";
		//$mail->IsSMTP();
		
		//Asignamos a Host el nombre de nuestro servidor smtp
		//$mail->Host = "smtp.gmail.com";
		//$mail->SMTPSecure = "ssl";
		//$mail->Host = "smtp.gmail.com";
		//$mail->Host = "smtp.googlemail.com";
		//$mail->Host = "192.168.0.12";
		$mail->Host = "mta.gtdinternet.com";
		
		//Le indicamos que el servidor smtp requiere autenticación
		$mail->SMTPAuth = TRUE;
		//$mail->Port = 465;
		
		//Le decimos cual es nuestro nombre de usuario y password
		$mail->Username = "info2@skysat.cl"; 
		$mail->Password = "info3000";
		//$mail->Username = "eallendes"; 
		//$mail->Password = "fichanueva";
		
		//Indicamos cual es nuestra dirección de correo y el nombre que 
		//queremos que vea el usuario que lee nuestro correo
		$mail->From ='info2@skysat.cl';
		if($esVeciOnRequest)
			$mail->FromName = 'Viajes el Corte Ingles';
		elseif ($esOTSi)
			$mail->FromName = 'OTSi Online';
		elseif ($esTouravion)
			$mail->FromName = 'Turavion Online';		
		else
			$mail->FromName = 'CTS-Online';
		
		//el valor por defecto 10 de Timeout es un poco escaso dado que voy a usar 
		//una cuenta gratuita, por tanto lo pongo a 30  
		$mail->Timeout=70;

		//$mail->AddAttachment($adjunto,$nombre_archivo,"base64", "application/octet-stream");
		
		//Indicamos cual es la dirección de destino del correo
		$correos = explode(";",$mail_to);
		for($i=0;$i<count($correos);$i++){
			$mail->AddAddress($correos[$i]);
		}
			
		//copia a
		if($copy_to!=''){
			//$mail->AddCC($copy_to);
			$cc = explode(";",$copy_to);
			for($i=0;$i<count($cc);$i++){
				$mail->AddAddress($cc[$i]);
			}		
		}
		//$mail->AddBCC("eallendes@kosmicmusic.com");
		//$mail->AddBCC("mmacker@kosmicmusic.com");
		//$mail->AddBCC("info@kosmicmusic.com");
		
		//Asignamos asunto y cuerpo del mensaje
		//El cuerpo del mensaje lo ponemos en formato html, haciendo 
		//que se vea en negrita
		$mail->Subject =$subject;
		
		
		$mail->Body = $message_html;
		
		//Definimos AltBody por si el destinatario del correo no admite email con formato html 
		$mail->AltBody = $message_alt;
		
		//se envia el mensaje, si no ha habido problemas 
		//la variable $exito tendra el valor true

		$exito = $mail->Send();
		
		//Si el mensaje no ha podido ser enviado se realizaran 4 intentos mas como mucho 
		//para intentar enviar el mensaje, cada intento se hara 5 segundos despues 
		//del anterior, para ello se usa la funcion sleep	
		$intentos=1; 
		while ((!$exito) && ($intentos <1)) {
		sleep(5);
			//echo " ".$mail->ErrorInfo;
			$exito = $mail->Send();
			$intentos=$intentos+1;	
		
		}

		if(!$exito)
		{
			//$ok = "No se ha podido enviar el correo electr&oacute;nico, favor comunicarse al e-mail eallendes@ingepesa.cl.".$mail->ErrorInfo;
			$ok = "no";
			ErrorPhp($db1,'0',__LINE__,$mail->ErrorInfo,$_SERVER['REQUEST_URI']);
			return $ok;
			//echo $mail->ErrorInfo;die();
		}else {
			//$ok = "El Correo Electr&oacute;ico se ha enviado con &eacute;xito.";
			$ok = "si";
			return $ok;
			//echo "mensaje enviado con exito";
			//echo $mail->ErrorInfo;die();
		}
	}
}

function envio_mail_cloud($mail_to,$copy_to,$subject,$message_html,$message_alt,$esVeciOnRequest=false,$esOTSi=false,$esTouravion=false){

    // create email headers
	// From es desde "quien" viene el email
		$from="hoteles@mail.distantis.com";
		$fromName="Servicio Hoteleria - Distantis";
		
		if($esVeciOnRequest){
			$from="veci@mail.distantis.com";
			$fromName="Viajes el Corte Ingles";
		}
		elseif ($esOTSi){
			$from="otsi@mail.distantis.com";
			$fromName = 'OTSI Online';
		}
		elseif ($esTouravion){
			$from="turavion@mail.distantis.com";
			$fromName = 'Turavion Online';		
		}
		else{
			$from="cts@mail.distantis.com";
			$fromName = 'CTS-Online';
		}

    $mailHeaders =  'From: '.$fromName.' '.$from."\r\n";
	
	//Copy to... Se reemplazan los ; separadores de cuentas de destino por ",", Exim4 soporta ","
    if($copy_to!=""){
		$copy_to = str_replace(";",",",$copy_to);
		$mailHeaders .= 'Cc: '.$copy_to."\r\n";
	}
	
	//Aqui se determina que el contenido del email es HTML y el juego de caracteres a usar
    $mailHeaders .= 'Content-Type: text/html; charset=ISO-8859-1'.'\r\n';
	//Esto dice que fue generado por un PHP
    //$mailHeaders .= 'X-Mailer: PHP/' . phpversion();
	
	//from y envelope
	$aditionalParms= '-f'.$from." -r".$from;
    
	//a quien se le envia el email... Se reemplazan los ; separadores de cuentas de destino por ",", Exim4 soporta ","
	$mail_to = str_replace(";",",",$mail_to);
	$mail_to = "<".str_replace(",",">,<",$mail_to).">";

	
    //Si el mensaje no ha podido ser enviado se realizaran 4 intentos mas como mucho 
	//para intentar enviar el mensaje, cada intento se hara 5 segundos despues 
	//del anterior, para ello se usa la funcion sleep	
	$intentos=1; 
	$exito=false;
	while ((!$exito) && ($intentos <4)) {
		sleep(5);
		//echo " ".$mail->ErrorInfo;
		$exito = mail($mail_to, $subject, $message_html, $mailHeaders);
		$intentos=$intentos+1;	
	
	}

	if(!$exito)
	{
		$ok = "no";
		return $ok;
	}else {
		$ok = "si";
		return $ok;
	}
}
?>
