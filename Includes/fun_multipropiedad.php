<?php	 
	function cbbHoteles($dbCon, $id_usuario, $id_empresa, $id_cliente){
		$StringOutput="";
		
		$sqlHoteles="
			SELECT DISTINCT
				e.id_empresa, h.hot_nombre
			FROM    (SELECT id_empresa FROM usuarios WHERE id_usuario = ".$id_usuario."
				UNION
				SELECT id_empresa FROM usuario_multihotel WHERE id_usuario = ".$id_usuario.") e
			INNER JOIN hotel h ON h.id_hotel = e.id_empresa
			WHERE
				h.`id_tipousuario` = 2
			ORDER BY h.hot_nombre
		";
		//echo $sqlHoteles;
		$recHoteles = $dbCon->SelectLimit($sqlHoteles) or die(__FUNCTION__." : ".__LINE__." ".$dbCon->ErrorMsg());
		
		$StringOutput.='<form id="formHoteles" name="formHoteles" method="get">';
		$StringOutput.='<input type="hidden" name="id_cliente" value="'.$id_cliente.'">';
		$StringOutput.='<select class="styled-select" name="MultiHotel" id="Multihotel" onchange="this.form.submit()">';
		while(!$recHoteles->EOF){
			if($id_empresa==$recHoteles->Fields('id_empresa'))
				$StringOutput.='<option SELECTED value="'.$recHoteles->Fields('id_empresa').'">'.$recHoteles->Fields('hot_nombre').'</option>';
			else
				$StringOutput.='<option value="'.$recHoteles->Fields('id_empresa').'">'.$recHoteles->Fields('hot_nombre').'</option>';
				
			$recHoteles->MoveNext();
		}
	
		$StringOutput.='</select>';
		$StringOutput.='</form>';
		return $StringOutput;		
		
	}
	function cbbHotelesDesdeArray($arrayHoteles){
		echo count($arrayHoteles['id_hotel']);
	}
	function cbbCliente($db1,$id_hotel, $id_cliente){
		$StringOutput="";
		$StringOutput.='<form method="post">';
		//echo $_SESSION['cliente'][$id_cliente];
		// $StringOutput.='<input type="hidden" name="MultiHotel" value="'.$id_hotel.'">';
		$StringOutput.='	<select class="styled-select" name="id_cliente" onchange="this.form.submit()">';
		$clientes_sql = "SELECT * FROM cliente WHERE cli_estado = 1 and id_cliente in (".implode(',', $_SESSION['queCliente']).")";
		//echo $clientes_sql ;
		$clientes = $db1->Execute($clientes_sql) or die(__LINE__." - ".$db1->ErrorMsg());

		while(!$clientes->EOF){
			$StringOutput.='<option value="'.$clientes->Fields('id_cliente').'"';
			if($id_cliente==$clientes->Fields('id_cliente')){
						$StringOutput.='SELECTED';
			}
			$StringOutput.='>'.$clientes->Fields('cli_nombre').'</option>';
			$clientes->MoveNext();
		}
		$StringOutput.='</select>';
		$StringOutput.='</form>	';
		
		return $StringOutput;
    }				

	function arrHoteles($dbCon, $username, $password){	
		$sqlHoteles="
		SELECT DISTINCT
			e.id_empresa, h.hot_nombre
		FROM    (SELECT id_empresa FROM usuarios WHERE usu_login = '".$username."' AND usu_password = '".$password."'
			UNION
			SELECT id_empresa FROM usuario_multihotel WHERE id_usuario = 
				(SELECT id_usuario FROM usuarios WHERE usu_login = '".$username."' AND usu_password = '".$password."')) e
		INNER JOIN hotel h ON h.id_hotel = e.id_empresa
		WHERE
			h.`id_tipousuario` = 2
		ORDER BY h.hot_nombre";
		//echo $sqlHoteles;
		$recHoteles = $dbCon->SelectLimit($sqlHoteles) or die(__FUNCTION__." : ".__LINE__." ".$dbCon->ErrorMsg());
		
		while(!$recHoteles->EOF){
			$arrHoteles['id_hotel']=$recHoteles->Fields('id_empresa').'';
			$arrHoteles['hot_nombre']=$recHoteles->Fields('hot_nombre').'';
			$recHoteles->MoveNext();
		}
		return $arrHoteles;
		
	}
?>
