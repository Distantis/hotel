<?php	 	 
$parametro = $_GET['parametro'];
$hotel = $_GET['id_hotel'];
require_once('Connections/db1.php');
include_once("Connections/db3.php");
include_once("Connections/db4.php");
include_once("Connections/db5.php");
require_once('Includes/Control.php');
require_once('lan/idiomas.php');

$query_nulo = "SELECT 
				  d.cd_numreserva,
				  d.id_cotdes,
				  p.pac_nombre,
				  c.id_tipopack,
				  c.id_seg,
				  c.id_cot,
				  s.cp_nombres,
				  s.cp_apellidos,
				  DATE_FORMAT(c.cot_fec, '%d-%m-%Y') AS cot_fec,
				  DATE_FORMAT(MIN(d.cd_fecdesde), '%d-%m-%Y') AS cot_fecdesde,
				  DATE_FORMAT(MAX(d.cd_fechasta), '%d-%m-%Y') AS cot_fechasta,
				  DATE_FORMAT(
					MAX(c.cot_fecanula),
					'%d-%m-%Y %H:%i:%s'
				  ) AS cot_fecanula,
				  DATE_FORMAT(
					MAX(cot_fecconf),
					'%d-%m-%Y %H:%i:%s'
				  ) AS cot_fecconf,
				  CASE
					c.id_tipopack 
					WHEN 1 
					THEN 'PROGRAMA CUSTOMIZADO' 
					WHEN 2 
					THEN p.pac_nombre 
					WHEN 3 
					THEN 'SERVICIO INDIVIDUAL' 
					WHEN 5 
					THEN p.pac_nombre 
				  END AS nom_programa,
				  c.cot_estado,
				  c.cot_numpas,
				  IF(
					d.cd_hab1 > 0,
					CONCAT(d.cd_hab1, ' SINGLE
				'),
					''
				  ) AS SIN,
				  IF(
					d.cd_hab2 > 0,
					CONCAT(d.cd_hab2, ' TWIN
				'),
					''
				  ) AS dob,
				  IF(
					d.cd_hab3 > 0,
					CONCAT(d.cd_hab3, ' MATRIMONIAL
				'),
					''
				  ) AS mat,
				  IF(
					d.cd_hab4 > 0,
					CONCAT(d.cd_hab4, ' TRIPLE'),
					''
				  ) AS tri 
				FROM
				  cotdes d 
				  INNER JOIN cot c 
					ON c.id_cot = d.id_cot 
				  INNER JOIN cotpas s 
					ON s.id_cot = c.id_cot 
				  LEFT JOIN pack p 
					ON p.id_pack = c.id_pack 
				WHERE c.cot_estado != 1 
				  AND c.id_seg IN (7, 13) 
				  AND d.id_seg = 7 
				  AND d.id_hotel = ".$_GET['id_hotel']." 
				  AND d.cd_estado = 0
				  AND cd_numreserva IS NULL
				GROUP BY d.id_cot 
				ORDER BY c.id_cot DESC ";
//echo $query_nulo;
if($parametro == 1){
$conf = $db1->SelectLimit($query_nulo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
//echo "entro 1";
}
if($parametro == 3){
$conf = $db3->SelectLimit($query_nulo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
//echo "entro 3";
}
if($parametro == 4){
$conf = $db4->SelectLimit($query_nulo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
//echo "entro 3";
}
if($parametro == 5){
$conf = $db5->SelectLimit($query_nulo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
//echo "entro 3";
}


?>

<html>
	<head>
		 <link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
		<link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	
	<script>
	
		function ingresar(e,f,g,h){
			var listo = true;
			if(f == ''){
				//alert("vacio");
				listo = false;
			}
			if(f == '0'){
				//alert("es 0");
				listo = false;
			}
			if (listo == true){
				
				ingresar2(e,f,g,h);
			}
		}
		function ingresar2(a,b,c,d){
		    $.ajax({
			type: 'POST',
			url: 'solicitar_reserva_pendiente.php?flag='+c+'&cot='+a+'&reserva='+b+'&cliente='+d,
			data: {},
			success:function(result){
				var comboboxCiudad='';
				comboboxCiudad+=result;
				//alert(result);
				
				if(result=='0'){
				alert("Hubo un problema en la integración de SAP");
				}else{
				//alert("Ingreso bien el numero SAP");
				}
				
								
			},
			error:function(){
				alert("Error!!");
				return;
			}
		});
		}
		
		
	</script>
	</head>

<table width="900px" class="programa">
                              <tr>
                                <th width="3%">N&ordm;</th>
                                <th width="6%">COT</th>
                                <th width="12%">Tipo HAB</th>
                                <th width="13%">Fecha Desde</th>
                                <th width="13%">Fecha Hasta</th>
                                <th width="20%">pasajero</th>
                                <th width="11%">Estado</th>
                                <th width="12%">NUMERO CONFIRMACION</th>
                                
							</tr>
                               
                                <?php	 	 
                      $c = 1;
                        while (!$conf->EOF) {
                      ?>
                          
                              
                              <tr title='N&deg;<?php	 	  echo $c?>' onmouseover="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onmouseout="style.background='none', style.color='#000'" style="font-size:11px;">
                                <td><center>
                                  <?php	 	  echo $c; ?>&nbsp;
                                </center></td>
                                <td align="center"><?php	 	  echo $conf->Fields('id_cot'); ?></td>
                                <td align="center"><? echo $conf->Fields('sin').$conf->Fields('dob').$conf->Fields('mat').$conf->Fields('tri');?></td>
                                <td align="center"><?php	 	  echo $conf->Fields('cot_fecdesde'); ?>&nbsp;</td>
                                <td align="center"><?php	 	  echo $conf->Fields('cot_fechasta'); ?>&nbsp;</td>
                                <td align="center"><?php	 	  echo $conf->Fields('cp_nombres')." ".$conf->Fields('cp_apellidos'); ?></td>
								<td align="center" bgcolor="#009900"><font color='white'><b>CONFIRMADA<br /><?php	 	  echo $conf->Fields('cot_fecconf') ?></b></font></td> 
							    <td align="center"><input id="nr" type="text" onblur="ingresar('<?php	 	  echo $conf->Fields('id_cotdes') ?>',this.value,'nr','<?php	 	  echo $parametro ?>')" value="" name="nr"></input></td>
							 </tr>	
						<?php	 	 
						$c++;
						$conf->MoveNext();
						}
						?>
						</table>
						
						
</html>