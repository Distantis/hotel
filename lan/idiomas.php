<?
$distantis_mail = "backup.distantis@gmail.com;natalia@distantis.com";
$distantis_mail_veci = "backup.distantis@gmail.com";

$distantis_mail_otsi = "backup.distantis@gmail.com;natalia.chavez.distantis@gmail.com;online@otsi.cl";

$distantis_adm = "backup.distantis@gmail.com";

//$skiweek_mail = "; finanzas@ctsturismo.cl; hugo.palma@ctsturismo.cl";
$skiweek_mail = "; backup.distantis@gmail.com";
if($_SESSION['idioma'] == 'sp'){
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//FRASES EN ESPAÑOL.
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	$confirm_prog_mod_fecha="La fecha de llegada o las habitaciones fueron modificadas en esta cotización.\\nSe debe verificar la disponibilidad nuevamente, esto eliminará las noches adicionales\\n\\n¿Está seguro que desea continuar?";
	
	$advertenciaserv = "Importante: Se agrego el servicio, pero hay un servicio para la misma fecha.";
	$noexisteserv = "No existe servicio individual para la fecha / destino ingresados.";
	$mensaje_antelacion1 = "Tiene que reservar con ";
	$mensaje_antelacion2 = " dias de antelaci\363n.";
	
	$buenostardes = "Buenas tardes";
	$buenosdias = "Buenas dias";
	$buenosnoches = "Buenas noches";
	$bienvenido = "bienvenido";
	
	$ayuda = "Ayuda";
	$buscar = "Buscar";
	$idiotas = "Solo tiene que apretar una vez el boton confirmar.";
	
	$primero = "Primera";
	$anterior = "Anterior";
	$siguiente = "Siguiente";
	$ultimo = "&Uacute;ltima";
	$activa = "Activo";
	
	$de = "de";
	$todos = "-= TODOS =-";
	$todos2 = "Todos";
	
	$validas_tarifas = "TARIFAS VALIDAS SOLO PARA EXTRANJEROS NO RESIDENTES EN CHILE.";
	$tributaria = "Se les informara que los pasajeros para hacer uso de la franquicia tributaria deben presentar pasaporte y tarjeta de emigración.";

	$producto = "Productos";
	$progr = "WORKBOOK & PROGRAMAS";
	$solowork = "WORKBOOK";
	$soloprogr = "PROGRAMAS";
	$programa = "Programa";
	$proconf = "Programa confirmado";
	$progr_tt = "Selecciona alguno de nuestros Programas";
	$creaprog = "Crea un Programa";
	$creaprog_tt = "Crea un Programa a la medida de tu cliente";
	$servind = "Servicios Individuales";
	$servinc = "Servicios Incluidos";
	$nomserv = "Nombre Servicio";
	$servind_tt = "Contrata Servicios Individuales";
	$dest = "Destacados";
	$progdest = "Programas Destacados";
	$prtodos = "";
	$prtodos_tt = "Ver Todos los Programas";
	$wbtodos_tt = "Ver Todos los Workbook";

	$reservar = "Reservar Ahora";
	$duracion = "Duraci&oacute;n";
	$origen = "Origen";
	$destino = "Destino";
	$dias = "Dias";
	$noches = "Noches";
	$selfoto = "Seleccionar este Programa";
	
	$cot1 = "Cotizacion";
	$cot2 = "Cot";
	$paso = "Paso";
	$datosprog = "Datos Programa";
	$nombre = "Nombre";
	$nombre2 = "Nombre Pax";
	$numpas = "N&ordm; de Pasajeros";
	$resumen = "Resumen Destino";
	$tipohotel = "Tipo Hotel";
	$sector = "Sector Hotel";
	$fecha1 = "Fecha Desde";
	$fecha11 = "Fecha Llegada";
	$fecha2 = "Fecha Hasta";
	$fecha22 = "Fecha Salida";	
	$volver = "Volver";
	$irapaso = "Ir a Paso";
	$direccion = "Direcci&oacute;n";
	
	$fecha_anulacion = "Fecha Anulaci&oacute;n Sin Costo :";
	
	$fecha_abono = "Fecha requerida del abono :";


	$tipohab = "Tipo Habitacion";
	$sin = "Single";
	$dob = "Doble Twin";
	$tri = "Doble Matrimonial";
	$cua = "Triple";

	$deshotel = "Destino y Hotel";
	$cat = "Categor&iacute;a";
	$serv = "Servicio";
	$fechaserv = "Fecha";
	$agregar = "Agregar";

	$disinm = "Confirmaci&oacute;n Instantanea";
	$hotel_nom = "Hotel";
	$disp_hotel = "Disponibilidad Hoteles";
	$disp = "Disponibilidad";
	$hotel_noms = "Hoteles";
	$val = "Valor Total Reserva";
	$valdes = "Valor Destino";
	$res = "Reservar";
	$solicitar = "Solicitar";

	$confirma = "Confirmar";
	$detvuelo = "Detalle del Vuelo";
	$vuelo = "N&deg; Vuelo Llegada";
	$detpas = "Detalle del Pasajero";
	$pasajero = "Pasajero";
	$borrarpas = "Eliminar pasajero";
	$ape = "Apellidos";
	$pasaporte = "DNI  / N&deg; Pasaporte";
	
	$sel_pais = "-= Seleccione Pais =-";
	
	$serv_trans_pasajero = "N&deg; de Pasajeros";
	$serv_trans_nombre1 = "Nombre 1&deg; Pasajero";
	$serv_trans_apellido1 = "Apellido 1&deg; Pasajero";

	$nuevopro = "Crea tu Propio Programa";
	$siguiente = "Siguiente";
	$cancelar = "Cancelar";

	$otrodest = "Otro Destino";
	$servaso = "Servicios Adicionales";
	$nochesad = "Servicios de Noches Adicionales";
	$nochesad_pre = 'Servicio Noche Adicional PRE-PROGRAMA';
	$nochesad_post = 'Servicio Noche Adicional POST-PROGRAMA';
	$nochesadi = "Noches Adicionales";
	$nomservaso = "Nombre del Servicio";

	$serv_indi_home = "Si necesita combinar ambos Servicios, lo invitamos a";
	$serv_hotel = "Servicios de Hoteles";
	$serv_trans = "Servicios de Transportes";
	$serv_transconf = "Servicio Individual Transporte Confirmado";
	$serv_transor = "Servicio Individual Transporte ON-REQUEST";
	$serv_transcanu = "Servicio Individual Transporte Anulado";
	$dettrans = "Detalle del Transporte";
	$transporte = "Transporte";
	$crea_serv = "Crear un Servicio de Transporte";
	$crea_hot = "Crear Servicios de Hotel";

	$duracion = "Duraci&oacute;n";
	$tipo_pro = "Tipo Programa";
	$nom_prog = "Nombre Programa";
	$estado = "Estado";
	$tipo = "Tipo";
	$ver = "Ver";
	
	$seleccione = "-= Seleccione =-";
	$comuna ="Comuna";

	$procoti = "Programas Cotizados";
	$descripcion = "Descripcion";
	$habprograma = "Habitaciones para el Programa";
	$servicios = "Necesita agregar algun servicio extra al Programa";
	$cerrarcot = "Cerrar Cotizacion";
	$perfil = "Mi Perfil";
	$salir = "Salir";
	$olvpass = "Olvido su contraseña";
	$registrarse = "Registrarse";
	$contacto = "Contacto";
	$user = "Usuario";
	$userCreador = "Usuario creador";
	
	$pass = "Contrase&Ntilde;a";
	$pass_cambia = "Cambia Contrase&ntilde;a";
	$pass_actual = "Contrase&Ntilde;a Actual";
	$pass_nueva = "Nueva Contrase&Ntilde;a";
	$pass_nueva_rep = "Repita Nueva Contrase&Ntilde;a";
	$tarifas = "Tarifas";

	$limpiar = "Limpiar";
	$guardar = "Guardar";
	$cancelar = "Cancelar";
	$mod = "Modifica";
	$anu = "Anula";

	$derechos = "Derechos Reservados";
	
	$programaconfirmado = "Este Programa se encuentra Confirmado y no es posible modificar sus datos.";
	$sinreserva = "Si no quieres confirmar la reserva, &eacute;sta quedar&aacute; guardada en '<a href='pack_busca.php'>Programas Cotizados</a>'.";
	$pais_p = "Pa&iacute;s";
	
	$confirma1 = "Gracias por comprar en CTS ONLINE. Ya deber&iacute;as haber recibido una copia para tus registros, por lo tanto, revisa tu correo. Te recordamos que todas las pol&iacute;ticas habituales de venta, modificaciones, y anulaciones de CTS rigen para esta reserva.";
	$confirma2 = "En caso de que requieras modificar o anular tu reserva, simplemente ingresa a <a href='pack_busca.php'>Programas Cotizados</a>.";
	$confirma3 = "En caso de cualquier duda o consulta, escr&iacute;benos un mail a <a href='mailto:info@distantis.com'>info@distantis.com</a>.";

	$confirma_veci1 = "Gracias por comprar en VECI ONLINE. Tu reserva ha sido confirmada de acuerdo a la informaci&oacute;n que aparece abajo. Ya deber&iacute;as haber recibido una copia de la confirmaci&oacute;n para tus registros, por lo que revisa tu correo. Te recordamos que todas las pol&iacute;ticas habituales de venta, modificaciones, y anulaciones de VECI rigen para esta reserva.";
	$confirma_veci2 = "En caso de que requieras modificar o anular tu reserva, simplemente ingresa a <a href='pack_busca.php'>Programas Cotizados</a>.";
	$confirma_veci3 = "En caso de cualquier duda o consulta, escr&iacute;benos un mail a <a href='mailto:info@distantis.com'>info@distantis.com</a>.";


	$confirma_otsi1 = "Gracias por comprar en OTSi ONLINE. Tu reserva ha sido confirmada de acuerdo a la informaci&oacute;n que aparece abajo. Ya deber&iacute;as haber recibido una copia de la confirmaci&oacute;n para tus registros, por lo que revisa tu correo. Te recordamos que todas las pol&iacute;ticas habituales de venta, modificaciones, y anulaciones de OTSi rigen para esta reserva.";
	$confirma_otsi2 = "En caso de que requieras modificar o anular tu reserva, simplemente ingresa a <a href='pack_busca.php'>Programas Cotizados</a>.";
	$confirma_otsi3 = "En caso de cualquier duda o consulta, escr&iacute;benos un mail a <a href='mailto:info@distantis.com'>info@distantis.com</a>.";

	$confirma_ta1 = "Gracias por comprar en Turavion ONLINE. Tu reserva ha sido confirmada de acuerdo a la informaci&oacute;n que aparece abajo. Ya deber&iacute;as haber recibido una copia de la confirmaci&oacute;n para tus registros, por lo que revisa tu correo. Te recordamos que todas las pol&iacute;ticas habituales de venta, modificaciones, y anulaciones de Turavion rigen para esta reserva.";
	$confirma_ta2 = "En caso de que requieras modificar o anular tu reserva, simplemente ingresa a <a href='pack_busca.php'>Programas Cotizados</a>.";
	$confirma_ta3 = "En caso de cualquier duda o consulta, escr&iacute;benos un mail a <a href='mailto:info@distantis.com'>info@distantis.com</a>.";

	$vol_reservar = "Realizar Otra Reserva";
	
	$servicios_hotel = "RESERVAR UN HOTEL SIN SERVICIOS TERRESTRES";
	$servicios_trans = "RESERVAR SERVICIOS TERRESTRES SIN HOTELERIA";
	
	$quiere_serv = "¿Quieres agregar otros Servicios al Programa?";
	$otro_prog = "¿Desea agregar otro destino a este Programa?";
	$quiere_nox = "¿Necesita agregar Noches Adicionales?";
	
	$si = "Si";

	$serv_indi = "Reserva On-Request";

	$pack_promo = "Promociones";
	$pack_promo_tt = "Ver Todas las Promociones";
	
	$observa = "Observaciones";
	$nuevo_op = "Nuevo Operador";
	$operador = "Operador";
	$correlativo = "Numero Correlativo";
	
	$anula1 = "Te recordamos que las pol&iacute;ticas de anulaci�n de Reservas son aquellas vigentes con CTS.";
	$anula2 = "De acuerdo a dichas pol&iacute;ticas esta Reserva puede anularse sin costo hasta el ";
	$anula3 = "Una vez anulada la Reserva no podra ser restituida. Si quieres reactivarla tendr�s que realizar una nueva Reserva.";
	$anula4 = "Si los plazos de anulaci&iacute;n sin costo han vencido, la plataforma no permitira realizar la anulaci�n ON-LINE. En dicho caso favor contactar <a href='mailto:cts@distantis.com'>cts@distantis.com</a>.";		

	$anula_veci1 = "Te recordamos que las pol&iacute;ticas de anulaci�n de Reservas son aquellas vigentes con VECI.";
	$anula_veci2 = "De acuerdo a dichas pol&iacute;ticas esta Reserva puede anularse sin costo hasta el ";
	$anula_veci3 = "Una vez anulada la Reserva no podra ser restituida. Si quieres reactivarla tendr�s que realizar una nueva Reserva.";
	$anula_veci4 = "Si los plazos de anulaci&iacute;n sin costo han vencido, la plataforma no permitira realizar la anulaci�n ON-LINE. En dicho caso favor contactar <a href='mailto:cts@distantis.com'>veci@distantis.com</a>.";		
	
	$pol_anula_no = "Lo sentimos. Los plazos de anulacion sin costo para esta reserva han sido excedidos, favor contactar <a href='mailto:cts@distantis.com'>cts@distantis.com</a> para solicitar la anulaci�n manualmente.";
	$pol_anula_si = "La reserva ha sido anulada con �xito.";

	$anulado = "Este programa ha sido anulado";
	$creador = "Creador";
	
	$request1 = "Gracias por utilizar la plataforma CTS ONLINE. Debido a problemas de disponibilidad, la solicitud de servicios que has ingresado se encuentra ON REQUEST y a&uacute;n no se considera confirmada. No podr&aacute; confirmarse hasta recibir autorizaci&oacute;n de disponibilidad de el o los hoteles que solicitaste en el proceso de compra.";
	$request21 = "La solicitud de reserva fue ingresada a las ";
	$request22 = ". A contar de dicha hora, el o los hoteles solicitados tienen un plazo m&aacute;ximo de 24 horas para confirmar o rechazar la solicitud de reserva. Si ese plazo se cumple sin recibir notificaci&oacute;n de el o los hoteles solicitados, la solicitud quedar&aacute; autom&aacute;ticamente anulada.";
	$request3 = "Recibir&aacute;s una notificaci&oacute;n autom&aacute;tica via email tan pronto el o los hoteles solicitados confirmen o rechacen la reserva solicitada. Tambi&eacute;n podr&aacute;s revisar el estado de esta solicitud <a href='pack_busca.php'>aqui</a>.";
	$request4 = "Ante cualquier duda o consulta, por favor cont&aacute;ctanos a <a href='cts@distantis.com'>cts@distantis.com</a>.";
	
	$el = "el";
	$agrega_pax = "Agregar Pasajero";
	$agrega_pax2 = "Agregar PAX";
	$todos_pax = "Para todos los PAX";
	$numtrans = "N&deg; de Transporte";
	$clickaca = "Ver Promociones haga click "; 
	$quetemporada = "&iquest;que temporada quieres ver?"; 
	$serv_hotel = "Servicio Individual Hotel";
	
	$buscar = "Buscar";
	
	$valor = "Valor";
	$confirmado = "Confirmado";
	
	$tarifa_chile = "Tarifas validas solo para extranjeros no residentes en Chile";
	$dest_rem = "El destino ha sido removido";
	$sehaencontradodisp="SE HA ENCONTRADO DISPONIBILIDAD PARA LAS FECHAS SOLICITADAS";
	$nohaencontradodisp="No se a encontrado disponibilidad para las fechas solicitadas pero puede seleccionar uno de los siguientes hoteles.";
	
	$noxtotales="Noches Totales";
	$total="Total";

	$reserva="Reserva";
	$docDesde="Documento enviado desde";
	$aca="aca";
	
	$tra_or="(SERVICIO ON-REQUEST)";

	$just_cuerpo_mail="Justificaci&oacute;n / Email";
	$aprobar="Aprobar";	
}

if($_SESSION['idioma'] == 'po'){
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//FRASES EN PORTUGUES.
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		$confirm_prog_mod_fecha="A data de chegada ou a quantidade de habitações desta reserva foram modificadas.\\nSerá necessário verificar a disponibilidade novamente e isso eliminará as noites adicionais.\\n\\nDeseja continuar?";

	$advertenciaserv = "Importante: O serviço foi adicionado, porém já existe um outro para a mesma data.";
	$noexisteserv = "Não existe serviço individual para a data ou destino ingressado.";
	$mensaje_antelacion1 = "Deve-se reservar com ";
	$mensaje_antelacion2 = " dias de anteced\352ncia.";
	
	$buenostardes = "Boa Tarde";
	$buenosdias = "Bom dia";
	$buenosnoches = "Boa Noite";
	$bienvenido = "seja bem vindo";
	$procoti = "Programas Cotados";
	$salir = "Sair";
	
	$ayuda = "Ajuda";
	
	$idiotas = "Basta pressionar uma vez o botão confirmar.";
	
	$primero = "Primeira";
	$anterior = "Anterior";
	$siguiente = "Seguinte";
	$ultimo = "&Uacute;ltima";
	$activa = "Ativo";
	
	$de = "de";
	$todos = "-= TODOS =-";
	$todos2 = "Todos";
	
	$validas_tarifas = "TARIFAS VALIDAS S&Oacute; PARA EXTRANJEIROS N&Atilde;O RESIDENTES NO CHILE.";
	$tributaria = "Eles relataram que os passageiros fazem uso da isenção fiscal deve apresentar passaporte e cartão de migração.";
	
	$producto = "Produtos";
	$progr = "WORKBOOK & PROGRAMAS";
	$solowork = "WORKBOOK";
	$soloprogr = "PROGRAMA";
	$programa = "Programas";
	$proconf = "Programa Confirmado";
	$progr_tt = "Selecione qualquer um dos nossos programas";
	$creaprog = "Criar um programa";
	$creaprog_tt = "Criar um programa sob medida para o seu cliente";
	$servind = "Servi&ccedil;os Individuais";
	$servinc = "Servi&ccedil;os Incluidos";
	$nomserv = "Nome do Servi&ccedil;o";
	$servind_tt = "Contrato de Servi&ccedil;os individuais";
	$dest = "Destaques";
	$progdest = "Programas Destaques";
	$prtodos = "";
	$prtodos_tt = "Ver Todos os Programas";
	$wbtodos_tt = "Ver Todos os Workbook";

	$reservar = "Reservar Agora";
	$duracion = "Dura&ccedil;&atilde;o";
	$origen = "Origem";
	$destino = "Destino";
	$dias = "Dias";
	$noches = "Noites";
	$selfoto = "Selecione este programa";

	$cot1 = "Cota&ccedil;&atilde;o";
	$cot2 = "Cot";
	$paso = "Passo";
	$datosprog = "Dados Programa";
	$nombre = "Nome";
	$nombre2 = "Nome Pax";
	$numpas = "N&deg; de Passageiros";
	$resumen = "Resumo Destino";
	$tipohotel = "Tipo Hotel";
	$sector = "Setor Hotel";
	$fecha1 = "Data Chegada";
	$fecha11 = "Data Chegada";
	$fecha2 = "Data Sa&iacute;da";
	$fecha22 = "Data Sa&iacute;da";
	$irapaso = "Ir para Etapa";
	$direccion = "Endere&ccedil;o";
	
	$fecha_anulacion = "Data de Cancelamento Sem Multa :";
	
	$fecha_abono = "Data exigida de pagamento :";

	$tipohab = "Tipo Apto.";
	$sin = "Single";
	$dob = "Duplo Twin";
	$tri = "Duplo Matrimonial";
	$cua = "Triplo";

	$deshotel = "Destino e Hotel";
	$cat = "Categoria";
	$serv = "Servi&ccedil;o";
	$fechaserv = "Data";
	$agregar = "Adicionar";

	$disinm = "Confirma&ccedil;&atilde;o Imediata";
	$hotel_nom = "Hotel";
	$disp_hotel = "Disponibilidade Hot&eacute;is";
	$disp = "Disponibilidade";
	$hotel_noms = "Hot&eacute;is";
	$volver = "Voltar";
	$res = "Reservar";
	$solicitar = "Solicitar";
	
	$serv_trans_pasajero = "N&deg; de Passageiros";
	$serv_trans_nombre1 = "1&deg; nome do passageiro";
	$serv_trans_apellido1 = "1&deg; Sobrenome do passageiro";

	$confirma = "Confirmar";
	$detvuelo = "N&uacute;mero do V&ocirc;o";
	$vuelo = "N&deg; V&ocirc;o Chegada";
	$detpas = "Detalhes Passageiros";
	$pasajero = "Passageiro";
	$borrarpas = "Remover passageiro";
	$ape = "Sobrenomes";
	$pasaporte = "DNI  / N&deg; Passaporte";
	
	$sel_pais = "-= Selecione o País =-";

	$nuevopro = "Crie o seu Pr&oacute;prio Programa";
	$siguiente = "Seguinte";
	$cancelar = "Cancelar";

	$otrodest = "Outro Destino";
	$servaso = "Servi&ccedil;os adicional";
	$nochesad = "Servi&ccedil;os noites adicionais";
	$nochesad_pre = 'Servi&ccedil;o Noites Adicionais PRE-PROGRAMA';
	$nochesad_post = 'Servi&ccedil;o Noites Adicionais POST-PROGRAMA';
	$nochesadi = "Noites adicionais";
	$nomservaso = "Nome do Servi&ccedil;o";

	$serv_indi_home = "Se voc&ecirc; precisa combinar os dois servi&ccedil;os, lhe convidamos a criar o";
	$serv_hotel = "Servi&ccedil;os Hot&eacute;is";
	$serv_trans = "Servi&ccedil;os de Transporte";
	$transporte = "Transporte";
	$crea_serv = "Criar um Servi&ccedil;o de Transporte";
	$crea_hot = "Servi&ccedil;o Individual de Hotel";
	$serv_transconf = "Servi&ccedil;o de Transporte Individual Confirmado";
	$serv_transor = "Servi&ccedil;o de Transporte Individual ON-REQUEST";
	$serv_transcanu = "Servi&ccedil;o de Transporte Individual Cancelado";

	$duracion = "Dura&ccedil;&acirc;o";
	$tipo_pro = "Tipo Programa";
	$nom_prog = "Nome do Programa";
	$estado = "Estado";
	$tipo = "Tipo";
	$ver = "Ver";
	
	$seleccione = "-= Seleccione =-";
	$comuna ="Localiza&ccedil;&atilde;o";

	$descripcion = "Descri&ccedil;&atilde;o";
	$val = "Total Reserva";
	$valdes = "Valor Hotel";

	$habprograma = "Aptos para o Programa";
	$servicios = "Necessita adicionar algum Servi&ccedil;o ao Programa?";
	$cerrarcot = "Fechar Cota&ccedil;&atilde;o";
	$perfil = "Meu Perfil";
	$olvpass = "Esqueceu sua Senha?";
	$registrarse = "Registrar";
	$contacto = "Contato";
	$user = "Usu&aacute;rio";
	$userCreador = "Usu&aacute;rio criador";
	$pass = "Senha";
	$pass_cambia = "Mudar Senha";
	$pass_actual = "Senha Atual";
	$pass_nueva = "Nova Senha";
	$pass_nueva_rep = "Repita a Senha Atual";
	$tarifas = "Tarifas";
	$buscar = "Buscar";
	$limpiar = "Limpar";
	$guardar = "Salvar";
	$cancelar = "Cancelar";
	$mod = "Modifica&ccedil;&otilde;es";
	$anu = "Anular";
	
	$derechos = "Direitos Reservados";
	$programaconfirmado = "Este programa está confirmado e nâo será possível mudar seus dados";
	$sinreserva = "Se voc&ecirc; quiser confirmar a reserva, ele est&aacute; salvo nos '<a href='pack_busca.php'>Programas Cotados</a>'.";
	$pais_p = "Pais";
	$confirma1 = "Obrigado por fazer compras CTS ONLINE. A sua reserva foi confirmada de acordo com as informa&ccedil;&ocirc;es abaixo. Voc&ecirc; deve ter recebido uma c&oacute;pia da confirma&ccedil;&acirc;o de seus registros, favor verificar em seus e-mails. Lembre-se que todas as pol&iacute;ticas habituais de vendas, modifica&ccedil;&ocirc;es e cancelamentos se aplicam a esta reserva.";
	$confirma2 = "Caso voc&eacute; precise alterar ou cancelar sua reserva, basta clicar em <a href='pack_busca.php'>Programas Cotados.</a>.";
	$confirma3 = "Em caso de d&uacute;vidas envie-nos um e-mail para <a href='mailto:cts@distantis.com'>cts@distantis.com</a>.";
	$vol_reservar = "Fazer outra reserva";

	$servicios_hotel = "RESERVAR UM HOTEL SEM SERVI&Ccedil;OS DE TRANSPORTE";
	$servicios_trans = "RESERVAR UM TRANSPORTE SEM SERVI&Ccedil;OS DE HOTEL";
	
	$quiere_serv = "Voc&ecirc; quer adicionar servi&ccedil;os para o programa?";
	$otro_prog = "Voc&ecirc; quer adicionar outro destino para este programa?";
	$quiere_nox = "Voc&ecirc; quer adicionar noites extras?";
	
	$si = "Sim";
	
	$serv_indi = "Reserva On-Request";

	$pack_promo = "Promo&ccedil;&ocirc;es";
	$pack_promo_tt = "Ver Todas as Promo&ccedil;&ocirc;es";
	
	$observa = "Observa&ccedil;&ocirc;es";
	$nuevo_op = "Novo Operador";
	$operador = "Operador";
	$correlativo = "Numero Correlativo";

	$anula1 = "Lembramos que a pol&iacute;tica de cancelamento s&acirc;o aqueles em Reservas efeito com CTS.";
	$anula2 = "De acordo com essas pol&iacute;ticas da Reserva pode ser cancelada sem nenhum custo para o ";
	$anula3 = "Uma vez cancelada a reserva n&acirc;o poder&aacute; ser reativada. Se voc&ecirc; quiser reativar voc&ecirc; voc&ecirc; precisar&aacute;</h6> fazer uma nova reserva.";
	$anula4 = "Se o prazo de anula&ccedil;&atilde;o sem custo estiver expirado, a plataforma n&atilde;o permitir&aacute; que se anule a reserva. Neste caso, entre em contato <a href='mailto:cts@distantis.com'>cts@distantis.com</a>.";		
	
	$pol_anula_no = "Desculpe. O prazo de anula&ccedil;&atilde;o sem custo para a reserva expirou. Por favor entre em contato <a href='mailto:cts@distantis.com'>cts@distantis.com</a> para solicitar a anula&ccedil;ao manualmente.";
	$pol_anula_si = "A reserva foi cancelada com sucesso.";

	$anulado = "Este programa foi cancelado";
	$creador = "Criador";
	
	$request1 = "Obrigado por utilizar a plataforma CTS ONLINE. Devido a problemas de disponibilidade, a solicita&ccedil;&acirc;o dos servi&ccedil;os foi registrado e se encontra como ON REQUEST, portanto ainda n&atilde;o foi confirmado. Favor n&atilde;o confirmar at&eacute; que receba a autoriza&ccedil;&acirc;o ou a disponibilidade dos hot&eacute;is que você pediu no processo de compra	.";
	$request21 = "O pedido de reserva foi admitido na ";
	$request22 = ". A contar de dicha hora, el o los hoteles solicitados tienen un plazo m&aacute;ximo de 24 horas para confirmar o rechazar la solicitud de reserva. Si ese plazo se cumple sin recibir notificaci&oacute;n de el o los hoteles solicitados, la solicitud quedar&aacute; autom&aacute;ticamente anulada.";
	
	$request22 = ". A partir do hor&aacute;rio da reserva, os hot&eacute;is solicitados possuem um prazo de 24 horas para confirmar ou rejeitar o pedido de reserva. Se esse prazo expira e n&atilde;o recebeu uma notifica&ccedil;&atilde;o sobre a reserva solicitada, o pedido estar&aacute; automaticamente anulada .";
	$request3 = "Voc&ecirc; receber&aacute; uma notifica&ccedil;&acirc;o autom&aacute;tica por e-mail assim que o solicitado ou hot&eacute;is confirmar ou negar a reserva solicitada. Voc&ecirc; tamb&eacute;m poder&aacute; verificar o status desta reserva <a href='pack_busca.php'>aqui</a>.";
	$request4 = "Para quaisquer quest&ocirc;es ou dúvidas, entre em contato conosco <a href='cts@distantis.com'>cts@distantis.com</a>.";
	$el = "o";
	$agrega_pax = "Adicionar Passageiro";
	$agrega_pax2 = "Agregar PAX";
	$todos_pax = "Para todos los PAX";
	$numtrans = "N&deg; de V&ocirc;o";
	$clickaca = "Clique para ver Promo&ccedil;&otilde;es"; 
	$quetemporada = "Epoca que voc&ecirc; quer ver?"; 
	$serv_hotel = "Servi&ccedil;o Individual de Hotel";
	
	$valor = "Valor";
	$confirmado = "Confirmado";

	$tarifa_chile = "Tarifas validas somente para os estrangeiros que vivem n&atilde;o Chile";
	$dest_rem = "Destino foi removido";
	
	$noxtotales="Total de Noites";
	$total="Total";
	
	$reserva="Reserva";
	
	$docDesde="Documento enviado desde";
	$aca="aqui";
	
	$tra_or="(SERVI&Ccedil;O
	 ON-REQUEST)";

	$just_cuerpo_mail="Justifica&Ccedil;&acirc;o / Email";
	$aprobar="Aprovar";
	
}

if($_SESSION['idioma'] == 'en'){
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//FRASES EN INGLES.
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		$confirm_prog_mod_fecha="You are modifying travel dates and/or the number of rooms in your reservation.\\nAvailability will be automatically checked and verified. If this reservation had extra nights added pre or post, they need to be added again in the next step.\\n\\nDo you want to continue?";

	$advertenciaserv = "Warning: a service already exist for the same date. New service has been added.";
	$noexisteserv = "Individual service is not available for the date or destination informed.";
	$mensaje_antelacion1 = "Please note that reservations must be done ";
	$mensaje_antelacion2 = " days in advance.";
		
	$buenostardes = "Good afternoon";
	$buenosdias = "Good morning";
	$buenosnoches = "Good night";
	$bienvenido = "welcome";
	
	$ayuda = "Help";
	
	$idiotas = "Just press once, the confirm button.";
	
	$primero = "First";
	$anterior = "Previous";
	$siguiente = "Next";
	$ultimo = "Last";
	$activa = "Active";
	
	$de = "of";
	$todos = "-= ALL =-";
	$todos2 = "All";
	
	$validas_tarifas = "RATES VALID ONLY FOR NON CHILEAN RESIDENTS.";
	$tributaria = "They reported that the passengers to make use of the tax exemption must present passport and migration card.";
	
	
	$producto = "Products";
	$progr = "WORKBOOK & PACKAGES";
	$solowork = "WORKBOOK";
	$soloprogr = "PACKAGES";
	$proconf = "Confirmed package";
	$progr_tt = "Please select one of our packages";
	$creaprog = "Create a package";
	$creaprog_tt = "Create a customized package for your client";
	$servind = "Individual services";
	$servinc = "Services Included";
	$nomserv = "Service name";
	$servind_tt = "Book individual services";
	$dest = "Highlighted";
	$progdest = "Highlighted Packages";
	$prtodos = "";
	$prtodos_tt = "Display all packages";
	$wbtodos_tt = "Display all Workbook";

	$reservar = "Book now";
	$duracion = "Duration";
	$origen = "Origin";
	$destino = "Destination";
	$dias = "Days";
	$noches = "Nights";
	$selfoto = "Select this package";
	
	$cot1 = "Cotizacion";
	$cot2 = "File";
	$paso = "Step";
	$datosprog = "Package data";
	$nombre = "First name";
	$nombre2 = "Name Pax";
	$numpas = "N&ordm; of Guests";
	$resumen = "Destination Summary";
	$tipohotel = "Hotel type";
	$sector = "Hotel location";
	$fecha1 = "From";
	$fecha11 = "Check in";
	$fecha2 = "To";
	$fecha22 = "Check out";
	$volver = "Back";
	$irapaso = "Go to step";
	$direccion = "address";
	
	$fecha_anulacion = "Penalty free cancellation deadline :";
	
	$fecha_abono = "Requiered payment date :";
	
	
	$tipohab = "Type of room";
	$sin = "Single";
	$dob = "Double Twin";
	$tri = "Double";
	$cua = "Triple";
	
	$deshotel = "Destination and hotel";
	$cat = "Category";
	$serv = "Service";
	$fechaserv = "Date";
	$agregar = "Add";
	
	$disinm = "Instant Confirmation";
	$hotel_nom = "Hotel";
	$disp_hotel = "Quick Check Hotel Availability";
	$disp = "Availability";
	$hotel_noms = "Hotels";
	$val = "Total Reservation Value";
	$valdes = "Destination Value";
	$res = "Book now";
	$solicitar = "Solicitar";
	
	$confirma = "Confirm";
	$detvuelo = "Flight Details";
	$vuelo = "N&deg; Arriving flight";
	$detpas = "Guest detail";
	$pasajero = "Guest";
	$borrarpas = "Remove Guest";
	$ape = "Last name";
	$pasaporte = "Passport number";
	
	$sel_pais = "-= Select Country =-";
	
	$serv_trans_pasajero = "N&deg; of Passengers";
	$serv_trans_nombre1 = "First Name Passenger 1";
	$serv_trans_apellido1 = "Last Name Passenger 1";
	
	$nuevopro = "Create your own package";
	$siguiente = "Next";
	$cancelar = "Cancel";
	
	$otrodest = "Other Destination";
	$servaso = "Additional Services";
	$nochesad = "Additional nights";
	$nochesad_pre = 'PRE-PROGRAM Additional Night Service';
	$nochesad_post = 'POST-PROGRAM Additional Night Service';
	$nochesadi = "Additional nights";
	$nomservaso = "Service Name";
	
	$serv_indi_home = "If you need to combine both services then we invite you to ";
	$serv_hotel = "Hotel Services";
	$serv_trans = "Transportation Services";
	$serv_transconf = "Individual Transportation Service Confirmed";
	$dettrans = "Transportation Detail";
	$transporte = "Transport";
	$crea_serv = "Create transportation service";
	$crea_hot = "BOOK A HOTEL";
	$serv_transor = "Individual Transportation Service ON-REQUEST";
	$serv_transcanu = "Individual Transportation Service Cancelled";
	
	$duracion = "Duration";
	$tipo_pro = "Type of package";
	$nom_prog = "Package Name";
	$estado = "Status";
	$tipo = "Type";
	$ver = "View";
	
	$seleccione = "-= Select =-";
	$comuna ="Location";
	
	$procoti = "Transaction List";
	$descripcion = "Description";
	$habprograma = "Habitaciones para el Programa";
	$servicios = "Add additional services to the package?";
	$cerrarcot = "Finish";
	$perfil = "My Profile";
	$salir = "Quit";
	$olvpass = "Forgot your password?";
	$registrarse = "Register";
	$contacto = "Contact us";
	$user = "User";
	$userCreador="User creator";
	$pass = "Password";
	$pass_cambia = "Change Password";
	$pass_actual = "Your Password";
	$pass_nueva = "New password";
	$pass_nueva_rep = "Repeat new password";
	$tarifas = "Type of Rate";

	$limpiar = "Clean";
	$guardar = "Save";
	$cancelar = "Cancel";
	$mod = "Modify";
	$anu = "Cancel reservation";
	
	$derechos = "All Rights Reserved";
	
	$programaconfirmado = "This package is confirmed and it is not possible to modify it’s data.";
	$sinreserva = "If you don't want to confirm this reservation, it will be saved in '<a href='pack_busca.php'>Transaction List</a>'.";
	$pais_p = "Country";
	
	$buscar = "Search";
	
	$confirma1 = "Thank you for buying trough CTS ONLINE. Your reservation has been confirmed according to the information displayed above. At this moment you should have received an email with a copy of the reservation for your backup, so check your inbox. We remind you that that all CTS common sales, modifications and cancellation policies applies to this reservation";
	$confirma2 = "If you need to modify or cancel your reservation just go to<a href='pack_busca.php'>Transaction List</a>.";
	$confirma3 = "Comments, doubts? Please send us an email to <a href='mailto:cts@distantis.com'>cts@distantis.com</a>.";
	
	$vol_reservar = "Make another reservation";
	
	$servicios_hotel = "BOOK A HOTEL WITHOUT TRANSPORTATION SERVICES";
	$servicios_trans = "BOOK A TRANSPORTATION SERVICES WITHOUT HOTEL";
	
	$quiere_serv = "Would you like to add other services to the package?";
	$quiere_nox = "Would you like to add additional nights?";
	$otro_prog = "Would you like to add other destination to the package?";
	
	$si = "Yes";
	
	$serv_indi = "On-Request Reservation";
	
	$pack_promo = "Special Offers";
	$pack_promo_tt = "View all Special Offers";
	
	$observa = "Observations";
	$nuevo_op = "New Operator";
	$operador = "Wholesaler";
	$correlativo = "File Number";
	
	$anula1 = "We remind you that standard CTS cancellation policy is in place for this reservation.";
	$anula2 = "Said policy indicates that this reservation can be cancelled without penalty until ";
	$anula3 = "Once a reservation is cancelled, it cannot be reactivated. You will therefore have to make a new reservation in said case.";
	$anula4 = "If the deadline for cancellation without penalty has been exceeded, CTS ONLINE will not permit cancellation of the reservation via the platform. In such cases, please contact <a href='mailto:cts@distantis.com'>cts@distantis.com</a>.";
	
	$pol_anula_no = "We´re sorry. The deadline for cancellation without penalty has been exceeded. Please contact <a href='mailto:cts@distantis.com'>cts@distantis.com</a> to request offline cancellation of this reservation.";
	$pol_anula_si = "The reservation has been cancelled succesfully.";
	
	$anulado = "This program has been cancelled";
	$creador = "User";
	
	$request1 = "Thank you for using CTS ONLINE. Due to availability constraints, the services you requested cannot be confirmed automatically and have been placed on ON REQUEST status. Confirmation will only be processed once the hotel requested authorizes avilability.";
	$request21 = "The reservation request was processed at ";
	$request22 = ". As of said time, the requested hotel has a maximum of 24 hours to either accept or deny the reservation request. If the hotel does not accept or deny the request in the alloted time, the request will automatically expire.";
	$request3 = "You will recieve automatic notification via email as soon as the hotel accepts or rejects the reservation request. You can also check reservation status <a href='pack_busca.php'>here</a>.";
	$request4 = "If you have any questions, please contact us at <a href='cts@distantis.com'>cts@distantis.com</a>.";
	$el = "el";
	$agrega_pax = "Add guest";
	$agrega_pax2 = "Add PAX";
	$todos_pax = "For all PAX";
	$numtrans = "Flight number";
	$clickaca = "Click to see promotions ";
	$quetemporada = "what season do you want to view?";
	$serv_hotel = "Hotel bookings";
	
	$valor = "Value";
	$confirmado = "Confirmed";
	
	$tarifa_chile = "Rates apply to foreign tourists not residing in Chile ";
	$dest_rem = "Destination has been removed";
	$sehaencontradodisp="Success! Rooms are available for the requested dates in the hotel you originally booked.";
	$nohaencontradodisp="We?re sorry, rooms are not available for the requested dates in the hotel you originally booked. However, rooms are available for said dates in these alternative hotels. 	";
	$noxtotales="Total nights";
	$total="Total";

	$reserva="Reserve";
	$docDesde="Document sent from";
	$aca="here";

	$tra_or="(SERVICE ON-REQUEST)";

	$just_cuerpo_mail="Reason / Email";
	$aprobar="Approve";

}
?>