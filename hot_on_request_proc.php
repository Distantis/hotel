<?php	 	

include_once("Connections/db1.php");
include_once("Connections/db3.php");
include_once("Connections/db4.php");
include_once("Connections/db5.php");

require('secure.php');

include('Includes/seteo_parametros.php');

require_once('lan/idiomas.php');
require_once('genMails.php');
//echo "Editando radio-usuario: ".$_SESSION['id_empresa']."<br>";

$where="";
$callback="hot_on_request.php?id_cliente=".$id_cliente;
$permiso_log_conf_onrequest=807;
$permiso_log_conf_coti=811;
$permiso_log_recha_coti=810;

$justificacion="";

if(isset($_POST["where"])){
	$where=$_POST["where"];
	$callback="cot_detalle_v2.php?id_cliente=".$id_cliente."&id_cot=".$_POST["id_cot"];
	
	if ($where=="visua"){
		$callback="adm_cot_d.php?id_cliente=".$id_cliente."&id_cot=".$_POST["id_cot"];
	}
	
	$seleccion=1;
	$seleccion2=0;
	$permiso_log_conf_onrequest=1501;
	$permiso_log_conf_coti=1502;
	$justificacion=$_POST["ap_onreq_just"];
	$id_cot=$_POST["id_cot"];
}

$arrayids=explode(",", $_POST['ids']);

for($i=0;$i<count($arrayids)-1;$i++){
	
	$busca_hotel="select id_cotdes,id_cot from cotdes where id_cotdes=$arrayids[$i] and id_hotel=".$id_hotel." and cd_estado=0" ;
	$rs_busca_hotel=$db_query->SelectLimit($busca_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());
	
	$seleccion=$_POST['radio_option_c_'.$arrayids[$i]];
	$seleccion2=$_POST['radio_option_r_'.$arrayids[$i]];
	$trechazo = $_POST['mot_rechazo_'.$arrayids[$i]];
	
	$id_cot=$rs_busca_hotel->Fields('id_cot');
	//EN CASO DE CONFIRMACION

	if($seleccion==1){
		 
		$id_cotdes=$arrayids[$i];
		$updateHotelConfirmado="update cotdes set id_seg=7 where id_cotdes=".$id_cotdes;
 		//echo $updateHotelConfirmado."<br>";
		$db_query->Execute($updateHotelConfirmado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());
		
		//DEJAMOS EN ESTADO 0 TODOS LOS DIAS YA TOMADOS.
		$update_activa="update hotocu set hc_estado=0 where id_cotdes=$id_cotdes and id_cot=$id_cot" ; 
		
		$db_query->Execute($update_activa) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());
		
		$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, now(), %s)",
				$_SESSION['id'], $permiso_log_conf_onrequest, $rs_busca_hotel->Fields('id_cotdes'));			
		//echo $insertSQL1."<br>";
		$Result11 = $db_query->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());

		
		//buscamos si los otros hoteles estan confirmados
		$busca_All_hotel="select COUNT(*) AS cont from cotdes where id_cot=$id_cot and id_seg != 7 and cd_estado=0 and id_cotdespadre=0";
 		//echo $busca_All_hotel;die();
		$listadoHoteles =$db_query->SelectLimit($busca_All_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());

		$n_hoteles=$listadoHoteles->Fields('cont');
		
		if($n_hoteles<1){
			//enviar mail y poner a cot en estado = 7 en caso q todos los hoteles estén confirmados
			$ii="update cot set id_seg=7 where id_cot=$id_cot";
			$db_query->Execute($ii) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());
			//echo $ii."<br>";
			
			$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, now(), %s)",
			$_SESSION['id'], $permiso_log_conf_coti, $arrayids[$i]);					

			$Result11 = $db_query->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());

			$tipoPak_sql="select*from cot where id_cot = ".$id_cot;
			$tipoPak=$db_query->SelectLimit($tipoPak_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());
		//si la cotizacion esta completamente confirmada y es OTSi, entonces se integra
			
			if($esOTSi){
				$sqlOTSiParaIntegraraST="select
				  							id_seg, id_area, cot_estado,
				  							(select count(*) from otsi.cotdes where id_cot = c.id_cot and id_seg <> 7) as cantNoConfCotdes
										from
				  							otsi.cot c
										where
				  							id_cot =".$id_cot;
				$rsParaIntegracion=$db_query->SelectLimit($sqlOTSiParaIntegraraST) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());
				
				if($rsParaIntegracion->RecordCount()>0){ //Si la cotizacion efectivamente esta en OTSi
				
					if($rsParaIntegracion->Fields("id_seg")==7 // Si esta confirmada
						&& $rsParaIntegracion->Fields("cot_estado")==0  // Si no esta anulada
						&& $rsParaIntegracion->Fields("cantNoConfCotdes")==0){ // Si no tiene destinos por confirmar
				
							if($rsParaIntegracion->Fields("id_area")==1){ //Si es receptivo
				
								require_once("../otsi/integradorFileSoptur.php");
								creaFF($db_query, $id_cot, false);
							}elseif ($rsParaIntegracion->Fields("id_area")==2) {
				
								require_once("../otsi/nacional/integradorFileSoptur.php"); //Si es Nacional
								creaFF($db_query, $id_cot, false);
							}
					}
				}

			}
			
			$resultado_op=generaMail_op($db_query,$id_cot,1,true,$esVeciOnRequest,$esOTSi,$esTouravion);
			$resultado_op=generaMail_hot($db_query,$id_cot,1,true,$esVeciOnRequest,$esOTSi,$esTouravion);

// 			die();

		}else{
			//ENVIAMOS MAIL A HOTEL INDIVIDUAL NOTIFICANDO LA CONFIRMACION
			
			generaMail_hot_unit($db_query,$id_cot,1,true,$id_hotel,$esVeciOnRequest,$esOTSi,$esTouravion);
			
		}
	}
	
	//EN CASO DE RECHAZO
	if($seleccion2==1){
	    if($_GET['id_cliente']==4){
		$updateHotelEliminado="update cotdes set cd_multi = 1 where id_cot=".$rs_busca_hotel->Fields('id_cot');
		//echo $updateHotelEliminado."<br>";
		$db_query->Execute($updateHotelEliminado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());
	
		}
	    // nb Se deja todo los destinos en 1 para poder tomar otra vez el hotel rechazado
		//$todo1
		$updateHotelEliminado="update cotdes set id_seg=16 , cd_multi = 0, cd_rechazo=".$trechazo." where id_cotdes=".$rs_busca_hotel->Fields('id_cotdes');
		//echo $updateHotelEliminado."<br>";
		$db_query->Execute($updateHotelEliminado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());
	
		//Eliminamos solicitud completa 
		$del_solicitud="update cot set id_seg=16 where id_cot=$id_cot";
		$db_query->Execute($del_solicitud) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());
		
		$del_solicitud="update hotocu set hc_estado=1 where id_cot=$id_cot";
		$db_query->Execute($del_solicitud) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());
		
		//LOG DE RECHAZO - FMEJIAS 21/08/2014
		$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, now(), %s)",
				$_SESSION['id'], $permiso_log_recha_coti, $rs_busca_hotel->Fields('id_cotdes'));			
		//echo $insertSQL1."<br>";
		$Result11 = $db_query->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());
		
		$resultado_op=generaMail_op($db_query,$id_cot,2,true, $esVeciOnRequest,$esOTSi,$esTouravion);
		$resultado_op=generaMail_hot($db_query,$id_cot,2,true, $esVeciOnRequest,$esOTSi,$esTouravion);
	}
	
	 
	
	//EN CASO DE RECHAZO
	
	
}

echo "<script>window.location='".$callback."'; </script>";
?>


