<?php	 	
include_once("Connections/db1.php");
include_once("Connections/db3.php");

//Aditional Functions
require_once('Includes/functions.inc.php');
require('secure.php');

require('lan/idiomas.php');

require_once('lan/idiomas.php');

// Busca los datos del registro



// end Recordset

$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["edita"]))) {
	if(strtoupper($_POST['pass_nueva']) != strtoupper($_POST['pass_nueva2'])){
    echo "<script>window.alert('- Error en los datos ingresados, contraseña nueva no coincide, intentelo nuevamente.');</script>";
    echo "<script>window.location='disponibilidad.php?id_cliente=".$_GET['id_cliente']."';</script>";
  }	else{
      $switch=0;

      //validacion para cambios e inserts en cts
      if(isset($_SESSION['userid'][1])){
        $data_cts = "select * from usuarios where id_usuario = ".$_SESSION['userid'][1];
        $resp_cts = $db1->SelectLimit($data_cts) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
        if($resp_cts->Fields("usu_password")!= strtoupper($_POST["pass"])){
          $flag_v=false;
        }else{
          $up_cts="update usuarios set usu_password = '".strtoupper($_POST["pass_nueva"])."' where id_usuario = ".$_SESSION['userid'][1];
          $result = $db1->Execute($up_cts) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
          $counter1= $db1->Affected_Rows();
          $switch+=1;
          $insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, now(), %s)",
          $_SESSION['id'], 806, $_SESSION['id']);         
          $Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

        }
      }
    //validacion para cambios e inserts en veci
    if(isset($_SESSION['userid'][3])){
      $data_veci = "select * from usuarios where id_usuario = ".$_SESSION['userid'][3];
      $resp_veci = $db3->SelectLimit($data_veci) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db3->ErrorMsg());
      if($resp_veci->Fields("usu_password")!= strtoupper($_POST["pass"])){
        $flag_c=false;  
      }else{
        $up_veci="update usuarios set usu_password = '".strtoupper($_POST["pass_nueva"])."' where id_usuario = ".$_SESSION['userid'][3];
        $result2 = $db3->Execute($up_veci) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db3->ErrorMsg());
        $counter2 = $db3->Affected_Rows();
        $switch+=2;
        $insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, now(), %s)",
        $_SESSION['id'], 806, $_SESSION['id']);
        $Result11 = $db3->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db3->ErrorMsg());
      }
    }
    switch ($switch) {
      case 1:
        if($counter1>0){
          echo "<script>window.alert('- El cambio se ha realizado con exito. Debe ingresar nuevamente al sistema.');</script>";
          echo "<script>window.location='logout.php';</script>";
        }else{
          echo "<script>window.alert('- Error al intentar cambiar los datos, intentelo nuevamente.')</script>";
          echo "<script>window.location='disponibilidad.php?id_cliente=".$_GET['id_cliente']."';</script>";
        }
        break;
      case 2:
        if($counter2>0){
          echo "<script>window.alert('- El cambio se ha realizado con exito. Debe ingresar nuevamente al sistema.');</script>";
          echo "<script>window.location='logout.php';</script>";
        }else{
          echo "<script>window.alert('- Error al intentar cambiar los datos, intentelo nuevamente.')</script>";
          echo "<script>window.location='disponibilidad.php?id_cliente=".$_GET['id_cliente']."';</script>";
        }
        break;
      case 3:
        if($counter1>0 && $counter2>0){
          echo "<script>window.alert('- El cambio se ha realizado con exito. Debe ingresar nuevamente al sistema.');</script>";
          echo "<script>window.location='logout.php';</script>";
        }else{
          echo "<script>window.alert('- Error al intentar cambiar los datos, intentelo nuevamente.')</script>";
          echo "<script>window.location='disponibilidad.php?id_cliente=".$_GET['id_cliente']."';</script>";
        }

        break;
      
      default:
        # code...
        break;
    }
	}
}


if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["cancela"]))) {
	$insertGoTo="home.php";
	KT_redir($insertGoTo);	
}


?>
<html>
<head>
<title>Distantis - <?= $_SESSION['hot_nombre'] ?></title>
    
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <meta name="keywords" content="turismo, b2b, cts" />
    <meta name="description" content="Gestor de planes de turismo B2B" />
    
    <meta http-equiv="imagetoolbar" content="no" />
    <!--<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />-->
    
    <!-- hojas de estilo -->    
    <link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
    <link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
    
    <link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />

    <!-- scripts varios -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/easy.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

	<script type="text/javascript" src="js/jquery_ui/jquery.blockUI.js"></script>
    
    <!-- LYTEBOX  -->
    <script type="text/javascript" language="javascript" src="lytebox_v5.5/lytebox.js"></script>
	<link rel="stylesheet" href="lytebox_v5.5/lytebox.css" type="text/css" media="screen" />
    
    <link rel="stylesheet" href="js/css/jquery.ui.all.css">
    <script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
    <script src="js/jquery_ui/jquery.ui.core.js"></script>
    <script src="js/jquery_ui/jquery.ui.widget.js"></script>
    <script src="js/jquery_ui/jquery.ui.position.js"></script>
    <script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
    <script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
    <link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<body OnLoad="document.form.pass.focus();">
    <div id="container" class="inner">
<form method="post" id="form" name="form" action="">
  <table align="center" width="500" class="programa">
    <th colspan="2">Cambiar Contrase&ntilde;a</th>
    
    <tr valign="baseline">
      <td width="168" align="left" nowrap><? echo $user;?> :</td>
      <td width="318"><b><? echo strtoupper($_SESSION["logname"]);?></b></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap><? echo $pass_actual;?> :</td>
      <td><input type="password" name="pass" value="" size="30" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap><? echo $pass_nueva;?> :</td>
      <td><input type="password" name="pass_nueva" value="" size="30" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap><? echo $pass_nueva_rep;?> :</td>
      <td><input type="password" name="pass_nueva2" value="" size="30" onChange="M(this)" /></td>
    </tr>
  </table>
 <center>
 	<button name="edita" type="submit" style="width:100px; height:27px">&nbsp;<? echo $guardar;?></button>&nbsp;
    <button name="cancela" type="submit" style="width:100px; height:27px">&nbsp;<? echo $cancelar;?></button>
 </center>
   <input type="hidden" name="MM_update" value="form1" />
</form>
    </div>
</body>
</html>
