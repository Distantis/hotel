<?php	 	 
require_once('Connections/db1.php');
include_once("Connections/db3.php");
include_once("Connections/db4.php");
include_once("Connections/db5.php");

$permiso=801;
require('secure.php');

include('Includes/seteo_parametros.php');
include('Includes/mailing.php');

$query_verifica = "SELECT d.id_hotel, replace(hh.hot_nombre, 'HOTEL','') as hot_nombre FROM stock s INNER JOIN hotdet d ON d.id_hotdet = s.id_hotdet inner join hotel hh on hh.id_hotel = d.id_hotel WHERE s.id_stock = ".$_POST["id_stock_1"];
$verifica = $db_disp->Execute($query_verifica) or die($db_disp->ErrorMsg());

$actualizado="";
$queryInsertLogCambio="";
$sw=0;


if($verifica->Fields('id_hotel') == $id_hotel){


	$cliente_para_email="";
	if($id_cliente=1)
		$cliente_para_email="CTS";
	elseif($id_cliente=3)
		$cliente_para_email="VECI";
	elseif($id_cliente=4)
		$cliente_para_email="OTSi";
	elseif($id_cliente=5)
		$cliente_para_email="Turavion";	

	$query_usuario = "select concat(ifnull(usu_nombre, ''), ' ', ifnull(usu_pat, ''), '(Login=', usu_login, ')') as usuario from usuarios where id_usuario = ".$_SESSION['id'];
	$usuu = $db_disp->Execute($query_usuario) or die($db_disp->ErrorMsg());
	$usuu->MoveFirst();

	$subj="Cambia Dispo ".$cliente_para_email.", HOTEL ".$verifica->Fields('hot_nombre');

	$actualizado="usuario: ".$usuu->Fields("usuario")."<br>".$actualizado;

	if($_POST['txt_dias1'] != '')$sw = 1;if($_POST['txt_dias2'] != '')$sw = 1;if($_POST['txt_dias3'] != '')$sw = 1;if($_POST['txt_dias4'] != '')$sw = 1;if($_POST['txt_min1'] != '')$sw = 1;
	if($sw == 1){
		//echo "todos<br>";
		for ($i=1;$i<=$_POST["s"];$i++) {

			$query_fecha = "SELECT date_format(s.sc_fecha, '%d-%m-%Y') as fechastock, s.sc_fecha FROM stock s  WHERE s.id_stock = ".$_POST["id_stock_".$i];
			$fecha = $db_disp->Execute($query_fecha) or die($db_disp->ErrorMsg());
			$fecha->MoveFirst();
			$actualizado.="<br>";
			$actualizado.="Identificador: ".$_POST["id_stock_".$i]." /  Dia Cambiado: ".$fecha->Fields('fechastock');
			
			$cantSingle="0";
			$cantDobleM="0";
			$cantDobleT="0";
			$cantTriple="0";
			$cantDias="0";

			if($_POST['txt_dias1'] != ''){
				$query = sprintf("update stock set sc_hab1=%s where id_stock = %s", GetSQLValueString($_POST['txt_dias1'], "int"),GetSQLValueString($_POST["id_stock_".$i], "int"));
				//echo $i." : ".$query."<br>";
				$recordset = $db_disp->Execute($query) or die($db_disp->ErrorMsg());
				$actualizado.="; Single = ".$_POST["txt_dias1"];
				$cantSingle=strval($_POST["txt_dias1"]);
			}else{
				$cantSingle="null";
			}
			if($_POST['txt_dias2'] != ''){
				$query = sprintf("update stock set sc_hab2=%s where id_stock = %s", GetSQLValueString($_POST['txt_dias2'], "int"),GetSQLValueString($_POST["id_stock_".$i], "int"));
				//echo $i." : ".$query."<br>";
				$recordset = $db_disp->Execute($query) or die($db_disp->ErrorMsg());
				$actualizado.="; Doble T = ".$_POST["txt_dias2"];
				$cantDobleT=strval($_POST["txt_dias2"]);
			}else{
				$cantDobleT="null";
			}
			if($_POST['txt_dias3'] != ''){
				$query = sprintf("update stock set sc_hab3=%s where id_stock = %s", GetSQLValueString($_POST['txt_dias3'], "int"),GetSQLValueString($_POST["id_stock_".$i], "int"));
				//echo $i." : ".$query."<br>";
				$recordset = $db_disp->Execute($query) or die($db_disp->ErrorMsg());
				$actualizado.="; Doble M = ".$_POST["txt_dias3"];
				$cantDobleM=strval($_POST["txt_dias3"]);
			}else{
				$cantDobleM="null";
			}
			if($_POST['txt_dias4'] != ''){
				$query = sprintf("update stock set sc_hab4=%s where id_stock = %s", GetSQLValueString($_POST['txt_dias4'], "int"),GetSQLValueString($_POST["id_stock_".$i], "int"));
				//echo $i." : ".$query."<br>";
				$recordset = $db_disp->Execute($query) or die($db_disp->ErrorMsg());
				$actualizado.="; Triple = ".$_POST["txt_dias4"];
				$cantTriple=strval($_POST["txt_dias4"]);
			}else{
				$cantTriple="null";
			}
			if($_POST['txt_min1'] != ''){
				$query = sprintf("update stock set sc_minnoche=%s where id_stock = %s", GetSQLValueString($_POST['txt_min1'], "int"),GetSQLValueString($_POST["id_stock_".$i], "int"));
				//echo $i." : ".$query."<br>";
				$recordset = $db_disp->Execute($query) or die($db_disp->ErrorMsg());
				$actualizado.="; Minimo de Noches = ".$_POST["txt_min1"];
				$cantDias=strval($_POST["txt_min1"]);
			}else{
				$cantDias="null";
			}

			$queryInsertLogCambio="insert into disponibilidad_cambio_log (fecha_cambio, id_usuario_cambio, id_stock_cambio, fecha_cambiada, single_cambio, doble_m_cambio, doble_t_cambio, triple_cambio, minimo_noches) values
									(	now(),
										".$_SESSION['id'].",
										".$_POST["id_stock_".$i].",
										'".$fecha->Fields('sc_fecha')."',
										".$cantSingle.",
										".$cantDobleM.",
										".$cantDobleT.",
										".$cantTriple.",
										".$cantDias.")";
		
			$recordsetLogCambio = $db_disp->Execute($queryInsertLogCambio);

			$actualizado.="<br>";
		}
	}else{
		for ($i=1;$i<=$_POST["s"];$i++) {
			$d1 = ($_POST["1_".$i]+$_POST["1d_".$i]);
			$d2 = ($_POST["2_".$i]+$_POST["2d_".$i]);
			$d3 = ($_POST["3_".$i]+$_POST["3d_".$i]);
			$d4 = ($_POST["4_".$i]+$_POST["4d_".$i]);
			//echo $d1."-".$d2."-".$d3."-".$d4."<br>";
			$query = sprintf("
				update stock
				set
				sc_hab1=%s,
				sc_hab2=%s,
				sc_hab3=%s,
				sc_hab4=%s,
				sc_minnoche=%s
				where
				id_stock=%s",
				GetSQLValueString($d1, "int"),
				GetSQLValueString($d2, "int"),
				GetSQLValueString($d3, "int"),
				GetSQLValueString($d4, "int"),
				GetSQLValueString($_POST["min_".$i], "int"),
				GetSQLValueString($_POST["id_stock_".$i], "int")
			);
			//echo $i." : ".$query."<br>";
			$recordset = $db_disp->Execute($query) or die($db_disp->ErrorMsg());

			$query_fecha = "SELECT date_format(s.sc_fecha, '%d-%m-%Y') as fechastock, s.sc_fecha FROM stock s  WHERE s.id_stock = ".$_POST["id_stock_".$i];
			$fecha = $db_disp->Execute($query_fecha) or die($db_disp->ErrorMsg());
			$fecha->MoveFirst();



			$queryInsertLogCambio="insert into disponibilidad_cambio_log (fecha_cambio, id_usuario_cambio, id_stock_cambio, fecha_cambiada, single_cambio, doble_m_cambio, doble_t_cambio, triple_cambio, minimo_noches) values
									(	now(),
										".$_SESSION['id'].",
										".$_POST["id_stock_".$i].",
										'".$fecha->Fields('sc_fecha')."',
										".$d1.",
										".$d2.",
										".$d3.",
										".$d4.",
										".$_POST["min_".$i].")";
		
			$recordsetLogCambio = $db_disp->Execute($queryInsertLogCambio);


			$actualizado.="<br>";
			$actualizado.="Identificador: ".$_POST["id_stock_".$i]." /  Dia Cambiado: ".$fecha->Fields('fechastock');
			$actualizado.="; Single = ".$d1;
			$actualizado.="; Doble T = ".$d2;
			$actualizado.="; Doble M = ".$d3;
			$actualizado.="; Triple = ".$d4;
			$actualizado.="; Minimo de Noches = ".$_POST["min_".$i];
			$actualizado.="<br>";
		}
	}
	$tar = explode(',',$_POST['id_tarifa']);
	foreach($tar as $t1)	{
		$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, Now(), %s)",$_SESSION['id'], 801, $t1);			
		//echo $insertSQL1."<br>";		
		$Result11 = $db_disp->Execute($insertSQL1) or die($db_disp->ErrorMsg());		
	}
    $url = parse_url($_SERVER["REQUEST_URI"]);
	$insertGoTo="disponibilidad.php?".$url['query'];
	//echo $insertGoTo;
	//envio de email cuando un hotel cambia la dispo de una tarifa -> Control para ver que esta pasando
	$actualizado.=$_SERVER["REQUEST_URI"];
	//$ok=envio_mail('juan@distantis.com','juan@distantis.com',$subj,$actualizado,$actualizado);

	KT_redir($insertGoTo);
}else{
	echo "Ud. no puede realizar esta operación.";	
}

?>