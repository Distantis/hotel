<?php	 	 
//Connection statemente
require_once('Connections/db1.php'); 

//Aditional Functions
require_once('Includes/functions.inc.php');

// $permiso=511;
if(!function_exists('estaper')) include_once('secure.php');

require('lan/idiomas.php');


$query_cot = "SELECT *,c.id_operador,
DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde1,
DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta1,
h.hot_nombre as op2,
								(SELECT mon_nombre FROM mon WHERE id_mon = c.id_mon) as moneda
FROM		cot c
INNER JOIN	seg s ON s.id_seg = c.id_seg
INNER JOIN	hotel o ON o.id_hotel = c.id_operador
LEFT JOIN	hotel h ON h.id_hotel = c.id_opcts
WHERE	c.id_cot = ".$id_cot;
// echo $query_cot.'<br>';
$cot = $db1->SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

$anulaciondate = new DateTime($cot->Fields('ha_hotanula'));
$mon_nombre = $cot->Fields ( 'moneda' );
$query_destinos = "
SELECT *,
DATEDIFF(c.cd_fechasta, c.cd_fecdesde) as dias,
DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta1
FROM cotdes c
INNER JOIN hotel h ON h.id_hotel = c.id_hotel
INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad
LEFT JOIN comuna o ON c.id_comuna = o.id_comuna
LEFT JOIN cat a ON c.id_cat = a.id_cat
INNER JOIN tipohabitacion th ON c.id_tipohabitacion = th.id_tipohabitacion
WHERE id_cot = ".$id_cot." AND id_cotdespadre =0 ";
if($cot->Fields('cot_estado')==0) $query_destinos.="AND c.cd_estado = 0 ";
 
 
if ($cot->Fields('cantdes')>"0") $query_destinos = sprintf("%s LIMIT %s", $query_destinos,   $cot->Fields('cantdes'));

// echo $query_destinos."<br>";die();
$destinos = $db1->SelectLimit($query_destinos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

$totalRows_destinos = $destinos->RecordCount();



if(PerteneceCTS($cot->Fields('id_operador'))){ $oper = "Operador :"; $nom_oper = $cot->Fields('op2'); }else{ $oper = "&nbsp;"; $nom_oper = "&nbsp;";}


////////////////////////////////////////////////////////////////////////////////////////////////
////// MAIL AL OPERADOR
////////////////////////////////////////////////////////////////////////////////////////////////


//USUARIO CREADOR
$uC_sql="select*from usuarios where id_usuario=".$cot->Fields('id_usuario');
$uC = $db1->SelectLimit ( $uC_sql ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg ( "ERROR2" ) );

if ($esOtsi) {
	
				$cuerpoop ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<title>OTSi Turismo</title>
				<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
				<!-- hojas de estilo -->
				<link rel="stylesheet" href="http://otsi.distantis.com/otsi/css/easy.css" media="screen, all" type="text/css" />
				<link rel="stylesheet" href="http://otsi.distantis.com/otsi/css/easyprint.css" media="print" type="text/css" />
				<link rel="stylesheet" href="http://otsi.distantis.com/otsi/css/screen-sm.css" media="screen, print, all" type="text/css" />
				</head>
				<body>
				<center>
				<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
				<td width="26%"><img src="http://otsi.distantis.com/otsi/images/LOGOS_WHITE_OTSI.jpg" alt="" width="211" height="70" /><br>
				<center>RESERVA OTSi-ONLINE</center></td>
				<td width="74%"><table align="center" width="95%" class="programa">
				<tr>
				<th colspan="2" align="center" >'.$detvuelo.'</th>
				</tr>
				<tr>
				<td align="left">&nbsp;'.$numpas.' :</td>
				<td colspan="3" >'.$cot->Fields('cot_numpas').'</td>
				</tr>
				<tr>
				<td align="left">COT ID :</td>
				<td colspan="3" >'.$id_cot.'</td>
				</tr>
				<tr>
				<td align="left">COT REF:</td>
				<td colspan="3" >'.$cot->Fields('id_cotref').'</td>
				</tr>
				<tr>
					<td align="left">Status :</td>
					<td colspan="3" >'.$tipoMailEncabezado.'</td>
				 </tr>

				<tr valign="baseline">
				<td>'.$fecha_anulacion.'</td>
				<td>'.$anulaciondate->format('d-m-Y').'</td>
				</tr>
				<tr>
				<td align="left">Tipo :</td>
				<td colspan="3" >'.$tipopackNom.'</td>
				</tr>
				<tr>';
				if(isset($packNom)){
					$cuerpoop.='<tr>
				<td align="left">Nombre Programa :</td>
				<td colspan="3" >'.$packNom.'</td>
				</tr>';
				}
							  	$cuerpoop.='<td align="left">'.$val.' :</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',',')).'</td>
							  </tr>
				<tr>

				<td align="left">Usuario Creador :</td>
				<td colspan="3" >'.$uC->Fields('usu_nombre').' '.$uC->Fields('usu_pat').'</td>
				</tr>
				</table>
				</td>
				</tr>
				</table>';

				if($totalRows_destinos > 0){
					while (!$destinos->EOF) {
						$cuerpoop.='<table width="100%" class="programa">
						<tbody>
						<tr>
						<td colspan="4" bgcolor="#F38800" style="
				            margin: 0;
				                text-transform: uppercase;
				                border-bottom: thin ridge #dfe8ef;
				                padding: 10px;color:#FFFFFF;">'.$resumen.' '.$destinos->Fields('ciu_nombre').'.</th>
						</tr>
						<tr valign="baseline">
						<td width="16%" align="left">'.$hotel_nom.' :</td>
						<td width="36%">'.$destinos->Fields('hot_nombre')." - ".$destinos->Fields('th_nombre').'</td>
						<td>'.$valdes.' :</td>
						<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($destinos->Fields('cd_valor'),1,'.',',')).'</td>
						</tr>
						<tr valign="baseline">
						<td align="left">'.$fecha1.' :</td>
						<td>'.$destinos->Fields('cd_fecdesde1').'</td>
						<td>'.$fecha2.' :</td>
						<td>'.$destinos->Fields('cd_fechasta1').'</td>
						</tr>
						<tr><td colspan="">'.$direccion.':</td><td colspan="">'.$destinos->Fields("hot_direccion").'</td></tr>
						</tbody>
						</table>';
						
						
						//NUEVA CONSULTA PARA SACAR TARIFA DESDE HOTOCU//
						
						$query_tardest="select hc_hab1,hc_hab2,hc_hab3,hc_hab4,
											sum(hc_valor1) as valor1,
											sum(hc_valor2) as valor2,
											sum(hc_valor3) as valor3,
											sum(hc_valor4) as valor4,
											count(*) as noches 
								from
								hotocu
								inner join cotdes on hotocu.id_cotdes = cotdes.id_cotdes
								where hotocu.hc_mod=0 AND  hotocu.id_cotdes=".$destinos->Fields('id_cotdes')." and hotocu.id_cot = $id_cot and cotdes.id_cotdespadre =0 ";
								if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0 ) { 
									$query_tardest.=" and hc_estado=0 and cotdes.cd_estado=0 ";
									
								}
								 $query_tardest.=" GROUP BY hotocu.id_hotdet";
						
						
				// 		echo $query_tardest.'<hr>';$idEncabezado
								$tarDest=$db1->SelectLimit($query_tardest) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
						/////////////////////////////////////////////////
					

						$cuerpoop.='
						<table width="100%" class="programa">
						<tr>
						<th colspan="8" >'.$tipohab.'</th>
						</tr>
						<tr valign="baseline">
						<td>Tipo Habitacion</td>
						<td>Noches Totales</td>
						<td>Valor</td>

						</tr>';
					
						
						while (!$tarDest->EOF) {
							if($tarDest->Fields('hc_hab1')>0){
								$cuerpoop.='<tr valign="baseline">
								<td>'.$tarDest->Fields('hc_hab1').' '.$sin.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor1'),1,'.',',')).'</td>
								</tr>';
							}
							if($tarDest->Fields('hc_hab2')>0){
								$cuerpoop.='<tr valign="baseline">
				 				<td>'.$tarDest->Fields('hc_hab2').' '.$dob.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor2'),1,'.',',')).'</td>
								</tr>';
							}
							if($tarDest->Fields('hc_hab3')>0){
								$cuerpoop.='<tr valign="baseline">
				 				<td>'.$tarDest->Fields('hc_hab3').' '.$tri.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor3'),1,'.',',')).'</td>
								</tr>';
							}
							if($tarDest->Fields('hc_hab4')>0){
								$cuerpoop.='<tr valign="baseline">
				 				<td>'.$tarDest->Fields('hc_hab4').' '.$cua.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor4'),1,'.',',')).'</td>
								</tr>';
							}
							
							
						$tarDest->MoveNext();}$tarDest->MoveFirst();
						
				$query_pasajeros = "
					SELECT * FROM cotpas c
					INNER JOIN pais p ON c.id_pais = p.id_pais
					WHERE c.id_cot = ".$id_cot." AND c.id_cotdes = ".$destinos->Fields('id_cotdes')." and c.cp_estado = 0";
				$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

				$z=1;

				while (!$pasajeros->EOF) {

							$cuerpoop.="
							<table width='100%' class='programa'>
							<tr><td bgcolor='#FF9' style='font: bold 14px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
				            margin: 0;
				                text-transform: uppercase;
				                border-bottom: thin ridge #dfe8ef;
				                padding: 6px;color:#000;'><font color='white'><b>".$detpas." N&deg;".$z."</b></font></td></tr>
							<tr><td>
								<table align='center' width='100%' class='programa'>
								<tr><th colspan='4'></th></tr>
									<tr valign='baseline'>
									  <td width='142' align='left'>".$nombre." :</td>
									  <td width='296'>".$pasajeros->Fields('cp_nombres')."</td>
									  <td width='142'>".$ape." :</td>
									  <td width='287'>".$pasajeros->Fields('cp_apellidos')."</td>
									</tr>
									<tr valign='baseline'>
									  <td>".$pasaporte." :</td>
									  <td>".$pasajeros->Fields('cp_dni')."</td>
									  <td>".$pais_p." :</td>
									  <td>".$pasajeros->Fields('pai_nombre')."</td>
									</tr>
									</table></td></tr>";

							$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
							FROM cotser c
							INNER JOIN trans t ON c.id_trans = t.id_trans
							WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas')." AND cs_estado = 0
							ORDER BY c.cs_fecped";
							$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
							$totalRows_servicios = $servicios->RecordCount();

							if($totalRows_servicios>0){
								$cuerpoop.='<tr>
								<td>
								<table width="100%" class="programa">
								<tr>
								<th colspan="12">'.$servaso.'</th>
								</tr>
								<tr valign="baseline">
								<th align="left" nowrap="nowrap">N&deg;</th>
								<th>'.$nomservaso.'</th>
								<th width="97">'.$fechaserv.'</th>
								<th width="127">'.$numtrans.'</th>
								<th width="161">'.$estado.'</th>
								<th width="161">'.$valor.'</th>
								</tr>';
								$c = 1; 
								$tot=0;
								while (!$servicios->EOF) {
									$tot_tra=0;$tot_exc=0;
									$ser_temp1=NULL;$ser_temp2=NULL;
									$cuerpoop.='
									<tr valign="baseline">
									<td width="56" align="left" nowrap="nowrap">'.$c.'</td>
									<td width="263">'.$servicios->Fields('tra_nombre').'</td>
									<td>'.$servicios->Fields('cs_fecped').'</td>
									<td>'.$servicios->Fields('cs_numtrans').'</td>';
									$cuerpoop.='<td>';
									if($servicios->Fields('id_seg')==7){
										$cuerpoop.=$confirmado;
									}
									else if($servicios->Fields('id_seg')==13){
										$cuerpoop.='On Request';
									}
									$cuerpoop.='</td>';


									$cuerpoop.='<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')).'</td>
									</tr>';
								  if($servicios->Fields('cs_obs')!=''){
										$cuerpoop.='<tr>';
										$cuerpoop.='<td colspan="6" style="font-size:9px;color:#F00;padding-left:80px;">'.$servicios->Fields('cs_obs').'</td>';
										$cuerpoop.='</tr>';
								 	}
									$c++;
									$tot+=$servicios->Fields('cs_valor');
									$servicios->MoveNext();
								}
								$cuerpoop.='<tr valign="baseline">
								<td colspan="5" align="right">TOTAL: </td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tot,1,'.',',')).'</td>
								</tr>
							
								</table>
								</td>
								</tr>';
							}
							$z++;
							$pasajeros->MoveNext();
						}$pasajeros->MoveFirst();


						$cuerpoop.='</table></table>';

						
						$z=1; 

						
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						//////////////////////////////////////////NOCHE ADICIONALES !!!!//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


						$cuerpoop.=  '<tr valign="baseline">
						<td colspan="4">';


				 
						$query_nochead = "
						SELECT *,
						DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
						DATE_FORMAT(cd_fechasta, '%d-%m-%Y') as cd_fecdesde2
						FROM cotdes WHERE id_cot = ".$id_cot." AND id_cotdespadre = ".$destinos->Fields('id_cotdes')." ";
						if($cot->Fields('cot_estado')==0){$query_nochead.=" AND cd_estado = 0 ";}
				// 		echo $query_nochead.'<br>';
						$nochead = $db1->SelectLimit($query_nochead) or die(__LINE__." - ".$db1->ErrorMsg());
						$totalRows_nochead = $nochead->RecordCount();

						if($totalRows_nochead > 0){
							$cuerpoop.='<table width="100%" class="programa">
							<tr>
							<th colspan="8">'.$nochesad.'</th>
							</tr>
							<tbody>
							<tr valign="baseline">
							<th width="141">'.$numpas.'</th>
							<th>'.$fecha1.'</th>
							<th>'.$fecha2.'</th>
							<th>'.$sin.'</th>
							<th>'.$dob.'</th>
							<th align="center">'.$tri.'</th>
							<th width="86" align="center">'.$cua.'</th>
							</tr>';

							$c = 1;
							while (!$nochead->EOF) {

								$cuerpoop.=' <tr valign="baseline">
								<td align="center">'.$nochead->Fields('cd_numpas').'</td>
								<td width="118" align="center">'.$nochead->Fields('cd_fecdesde1').'</td>
								<td width="108" align="center">'.$nochead->Fields('cd_fecdesde2').'</td>
								<td width="88" align="center">'.$nochead->Fields('cd_hab1').'</td>
								<td width="102" align="center">'.$nochead->Fields('cd_hab2').'</td>
								<td width="160" align="center">'.$nochead->Fields('cd_hab3').'</td>
								<td align="center">'.$nochead->Fields('cd_hab4').'</td>
								</tr>';
								$c++;
								$nochead->MoveNext();
							}

							$cuerpoop.=' </tbody>
							</table>';


							$cuerpoop.='   </td>
							</tr>';





							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

							//NUEVA CONSULTA PARA SACAR TARIFA DESDE HOTOCU NOCHE ADICIONAL//
							
							$query_tardest_adi="select hc_hab1,hc_hab2,hc_hab3,hc_hab4,
											sum(hc_valor1) as valor1,
											sum(hc_valor2) as valor2,
											sum(hc_valor3) as valor3,
											sum(hc_valor4) as valor4,
											count(*) as noches
								from
								hotocu
								inner join cotdes on hotocu.id_cotdes = cotdes.id_cotdes
								where hotocu.hc_mod=0 AND hotocu.id_cot = $id_cot and cotdes.id_cotdespadre = ".$destinos->Fields('id_cotdes');
							
							
							if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0){
							$query_tardest_adi.=" and hc_estado=0 and cotdes.cd_estado=0 ";
								
							}
							$query_tardest_adi.=" GROUP BY hotocu.id_hotdet";
							
							
							
							
							
				// 					echo $query_tardest_adi.'<hr>';
							$tarDest_adi=$db1->SelectLimit($query_tardest_adi) or die(__LINE__." - ".$db1->ErrorMsg());
							/////////////////////////////////////////////////

							$cuerpoop.='
							<table width="100%" class="programa">
							<tr>
							<th colspan="8" >'.$tipohab.'</th>
							</tr>
							<tr valign="baseline">
							<td>Tipo Habitacion :</td>
							<td>Noches Totales</td>
							<td>Total</td>
							</tr>';

							$tot_adi+=0;
						while (!$tarDest_adi->EOF) {
							if($tarDest_adi->Fields('hc_hab1')>0){
								$cuerpoop.='<tr valign="baseline">
								<td>'.$tarDest_adi->Fields('hc_hab1').' '.$sin.'</td>
				 				<td>'.$tarDest_adi->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor1'),1,'.',',')).'</td>
								</tr>';
								$tot_adi+=$tarDest_adi->Fields('valor1');
							}
							if($tarDest_adi->Fields('hc_hab2')>0){
								$cuerpoop.='<tr valign="baseline">
				 				<td>'.$tarDest_adi->Fields('hc_hab2').' '.$dob.'</td>
				 				<td>'.$tarDest_adi->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor2'),1,'.',',')).'</td>
								</tr>';
								$tot_adi+=$tarDest_adi->Fields('valor2');
							}
							if($tarDest_adi->Fields('hc_hab3')>0){
								$cuerpoop.='<tr valign="baseline">
				 				<td>'.$tarDest_adi->Fields('hc_hab3').' '.$tri.'</td>
				 				<td>'.$tarDest_adi->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor3'),1,'.',',')).'</td>
								</tr>';
								$tot_adi+=$tarDest_adi->Fields('valor3');
							}
							if($tarDest_adi->Fields('hc_hab4')>0){
								$cuerpoop.='<tr valign="baseline">
				 				<td>'.$tarDest_adi->Fields('hc_hab4').' '.$cua.'</td>
				 				<td>'.$tarDest_adi->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor4'),1,'.',',')).'</td>
								</tr>';
								$tot_adi+=$tarDest_adi->Fields('valor4');
							}
							
							
						$tarDest_adi->MoveNext();}$tarDest_adi->MoveFirst();
						
						
						

							// echo $tothab1;
							$total1234 = $tot_adi;


							$cuerpoop.='
							<tr valign="baseline">
							<td colspan="2">&nbsp;</td>
							<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($total1234,1,'.',',')).'</td>
							</tr>
							</table>';
						}
						$destinos->MoveNext();
					}$destinos->MoveFirst();
				}



				$cuerpoop.='
				<table width="1000" class="programa">
				<tr>
				<th colspan="4">'.$operador.'</th>
				</tr>
				<tr>
				<td width="151" valign="top">N&deg; Correlativo :</td>
				<td width="266">'.$cot->Fields('cot_correlativo').'</td>
				<td width="115">'.$oper.'</td>
				<td width="368">'.$nom_oper.'</td>
				</tr>
				</table>
				<table width="100%" class="programa">
				<tr>
				<th colspan="2">'.$observa.'</th>
				</tr>
				<tr>
				<td width="153" valign="top">'.$observa.' :</td>
				<td width="755">'.$cot->Fields('cot_obs').'</td>
				</tr>
				</table>

				</center>
				<p align="left">
				* '.$confirma_otsi1.'<br>
				* '.$confirma_otsi2.'<br>
				* '.$confirma_otsi3.'<br>
				* '.$tributaria.'
				</p>
				</body>
				</html>
				';
				$cuerpoop.="
				<br>
				<center><font size='1'>
				Documento enviado desde OTSi-ONLINE 2011-2012.
				</center></font>";

				///////////////////////////////////////////////////////////////////////////////////////////
				///////////////////////////////MAIL SIN CSS////////////////////////////////////////////////
				///////////////////////////////////////////////////////////////////////////////////////////
				$cpo_sincss ='
				<body>
				<center>
				<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
				<td width="26%"><img src="http://otsi.distantis.com/otsi/images/LOGOS_WHITE_OTSI.jpg" alt="" width="211" height="70" /><br>
				<center>RESERVA OTSi-ONLINE</center></td>
				<td width="74%"><table align="center" width="95%" class="programa">
				<tr>
				<th colspan="2" align="center" >'.$detvuelo.'</th>
				</tr>
				<tr>
				<td align="left">&nbsp;'.$numpas.' :</td>
				<td colspan="3" >'.$cot->Fields('cot_numpas').'</td>
				</tr>
				<tr>
				<td align="left">COT ID :</td>
				<td colspan="3" >'.$id_cot.'</td>
				</tr>
				<tr>
				<td align="left">COT REF:</td>
				<td colspan="3" >'.$cot->Fields('id_cotref').'</td>
				</tr>
				<tr>
					<td align="left">Status :</td>
					<td colspan="3" >'.$tipoMailEncabezado.'</td>
				 </tr>

				<tr valign="baseline">
				<td>'.$fecha_anulacion.'</td>
				<td>'.$anulaciondate->format('d-m-Y').'</td>
				</tr>
				<tr>
				<td align="left">Tipo :</td>
				<td colspan="3" >'.$tipopackNom.'</td>
				</tr>';
				if(isset($packNom)){
					$cpo_sincss.='<tr>
				<td align="left">Nombre Programa :</td>
				<td colspan="3" >'.$packNom.'</td>
				</tr>';
				}
				$cpo_sincss.='<tr><td align="left">'.$val.' :</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',',')).'</td>
							  </tr>
				<tr>
				<td align="left">Usuario Creador :</td>
				<td colspan="3" >'.$uC->Fields('usu_nombre').' '.$uC->Fields('usu_pat').'</td>
				</tr>
				</table>
				</td>
				</tr>
				</table>';

				if(isset($find_pack2)){
					$cpo_sincss.='<table width="100%" class="programa">
							  <tr>
								<th colspan="2" width="1000">SERVICIOS ADICIONALES</th>
							  </tr>
							  <tr>
								<th>N&ordm;</th>
								<th>Nombre Servicio</th>';
					$cc = 1;
					while (!$find_pack2->EOF) {
						$cpo_sincss.='
										  </tr>
										  <tr>
											<td><center>'.$cc.'</center></td>
											<td align="center">'.$find_pack2->Fields('pd_nombre').'</td>
										  </tr>';
						$cc++;
						$find_pack2->MoveNext();
					}$find_pack2->MoveFirst();
					$cpo_sincss.='</table>';
				}

				if($totalRows_destinos > 0){
					while (!$destinos->EOF) {
						$cpo_sincss.='<table width="100%" class="programa">
						<tbody>
						<tr>
						<th colspan="4" >'.$resumen.' '.$destinos->Fields('ciu_nombre').'.</th>
						</tr>
						<tr valign="baseline">
						<td width="16%" align="left">'.$hotel_nom.' :</td>
						<td width="36%">'.$destinos->Fields('hot_nombre')." - ".$destinos->Fields('th_nombre').'</td>
						<td>'.$valdes.' :</td>
						<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($destinos->Fields('cd_valor'),1,'.',',')).'</td>
						</tr>
						<tr valign="baseline">
						<td align="left">'.$fecha1.' :</td>
						<td>'.$destinos->Fields('cd_fecdesde1').'</td>
						<td>'.$fecha2.' :</td>
						<td>'.$destinos->Fields('cd_fechasta1').'</td>
						</tr>
						<tr><td colspan="">'.$direccion.':</td><td colspan="">'.$destinos->Fields("hot_direccion").'</td></tr>
						</tbody>
						</table>';


						//NUEVA CONSULTA PARA SACAR TARIFA DESDE HOTOCU//

						$query_tardest="select hc_hab1,hc_hab2,hc_hab3,hc_hab4,
											sum(hc_valor1) as valor1,
											sum(hc_valor2) as valor2,
											sum(hc_valor3) as valor3,
											sum(hc_valor4) as valor4,
											count(*) as noches
								from
								hotocu
								inner join cotdes on hotocu.id_cotdes = cotdes.id_cotdes
								where hotocu.hc_mod=0 AND  hotocu.id_cotdes=".$destinos->Fields('id_cotdes')." and hotocu.id_cot = $id_cot and cotdes.id_cotdespadre =0 ";
						if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0 ) {
							$query_tardest.=" and hc_estado=0 and cotdes.cd_estado=0 ";
								
						}
						$query_tardest.=" GROUP BY hotocu.id_hotdet";


						// 		echo $query_tardest.'<hr>';$idEncabezado
						$tarDest=$db1->SelectLimit($query_tardest) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
						/////////////////////////////////////////////////


						$cpo_sincss.='
						<table width="100%" class="programa">
						<tr>
						<th colspan="8" >'.$tipohab.'</th>
						</tr>
						<tr valign="baseline">
						<td>Tipo Habitacion</td>
						<td>Noches Totales</td>
						<td>Valor</td>

						</tr>';


						while (!$tarDest->EOF) {
							if($tarDest->Fields('hc_hab1')>0){
								$cpo_sincss.='<tr valign="baseline">
								<td>'.$tarDest->Fields('hc_hab1').' '.$sin.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor1'),1,'.',',')).'</td>
								</tr>';
							}
							if($tarDest->Fields('hc_hab2')>0){
								$cpo_sincss.='<tr valign="baseline">
				 				<td>'.$tarDest->Fields('hc_hab2').' '.$dob.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor2'),1,'.',',')).'</td>
								</tr>';
							}
							if($tarDest->Fields('hc_hab3')>0){
								$cpo_sincss.='<tr valign="baseline">
				 				<td>'.$tarDest->Fields('hc_hab3').' '.$tri.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor3'),1,'.',',')).'</td>
								</tr>';
							}
							if($tarDest->Fields('hc_hab4')>0){
								$cpo_sincss.='<tr valign="baseline">
				 				<td>'.$tarDest->Fields('hc_hab4').' '.$cua.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor4'),1,'.',',')).'</td>
								</tr>';
							}
								
								
							$tarDest->MoveNext();}$tarDest->MoveFirst();



				$query_pasajeros = "
					SELECT * FROM cotpas c
					INNER JOIN pais p ON c.id_pais = p.id_pais
					WHERE id_cot = ".$id_cot." and id_cotdes = ".$destinos->Fields('id_cotdes')." AND c.cp_estado = 0";
					$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

				$z=1;

				while (!$pasajeros->EOF) {

							$cpo_sincss.="
							<table width='100%' class='programa'>
							<tr><td bgcolor='#F38800' style='font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
						margin: 0;
							text-transform: uppercase;
							border-bottom: thin ridge #dfe8ef;
							padding: 10px; color:#FFFFFF;'><font color='white'><b>".$detpas." N&deg;".$z."</b></font></td></tr>
							<tr><td>
								<table align='center' width='100%' class='programa'>
								<tr><th colspan='4'></th></tr>
										<tr valign='baseline'>
									  <td width='142' align='left'>".$nombre." :</td>
									  <td width='296'>".$pasajeros->Fields('cp_nombres')."</td>
									  <td width='142'>".$ape." :</td>
									  <td width='287'>".$pasajeros->Fields('cp_apellidos')."</td>
									  		</tr>
									  		<tr valign='baseline'>
									  		<td>".$pasaporte." :</td>
									  		<td>".$pasajeros->Fields('cp_dni')."</td>
							<td>".$pais_p." :</td>
									  <td>".$pasajeros->Fields('pai_nombre')."</td>
									</tr>
									</table>";

							$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
							FROM cotser c
							INNER JOIN trans t ON c.id_trans = t.id_trans
							WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas')." AND cs_estado = 0
							ORDER BY c.cs_fecped";
							$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
							$totalRows_servicios = $servicios->RecordCount();

							if($totalRows_servicios>0){
								$cpo_sincss.='<tr>
								<td colspan="4">
								<table width="100%" class="programa">
								<tr>
								<th colspan="12">'.$servaso.'</th>
								</tr>
								<tr valign="baseline">
								<th align="left" nowrap="nowrap">N&deg;</th>
								<th>'.$nomservaso.'</th>
								<th width="97">'.$fechaserv.'</th>
								<th width="127">'.$numtrans.'</th>
								<th width="161">'.$estado.'</th>
									<th width="161">'.$valor.'</th>
									</tr>';
									$c = 1;
									$tot=0;
							while (!$servicios->EOF) {
									$tot_tra=0;$tot_exc=0;
									$ser_temp1=NULL;$ser_temp2=NULL;
									$cpo_sincss.='
									<tr valign="baseline">
											<td width="56" align="left" nowrap="nowrap">'.$c.'</td>
									<td width="263">'.$servicios->Fields('tra_nombre').'</td>
									<td>'.$servicios->Fields('cs_fecped').'</td>
									<td>'.$servicios->Fields('cs_numtrans').'</td>';
								$cpo_sincss.='<td>';
								if($servicios->Fields('id_seg')==7){
								$cpo_sincss.=$confirmado;
								}
								else if($servicios->Fields('id_seg')==13){
								$cpo_sincss.='On Request';
								}
									$cpo_sincss.='</td>';


									$cpo_sincss.='<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')).'</td>
									</tr>';
								  	if($servicios->Fields('cs_obs')!=''){
										$cpo_sincss.='<tr>';
										$cpo_sincss.='<td colspan="6" style="font-size:9px;color:#F00;padding-left:80px;">'.$servicios->Fields('cs_obs').'</td>';
										$cpo_sincss.='</tr>';
								 	}		
														
									$c++;
							
									$tot+=$servicios->Fields('cs_valor');
									$servicios->MoveNext();
							}
										
							$cpo_sincss.='<tr valign="baseline">
								<td colspan="5" align="right">TOTAL: </td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tot,1,'.',',')).'</td>
								</tr>
									
								</table>
								</td>
								</tr>';
							}
							$z++;
							$pasajeros->MoveNext();
						}$pasajeros->MoveFirst();



							///////////////PASAJEROS!!!!!!!!!!!


							$cpo_sincss.='</table>';


							$z=1;


							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							//////////////////////////////////////////NOCHE ADICIONALES !!!!//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


							$cpo_sincss.=  '<tr valign="baseline">
						<td colspan="4">';



							$query_nochead = "
						SELECT *,
						DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
						DATE_FORMAT(cd_fechasta, '%d-%m-%Y') as cd_fecdesde2
						FROM cotdes WHERE id_cot = ".$id_cot." AND id_cotdespadre = ".$destinos->Fields('id_cotdes')." ";
							if($cot->Fields('cot_estado')==0){$query_nochead.=" AND cd_estado = 0 ";}
							// 		echo $query_nochead.'<br>';
							$nochead = $db1->SelectLimit($query_nochead) or die(__LINE__." - ".$db1->ErrorMsg());
							$totalRows_nochead = $nochead->RecordCount();

							if($totalRows_nochead > 0){
								$cpo_sincss.='<table width="100%" class="programa">
							<tr>
							<th colspan="8">'.$nochesad.'</th>
							</tr>
							<tbody>
							<tr valign="baseline">
							<th width="141">'.$numpas.'</th>
							<th>'.$fecha1.'</th>
							<th>'.$fecha2.'</th>
							<th>'.$sin.'</th>
							<th>'.$dob.'</th>
							<th align="center">'.$tri.'</th>
							<th width="86" align="center">'.$cua.'</th>
							</tr>';

								$c = 1;
								while (!$nochead->EOF) {

									$cpo_sincss.=' <tr valign="baseline">
								<td align="center">'.$nochead->Fields('cd_numpas').'</td>
								<td width="118" align="center">'.$nochead->Fields('cd_fecdesde1').'</td>
								<td width="108" align="center">'.$nochead->Fields('cd_fecdesde2').'</td>
								<td width="88" align="center">'.$nochead->Fields('cd_hab1').'</td>
								<td width="102" align="center">'.$nochead->Fields('cd_hab2').'</td>
								<td width="160" align="center">'.$nochead->Fields('cd_hab3').'</td>
								<td align="center">'.$nochead->Fields('cd_hab4').'</td>
								</tr>';
									$c++;
									$nochead->MoveNext();
								}

								$cpo_sincss.=' </tbody>
							</table>';


								$cpo_sincss.='   </td>
							</tr>';





								//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

								//NUEVA CONSULTA PARA SACAR TARIFA DESDE HOTOCU NOCHE ADICIONAL//
									
								$query_tardest_adi="select hc_hab1,hc_hab2,hc_hab3,hc_hab4,
								sum(hc_valor1) as valor1,
								sum(hc_valor2) as valor2,
								sum(hc_valor3) as valor3,
								sum(hc_valor4) as valor4,
								count(*) as noches
								from
								hotocu
								inner join cotdes on hotocu.id_cotdes = cotdes.id_cotdes
								where hotocu.hc_mod=0 AND hotocu.id_cot = $id_cot and cotdes.id_cotdespadre = ".$destinos->Fields('id_cotdes');
									
									
								if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0){
									$query_tardest_adi.=" and hc_estado=0 and cotdes.cd_estado=0 ";

								}
								$query_tardest_adi.=" GROUP BY hotocu.id_hotdet";
									
							$cpo_sincss.='
							<table width="100%" class="programa">
							<tr>
							<th colspan="8" >'.$tipohab.'</th>
							</tr>
							<tr valign="baseline">
							<td>Tipo Habitacion :</td>
								<td>Noches Totales</td>
								<td>Total</td>
								</tr>';
								// 			$total1234= 0; $tothab1=0; $tothab2=0; $tothab3=0; $tothab4=0;

								// 			$habhot1 = $hab->Fields('cd_hab1')." ".$sin;
								// 			$habval1 = " - '.$mon_nombre.' $".round($valhab->Fields('hd_sgl'),1);
								$tot_adi+=0;
								while (!$tarDest_adi->EOF) {
							if($tarDest_adi->Fields('hc_hab1')>0){
									$cpo_sincss.='<tr valign="baseline">
									<td>'.$tarDest_adi->Fields('hc_hab1').' '.$sin.'</td>
									<td>'.$tarDest_adi->Fields('noches').'</td>
									<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor1'),1,'.',',')).'</td>
									</tr>';
								$tot_adi+=$tarDest_adi->Fields('valor1');
							}
							if($tarDest_adi->Fields('hc_hab2')>0){
									$cpo_sincss.='<tr valign="baseline">
									<td>'.$tarDest_adi->Fields('hc_hab2').' '.$dob.'</td>
									<td>'.$tarDest_adi->Fields('noches').'</td>
									<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor2'),1,'.',',')).'</td>
									</tr>';
											$tot_adi+=$tarDest_adi->Fields('valor2');
							}
							if($tarDest_adi->Fields('hc_hab3')>0){
									$cpo_sincss.='<tr valign="baseline">
									<td>'.$tarDest_adi->Fields('hc_hab3').' '.$tri.'</td>
											<td>'.$tarDest_adi->Fields('noches').'</td>
											<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor3'),1,'.',',')).'</td>
											</tr>';
											$tot_adi+=$tarDest_adi->Fields('valor3');
								}
								if($tarDest_adi->Fields('hc_hab4')>0){
								$cpo_sincss.='<tr valign="baseline">
										<td>'.$tarDest_adi->Fields('hc_hab4').' '.$cua.'</td>
				 				<td>'.$tarDest_adi->Fields('noches').'</td>
				 				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor4'),1,'.',',')).'</td>
											</tr>';
											$tot_adi+=$tarDest_adi->Fields('valor4');
							}
								
								
							$tarDest_adi->MoveNext();}$tarDest_adi->MoveFirst();




							// echo $tothab1;
							$total1234 = $tot_adi;


							$cpo_sincss.='
							<tr valign="baseline">
							<td colspan="2">&nbsp;</td>
							<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($total1234,1,'.',',')).'</td>
							</tr>
							</table>';
				}
						$destinos->MoveNext();
					}$destinos->MoveFirst();
				}



				$cpo_sincss.='
				<table width="1000" class="programa">
				<tr>
				<th colspan="4">'.$operador.'</th>
				</tr>
				<tr>
				<td width="151" valign="top">N&deg; Correlativo :</td>
				<td width="266">'.$cot->Fields('cot_correlativo').'</td>
				<td width="115">'.$oper.'</td>
				<td width="368">'.$nom_oper.'</td>
				</tr>
				</table>
				<table width="100%" class="programa">
				<tr>
				<th colspan="2">'.$observa.'</th>
				</tr>
				<tr>
				<td width="153" valign="top">'.$observa.' :</td>
				<td width="755">'.$cot->Fields('cot_obs').'</td>
				</tr>
				</table>

				</center>
				<p align="left">
				* '.$confirma_otsi1.'<br>
				* '.$confirma_otsi2.'<br>
				* '.$confirma_otsi3.'<br>
				* '.$tributaria.'
				</p>
				</body>';


				///////////////////////////////////////////////////////////////////////////////////////////
				///////////////////////////////////////////////////////////////////////////////////////////
				///////////////////////////////////////////////////////////////////////////////////////////


				$subjectop="RESERVA OTSi-ONLINE ID ".$id_cot;

				$message_htmlop='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
				<title>RESERVA OTSi-ONLINE</title>
				<style type="text/css">
				<!--
				body {
				font-family: Arial;
				font-size: 12px;
				background-color: #FFFFFF;
				}
				table {
				font-size: 10px;
				border:0;
				}
				th {
				color:#FFFFFF;
				background-color: #595A7F;
				line-height: normal;
				font-size: 10px;
				}
				tr {
				color: #444444;
				line-height: 15px;
				}
				td {
				font-size: 14px;
				}
				.footer {	font-size: 9px;
				}
				-->
				</style>
				</head>
				<body>'.$cuerpoop.'</body>
				</html>';

				// 	  echo $message_htmlop."<hr>";

				$message_altop='Si ud no puede ver este email por favor contactese con OTSi-ONLINE &copy; 2011-2012';
				require('Includes/mailing.php');

				//echo $mail_to;die();
				//$mail_toop = 'eugenio.allendes@vtsystems.cl';
						if(PerteneceCTS($cot->Fields('id_operador'))){
				 			if($uC->Fields ( 'usu_mail' ) != '')$copy_toop = $uC->Fields ( 'usu_mail' );
						}

				$mail_toop = $distantis_mail_veci;

				if(!PerteneceCTS($cot->Fields('id_operador'))){
					//VERIFICAMOS SI EL OPERADOR TIENE SUPERVISOR: $cot->Fields('id_operador')
					$opSup="select u.id_usuario,us.usu_mail from usuop u
					inner join usuarios us on ( us.id_usuario = u.id_usuario )
					where u.id_hotel =".$cot->Fields('id_operador');
					$rs_opSup = $db1->SelectLimit($opSup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

					if($rs_opSup->RecordCount()>0){
						while (!$rs_opSup->EOF) {
							$mail_toop.=";".$rs_opSup->Fields('usu_mail');
							$rs_opSup->MoveNext();
						}$rs_opSup->MoveFirst();
					}
					if($uC->Fields ( 'usu_mail' ) != '')$copy_toop = $uC->Fields ( 'usu_mail' );
				}
}else{
$cuerpoop ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<title>CTS Turismo</title>
				<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
				<!-- hojas de estilo -->
				<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easy.css" media="screen, all" type="text/css" />
				<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easyprint.css" media="print" type="text/css" />
				<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/screen-sm.css" media="screen, print, all" type="text/css" />
				</head>
				<body>
				<center>
				<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
				<td width="26%"><img src="http://cts.distantis.com/distantis/images/logo-cts.png" alt="" width="211" height="70" /><br>
				<center>RESERVA CTS-ONLINE</center></td>
				<td width="74%"><table align="center" width="95%" class="programa">
				<tr>
				<th colspan="2" align="center" >'.$detvuelo.'</th>
				</tr>
				<tr>
				<td align="left">&nbsp;'.$numpas.' :</td>
				<td colspan="3" >'.$cot->Fields('cot_numpas').'</td>
				</tr>
				<tr>
				<td align="left">COT ID :</td>
				<td colspan="3" >'.$id_cot.'</td>
				</tr>
				<tr>
				<td align="left">COT REF:</td>
				<td colspan="3" >'.$cot->Fields('id_cotref').'</td>
				</tr>
				<tr>
					<td align="left">Status :</td>
					<td colspan="3" >'.$tipoMailEncabezado.'</td>
				 </tr>

				<tr valign="baseline">
				<td>'.$fecha_anulacion.'</td>
				<td>'.$anulaciondate->format('d-m-Y').'</td>
				</tr>
				<tr>
				<td align="left">Tipo :</td>
				<td colspan="3" >'.$tipopackNom.'</td>
				</tr>
				<tr>';
				if(isset($packNom)){
					$cuerpoop.='<tr>
				<td align="left">Nombre Programa :</td>
				<td colspan="3" >'.$packNom.'</td>
				</tr>';
				}
							  	$cuerpoop.='<td align="left">'.$val.' :</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',',')).'</td>
							  </tr>
				<tr>

				<td align="left">Usuario Creador :</td>
				<td colspan="3" >'.$uC->Fields('usu_nombre').' '.$uC->Fields('usu_pat').'</td>
				</tr>
				</table>
				</td>
				</tr>
				</table>';

				if($totalRows_destinos > 0){
					while (!$destinos->EOF) {
						$cuerpoop.='<table width="100%" class="programa">
						<tbody>
						<tr>
						<td colspan="4" bgcolor="#F38800" style="
				            margin: 0;
				                text-transform: uppercase;
				                border-bottom: thin ridge #dfe8ef;
				                padding: 10px;color:#FFFFFF;">'.$resumen.' '.$destinos->Fields('ciu_nombre').'.</th>
						</tr>
						<tr valign="baseline">
						<td width="16%" align="left">'.$hotel_nom.' :</td>
						<td width="36%">'.$destinos->Fields('hot_nombre')." - ".$destinos->Fields('th_nombre').'</td>
						<td>'.$valdes.' :</td>
						<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($destinos->Fields('cd_valor'),1,'.',',')).'</td>
						</tr>
						<tr valign="baseline">
						<td align="left">'.$fecha1.' :</td>
						<td>'.$destinos->Fields('cd_fecdesde1').'</td>
						<td>'.$fecha2.' :</td>
						<td>'.$destinos->Fields('cd_fechasta1').'</td>
						</tr>
						<tr><td colspan="">'.$direccion.':</td><td colspan="">'.$destinos->Fields("hot_direccion").'</td></tr>
						</tbody>
						</table>';
						
						
						//NUEVA CONSULTA PARA SACAR TARIFA DESDE HOTOCU//
						
						$query_tardest="select hc_hab1,hc_hab2,hc_hab3,hc_hab4,
											sum(hc_valor1) as valor1,
											sum(hc_valor2) as valor2,
											sum(hc_valor3) as valor3,
											sum(hc_valor4) as valor4,
											count(*) as noches 
								from
								hotocu
								inner join cotdes on hotocu.id_cotdes = cotdes.id_cotdes
								where hotocu.hc_mod=0 AND  hotocu.id_cotdes=".$destinos->Fields('id_cotdes')." and hotocu.id_cot = $id_cot and cotdes.id_cotdespadre =0 ";
								if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0 ) { 
									$query_tardest.=" and hc_estado=0 and cotdes.cd_estado=0 ";
									
								}
								 $query_tardest.=" GROUP BY hotocu.id_hotdet";
						
						
				// 		echo $query_tardest.'<hr>';$idEncabezado
								$tarDest=$db1->SelectLimit($query_tardest) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
						/////////////////////////////////////////////////
					

						$cuerpoop.='
						<table width="100%" class="programa">
						<tr>
						<th colspan="8" >'.$tipohab.'</th>
						</tr>
						<tr valign="baseline">
						<td>Tipo Habitacion</td>
						<td>Noches Totales</td>
						<td>Valor</td>

						</tr>';
					
						
						while (!$tarDest->EOF) {
							if($tarDest->Fields('hc_hab1')>0){
								$cuerpoop.='<tr valign="baseline">
								<td>'.$tarDest->Fields('hc_hab1').' '.$sin.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor1'),1,'.',',')).'</td>
								</tr>';
							}
							if($tarDest->Fields('hc_hab2')>0){
								$cuerpoop.='<tr valign="baseline">
				 				<td>'.$tarDest->Fields('hc_hab2').' '.$dob.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor2'),1,'.',',')).'</td>
								</tr>';
							}
							if($tarDest->Fields('hc_hab3')>0){
								$cuerpoop.='<tr valign="baseline">
				 				<td>'.$tarDest->Fields('hc_hab3').' '.$tri.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor3'),1,'.',',')).'</td>
								</tr>';
							}
							if($tarDest->Fields('hc_hab4')>0){
								$cuerpoop.='<tr valign="baseline">
				 				<td>'.$tarDest->Fields('hc_hab4').' '.$cua.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor4'),1,'.',',')).'</td>
								</tr>';
							}
							
							
						$tarDest->MoveNext();}$tarDest->MoveFirst();
						
				$query_pasajeros = "
					SELECT * FROM cotpas c
					INNER JOIN pais p ON c.id_pais = p.id_pais
					WHERE c.id_cot = ".$id_cot." AND c.id_cotdes = ".$destinos->Fields('id_cotdes')." and c.cp_estado = 0";
				$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

				$z=1;

				while (!$pasajeros->EOF) {

							$cuerpoop.="
							<table width='100%' class='programa'>
							<tr><td bgcolor='#FF9' style='font: bold 14px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
				            margin: 0;
				                text-transform: uppercase;
				                border-bottom: thin ridge #dfe8ef;
				                padding: 6px;color:#000;'><font color='white'><b>".$detpas." N&deg;".$z."</b></font></td></tr>
							<tr><td>
								<table align='center' width='100%' class='programa'>
								<tr><th colspan='4'></th></tr>
									<tr valign='baseline'>
									  <td width='142' align='left'>".$nombre." :</td>
									  <td width='296'>".$pasajeros->Fields('cp_nombres')."</td>
									  <td width='142'>".$ape." :</td>
									  <td width='287'>".$pasajeros->Fields('cp_apellidos')."</td>
									</tr>
									<tr valign='baseline'>
									  <td>".$pasaporte." :</td>
									  <td>".$pasajeros->Fields('cp_dni')."</td>
									  <td>".$pais_p." :</td>
									  <td>".$pasajeros->Fields('pai_nombre')."</td>
									</tr>
									</table></td></tr>";

							$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
							FROM cotser c
							INNER JOIN trans t ON c.id_trans = t.id_trans
							WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas')." AND cs_estado = 0
							ORDER BY c.cs_fecped";
							$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
							$totalRows_servicios = $servicios->RecordCount();

							if($totalRows_servicios>0){
								$cuerpoop.='<tr>
								<td>
								<table width="100%" class="programa">
								<tr>
								<th colspan="12">'.$servaso.'</th>
								</tr>
								<tr valign="baseline">
								<th align="left" nowrap="nowrap">N&deg;</th>
								<th>'.$nomservaso.'</th>
								<th width="97">'.$fechaserv.'</th>
								<th width="127">'.$numtrans.'</th>
								<th width="161">'.$estado.'</th>
								<th width="161">'.$valor.'</th>
								</tr>';
								$c = 1; 
								$tot=0;
								while (!$servicios->EOF) {
									$tot_tra=0;$tot_exc=0;
									$ser_temp1=NULL;$ser_temp2=NULL;
									$cuerpoop.='
									<tr valign="baseline">
									<td width="56" align="left" nowrap="nowrap">'.$c.'</td>
									<td width="263">'.$servicios->Fields('tra_nombre').'</td>
									<td>'.$servicios->Fields('cs_fecped').'</td>
									<td>'.$servicios->Fields('cs_numtrans').'</td>';
									$cuerpoop.='<td>';
									if($servicios->Fields('id_seg')==7){
										$cuerpoop.=$confirmado;
									}
									else if($servicios->Fields('id_seg')==13){
										$cuerpoop.='On Request';
									}
									$cuerpoop.='</td>';


									$cuerpoop.='<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')).'</td>
									</tr>';
								  if($servicios->Fields('cs_obs')!=''){
										$cuerpoop.='<tr>';
										$cuerpoop.='<td colspan="6" style="font-size:9px;color:#F00;padding-left:80px;">'.$servicios->Fields('cs_obs').'</td>';
										$cuerpoop.='</tr>';
								 	}
									$c++;
									$tot+=$servicios->Fields('cs_valor');
									$servicios->MoveNext();
								}
								$cuerpoop.='<tr valign="baseline">
								<td colspan="5" align="right">TOTAL: </td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tot,1,'.',',')).'</td>
								</tr>
							
								</table>
								</td>
								</tr>';
							}
							$z++;
							$pasajeros->MoveNext();
						}$pasajeros->MoveFirst();


						$cuerpoop.='</table></table>';

						
						$z=1; 

						
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						//////////////////////////////////////////NOCHE ADICIONALES !!!!//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


						$cuerpoop.=  '<tr valign="baseline">
						<td colspan="4">';


				 
						$query_nochead = "
						SELECT *,
						DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
						DATE_FORMAT(cd_fechasta, '%d-%m-%Y') as cd_fecdesde2
						FROM cotdes WHERE id_cot = ".$id_cot." AND id_cotdespadre = ".$destinos->Fields('id_cotdes')." ";
						if($cot->Fields('cot_estado')==0){$query_nochead.=" AND cd_estado = 0 ";}
				// 		echo $query_nochead.'<br>';
						$nochead = $db1->SelectLimit($query_nochead) or die(__LINE__." - ".$db1->ErrorMsg());
						$totalRows_nochead = $nochead->RecordCount();

						if($totalRows_nochead > 0){
							$cuerpoop.='<table width="100%" class="programa">
							<tr>
							<th colspan="8">'.$nochesad.'</th>
							</tr>
							<tbody>
							<tr valign="baseline">
							<th width="141">'.$numpas.'</th>
							<th>'.$fecha1.'</th>
							<th>'.$fecha2.'</th>
							<th>'.$sin.'</th>
							<th>'.$dob.'</th>
							<th align="center">'.$tri.'</th>
							<th width="86" align="center">'.$cua.'</th>
							</tr>';

							$c = 1;
							while (!$nochead->EOF) {

								$cuerpoop.=' <tr valign="baseline">
								<td align="center">'.$nochead->Fields('cd_numpas').'</td>
								<td width="118" align="center">'.$nochead->Fields('cd_fecdesde1').'</td>
								<td width="108" align="center">'.$nochead->Fields('cd_fecdesde2').'</td>
								<td width="88" align="center">'.$nochead->Fields('cd_hab1').'</td>
								<td width="102" align="center">'.$nochead->Fields('cd_hab2').'</td>
								<td width="160" align="center">'.$nochead->Fields('cd_hab3').'</td>
								<td align="center">'.$nochead->Fields('cd_hab4').'</td>
								</tr>';
								$c++;
								$nochead->MoveNext();
							}

							$cuerpoop.=' </tbody>
							</table>';


							$cuerpoop.='   </td>
							</tr>';





							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

							//NUEVA CONSULTA PARA SACAR TARIFA DESDE HOTOCU NOCHE ADICIONAL//
							
							$query_tardest_adi="select hc_hab1,hc_hab2,hc_hab3,hc_hab4,
											sum(hc_valor1) as valor1,
											sum(hc_valor2) as valor2,
											sum(hc_valor3) as valor3,
											sum(hc_valor4) as valor4,
											count(*) as noches
								from
								hotocu
								inner join cotdes on hotocu.id_cotdes = cotdes.id_cotdes
								where hotocu.hc_mod=0 AND hotocu.id_cot = $id_cot and cotdes.id_cotdespadre = ".$destinos->Fields('id_cotdes');
							
							
							if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0){
							$query_tardest_adi.=" and hc_estado=0 and cotdes.cd_estado=0 ";
								
							}
							$query_tardest_adi.=" GROUP BY hotocu.id_hotdet";
							
							
							
							
							
				// 					echo $query_tardest_adi.'<hr>';
							$tarDest_adi=$db1->SelectLimit($query_tardest_adi) or die(__LINE__." - ".$db1->ErrorMsg());
							/////////////////////////////////////////////////

							$cuerpoop.='
							<table width="100%" class="programa">
							<tr>
							<th colspan="8" >'.$tipohab.'</th>
							</tr>
							<tr valign="baseline">
							<td>Tipo Habitacion :</td>
							<td>Noches Totales</td>
							<td>Total</td>
							</tr>';

							$tot_adi+=0;
						while (!$tarDest_adi->EOF) {
							if($tarDest_adi->Fields('hc_hab1')>0){
								$cuerpoop.='<tr valign="baseline">
								<td>'.$tarDest_adi->Fields('hc_hab1').' '.$sin.'</td>
				 				<td>'.$tarDest_adi->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor1'),1,'.',',')).'</td>
								</tr>';
								$tot_adi+=$tarDest_adi->Fields('valor1');
							}
							if($tarDest_adi->Fields('hc_hab2')>0){
								$cuerpoop.='<tr valign="baseline">
				 				<td>'.$tarDest_adi->Fields('hc_hab2').' '.$dob.'</td>
				 				<td>'.$tarDest_adi->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor2'),1,'.',',')).'</td>
								</tr>';
								$tot_adi+=$tarDest_adi->Fields('valor2');
							}
							if($tarDest_adi->Fields('hc_hab3')>0){
								$cuerpoop.='<tr valign="baseline">
				 				<td>'.$tarDest_adi->Fields('hc_hab3').' '.$tri.'</td>
				 				<td>'.$tarDest_adi->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor3'),1,'.',',')).'</td>
								</tr>';
								$tot_adi+=$tarDest_adi->Fields('valor3');
							}
							if($tarDest_adi->Fields('hc_hab4')>0){
								$cuerpoop.='<tr valign="baseline">
				 				<td>'.$tarDest_adi->Fields('hc_hab4').' '.$cua.'</td>
				 				<td>'.$tarDest_adi->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor4'),1,'.',',')).'</td>
								</tr>';
								$tot_adi+=$tarDest_adi->Fields('valor4');
							}
							
							
						$tarDest_adi->MoveNext();}$tarDest_adi->MoveFirst();
						
						
						

							// echo $tothab1;
							$total1234 = $tot_adi;


							$cuerpoop.='
							<tr valign="baseline">
							<td colspan="2">&nbsp;</td>
							<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($total1234,1,'.',',')).'</td>
							</tr>
							</table>';
						}
						$destinos->MoveNext();
					}$destinos->MoveFirst();
				}



				$cuerpoop.='
				<table width="1000" class="programa">
				<tr>
				<th colspan="4">'.$operador.'</th>
				</tr>
				<tr>
				<td width="151" valign="top">N&deg; Correlativo :</td>
				<td width="266">'.$cot->Fields('cot_correlativo').'</td>
				<td width="115">'.$oper.'</td>
				<td width="368">'.$nom_oper.'</td>
				</tr>
				</table>
				<table width="100%" class="programa">
				<tr>
				<th colspan="2">'.$observa.'</th>
				</tr>
				<tr>
				<td width="153" valign="top">'.$observa.' :</td>
				<td width="755">'.$cot->Fields('cot_obs').'</td>
				</tr>
				</table>

				</center>
				<p align="left">
				* '.$confirma1.'<br>
				* '.$confirma2.'<br>
				* '.$confirma3.'<br>
				* '.$tributaria.'
				</p>
				</body>
				</html>
				';
				$cuerpoop.="
				<br>
				<center><font size='1'>
				Documento enviado desde CTS-ONLINE 2011-2012.
				</center></font>";

				///////////////////////////////////////////////////////////////////////////////////////////
				///////////////////////////////MAIL SIN CSS////////////////////////////////////////////////
				///////////////////////////////////////////////////////////////////////////////////////////
				$cpo_sincss ='
				<body>
				<center>
				<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
				<td width="26%"><img src="http://cts.distantis.com/distantis/images/logo-cts.png" alt="" width="211" height="70" /><br>
				<center>RESERVA CTS-ONLINE</center></td>
				<td width="74%"><table align="center" width="95%" class="programa">
				<tr>
				<th colspan="2" align="center" >'.$detvuelo.'</th>
				</tr>
				<tr>
				<td align="left">&nbsp;'.$numpas.' :</td>
				<td colspan="3" >'.$cot->Fields('cot_numpas').'</td>
				</tr>
				<tr>
				<td align="left">COT ID :</td>
				<td colspan="3" >'.$id_cot.'</td>
				</tr>
				<tr>
				<td align="left">COT REF:</td>
				<td colspan="3" >'.$cot->Fields('id_cotref').'</td>
				</tr>
				<tr>
					<td align="left">Status :</td>
					<td colspan="3" >'.$tipoMailEncabezado.'</td>
				 </tr>

				<tr valign="baseline">
				<td>'.$fecha_anulacion.'</td>
				<td>'.$anulaciondate->format('d-m-Y').'</td>
				</tr>
				<tr>
				<td align="left">Tipo :</td>
				<td colspan="3" >'.$tipopackNom.'</td>
				</tr>';
				if(isset($packNom)){
					$cpo_sincss.='<tr>
				<td align="left">Nombre Programa :</td>
				<td colspan="3" >'.$packNom.'</td>
				</tr>';
				}
				$cpo_sincss.='<tr><td align="left">'.$val.' :</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',',')).'</td>
							  </tr>
				<tr>
				<td align="left">Usuario Creador :</td>
				<td colspan="3" >'.$uC->Fields('usu_nombre').' '.$uC->Fields('usu_pat').'</td>
				</tr>
				</table>
				</td>
				</tr>
				</table>';

				if(isset($find_pack2)){
					$cpo_sincss.='<table width="100%" class="programa">
							  <tr>
								<th colspan="2" width="1000">SERVICIOS ADICIONALES</th>
							  </tr>
							  <tr>
								<th>N&ordm;</th>
								<th>Nombre Servicio</th>';
					$cc = 1;
					while (!$find_pack2->EOF) {
						$cpo_sincss.='
										  </tr>
										  <tr>
											<td><center>'.$cc.'</center></td>
											<td align="center">'.$find_pack2->Fields('pd_nombre').'</td>
										  </tr>';
						$cc++;
						$find_pack2->MoveNext();
					}$find_pack2->MoveFirst();
					$cpo_sincss.='</table>';
				}

				if($totalRows_destinos > 0){
					while (!$destinos->EOF) {
						$cpo_sincss.='<table width="100%" class="programa">
						<tbody>
						<tr>
						<th colspan="4" >'.$resumen.' '.$destinos->Fields('ciu_nombre').'.</th>
						</tr>
						<tr valign="baseline">
						<td width="16%" align="left">'.$hotel_nom.' :</td>
						<td width="36%">'.$destinos->Fields('hot_nombre')." - ".$destinos->Fields('th_nombre').'</td>
						<td>'.$valdes.' :</td>
						<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($destinos->Fields('cd_valor'),1,'.',',')).'</td>
						</tr>
						<tr valign="baseline">
						<td align="left">'.$fecha1.' :</td>
						<td>'.$destinos->Fields('cd_fecdesde1').'</td>
						<td>'.$fecha2.' :</td>
						<td>'.$destinos->Fields('cd_fechasta1').'</td>
						</tr>
						<tr><td colspan="">'.$direccion.':</td><td colspan="">'.$destinos->Fields("hot_direccion").'</td></tr>
						</tbody>
						</table>';


						//NUEVA CONSULTA PARA SACAR TARIFA DESDE HOTOCU//

						$query_tardest="select hc_hab1,hc_hab2,hc_hab3,hc_hab4,
											sum(hc_valor1) as valor1,
											sum(hc_valor2) as valor2,
											sum(hc_valor3) as valor3,
											sum(hc_valor4) as valor4,
											count(*) as noches
								from
								hotocu
								inner join cotdes on hotocu.id_cotdes = cotdes.id_cotdes
								where hotocu.hc_mod=0 AND  hotocu.id_cotdes=".$destinos->Fields('id_cotdes')." and hotocu.id_cot = $id_cot and cotdes.id_cotdespadre =0 ";
						if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0 ) {
							$query_tardest.=" and hc_estado=0 and cotdes.cd_estado=0 ";
								
						}
						$query_tardest.=" GROUP BY hotocu.id_hotdet";


						// 		echo $query_tardest.'<hr>';$idEncabezado
						$tarDest=$db1->SelectLimit($query_tardest) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
						/////////////////////////////////////////////////


						$cpo_sincss.='
						<table width="100%" class="programa">
						<tr>
						<th colspan="8" >'.$tipohab.'</th>
						</tr>
						<tr valign="baseline">
						<td>Tipo Habitacion</td>
						<td>Noches Totales</td>
						<td>Valor</td>

						</tr>';


						while (!$tarDest->EOF) {
							if($tarDest->Fields('hc_hab1')>0){
								$cpo_sincss.='<tr valign="baseline">
								<td>'.$tarDest->Fields('hc_hab1').' '.$sin.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor1'),1,'.',',')).'</td>
								</tr>';
							}
							if($tarDest->Fields('hc_hab2')>0){
								$cpo_sincss.='<tr valign="baseline">
				 				<td>'.$tarDest->Fields('hc_hab2').' '.$dob.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor2'),1,'.',',')).'</td>
								</tr>';
							}
							if($tarDest->Fields('hc_hab3')>0){
								$cpo_sincss.='<tr valign="baseline">
				 				<td>'.$tarDest->Fields('hc_hab3').' '.$tri.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor3'),1,'.',',')).'</td>
								</tr>';
							}
							if($tarDest->Fields('hc_hab4')>0){
								$cpo_sincss.='<tr valign="baseline">
				 				<td>'.$tarDest->Fields('hc_hab4').' '.$cua.'</td>
				 				<td>'.$tarDest->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest->Fields('valor4'),1,'.',',')).'</td>
								</tr>';
							}
								
								
							$tarDest->MoveNext();}$tarDest->MoveFirst();



				$query_pasajeros = "
					SELECT * FROM cotpas c
					INNER JOIN pais p ON c.id_pais = p.id_pais
					WHERE id_cot = ".$id_cot." and id_cotdes = ".$destinos->Fields('id_cotdes')." AND c.cp_estado = 0";
					$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

				$z=1;

				while (!$pasajeros->EOF) {

							$cpo_sincss.="
							<table width='100%' class='programa'>
							<tr><td bgcolor='#F38800' style='font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
						margin: 0;
							text-transform: uppercase;
							border-bottom: thin ridge #dfe8ef;
							padding: 10px; color:#FFFFFF;'><font color='white'><b>".$detpas." N&deg;".$z."</b></font></td></tr>
							<tr><td>
								<table align='center' width='100%' class='programa'>
								<tr><th colspan='4'></th></tr>
										<tr valign='baseline'>
									  <td width='142' align='left'>".$nombre." :</td>
									  <td width='296'>".$pasajeros->Fields('cp_nombres')."</td>
									  <td width='142'>".$ape." :</td>
									  <td width='287'>".$pasajeros->Fields('cp_apellidos')."</td>
									  		</tr>
									  		<tr valign='baseline'>
									  		<td>".$pasaporte." :</td>
									  		<td>".$pasajeros->Fields('cp_dni')."</td>
							<td>".$pais_p." :</td>
									  <td>".$pasajeros->Fields('pai_nombre')."</td>
									</tr>
									</table>";

							$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
							FROM cotser c
							INNER JOIN trans t ON c.id_trans = t.id_trans
							WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas')." AND cs_estado = 0
							ORDER BY c.cs_fecped";
							$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
							$totalRows_servicios = $servicios->RecordCount();

							if($totalRows_servicios>0){
								$cpo_sincss.='<tr>
								<td colspan="4">
								<table width="100%" class="programa">
								<tr>
								<th colspan="12">'.$servaso.'</th>
								</tr>
								<tr valign="baseline">
								<th align="left" nowrap="nowrap">N&deg;</th>
								<th>'.$nomservaso.'</th>
								<th width="97">'.$fechaserv.'</th>
								<th width="127">'.$numtrans.'</th>
								<th width="161">'.$estado.'</th>
									<th width="161">'.$valor.'</th>
									</tr>';
									$c = 1;
									$tot=0;
							while (!$servicios->EOF) {
									$tot_tra=0;$tot_exc=0;
									$ser_temp1=NULL;$ser_temp2=NULL;
									$cpo_sincss.='
									<tr valign="baseline">
											<td width="56" align="left" nowrap="nowrap">'.$c.'</td>
									<td width="263">'.$servicios->Fields('tra_nombre').'</td>
									<td>'.$servicios->Fields('cs_fecped').'</td>
									<td>'.$servicios->Fields('cs_numtrans').'</td>';
								$cpo_sincss.='<td>';
								if($servicios->Fields('id_seg')==7){
								$cpo_sincss.=$confirmado;
								}
								else if($servicios->Fields('id_seg')==13){
								$cpo_sincss.='On Request';
								}
									$cpo_sincss.='</td>';


									$cpo_sincss.='<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')).'</td>
									</tr>';
								  	if($servicios->Fields('cs_obs')!=''){
										$cpo_sincss.='<tr>';
										$cpo_sincss.='<td colspan="6" style="font-size:9px;color:#F00;padding-left:80px;">'.$servicios->Fields('cs_obs').'</td>';
										$cpo_sincss.='</tr>';
								 	}		
														
									$c++;
							
									$tot+=$servicios->Fields('cs_valor');
									$servicios->MoveNext();
							}
										
							$cpo_sincss.='<tr valign="baseline">
								<td colspan="5" align="right">TOTAL: </td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tot,1,'.',',')).'</td>
								</tr>
									
								</table>
								</td>
								</tr>';
							}
							$z++;
							$pasajeros->MoveNext();
						}$pasajeros->MoveFirst();



							///////////////PASAJEROS!!!!!!!!!!!


							$cpo_sincss.='</table>';


							$z=1;


							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							//////////////////////////////////////////NOCHE ADICIONALES !!!!//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


							$cpo_sincss.=  '<tr valign="baseline">
						<td colspan="4">';



							$query_nochead = "
						SELECT *,
						DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
						DATE_FORMAT(cd_fechasta, '%d-%m-%Y') as cd_fecdesde2
						FROM cotdes WHERE id_cot = ".$id_cot." AND id_cotdespadre = ".$destinos->Fields('id_cotdes')." ";
							if($cot->Fields('cot_estado')==0){$query_nochead.=" AND cd_estado = 0 ";}
							// 		echo $query_nochead.'<br>';
							$nochead = $db1->SelectLimit($query_nochead) or die(__LINE__." - ".$db1->ErrorMsg());
							$totalRows_nochead = $nochead->RecordCount();

							if($totalRows_nochead > 0){
								$cpo_sincss.='<table width="100%" class="programa">
							<tr>
							<th colspan="8">'.$nochesad.'</th>
							</tr>
							<tbody>
							<tr valign="baseline">
							<th width="141">'.$numpas.'</th>
							<th>'.$fecha1.'</th>
							<th>'.$fecha2.'</th>
							<th>'.$sin.'</th>
							<th>'.$dob.'</th>
							<th align="center">'.$tri.'</th>
							<th width="86" align="center">'.$cua.'</th>
							</tr>';

								$c = 1;
								while (!$nochead->EOF) {

									$cpo_sincss.=' <tr valign="baseline">
								<td align="center">'.$nochead->Fields('cd_numpas').'</td>
								<td width="118" align="center">'.$nochead->Fields('cd_fecdesde1').'</td>
								<td width="108" align="center">'.$nochead->Fields('cd_fecdesde2').'</td>
								<td width="88" align="center">'.$nochead->Fields('cd_hab1').'</td>
								<td width="102" align="center">'.$nochead->Fields('cd_hab2').'</td>
								<td width="160" align="center">'.$nochead->Fields('cd_hab3').'</td>
								<td align="center">'.$nochead->Fields('cd_hab4').'</td>
								</tr>';
									$c++;
									$nochead->MoveNext();
								}

								$cpo_sincss.=' </tbody>
							</table>';


								$cpo_sincss.='   </td>
							</tr>';





								//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

								//NUEVA CONSULTA PARA SACAR TARIFA DESDE HOTOCU NOCHE ADICIONAL//
									
								$query_tardest_adi="select hc_hab1,hc_hab2,hc_hab3,hc_hab4,
								sum(hc_valor1) as valor1,
								sum(hc_valor2) as valor2,
								sum(hc_valor3) as valor3,
								sum(hc_valor4) as valor4,
								count(*) as noches
								from
								hotocu
								inner join cotdes on hotocu.id_cotdes = cotdes.id_cotdes
								where hotocu.hc_mod=0 AND hotocu.id_cot = $id_cot and cotdes.id_cotdespadre = ".$destinos->Fields('id_cotdes');
									
									
								if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0){
									$query_tardest_adi.=" and hc_estado=0 and cotdes.cd_estado=0 ";

								}
								$query_tardest_adi.=" GROUP BY hotocu.id_hotdet";
									
							$cpo_sincss.='
							<table width="100%" class="programa">
							<tr>
							<th colspan="8" >'.$tipohab.'</th>
							</tr>
							<tr valign="baseline">
							<td>Tipo Habitacion :</td>
								<td>Noches Totales</td>
								<td>Total</td>
								</tr>';
								// 			$total1234= 0; $tothab1=0; $tothab2=0; $tothab3=0; $tothab4=0;

								// 			$habhot1 = $hab->Fields('cd_hab1')." ".$sin;
								// 			$habval1 = " - '.$mon_nombre.' $".round($valhab->Fields('hd_sgl'),1);
								$tot_adi+=0;
								while (!$tarDest_adi->EOF) {
							if($tarDest_adi->Fields('hc_hab1')>0){
									$cpo_sincss.='<tr valign="baseline">
									<td>'.$tarDest_adi->Fields('hc_hab1').' '.$sin.'</td>
									<td>'.$tarDest_adi->Fields('noches').'</td>
									<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor1'),1,'.',',')).'</td>
									</tr>';
								$tot_adi+=$tarDest_adi->Fields('valor1');
							}
							if($tarDest_adi->Fields('hc_hab2')>0){
									$cpo_sincss.='<tr valign="baseline">
									<td>'.$tarDest_adi->Fields('hc_hab2').' '.$dob.'</td>
									<td>'.$tarDest_adi->Fields('noches').'</td>
									<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor2'),1,'.',',')).'</td>
									</tr>';
											$tot_adi+=$tarDest_adi->Fields('valor2');
							}
							if($tarDest_adi->Fields('hc_hab3')>0){
									$cpo_sincss.='<tr valign="baseline">
									<td>'.$tarDest_adi->Fields('hc_hab3').' '.$tri.'</td>
											<td>'.$tarDest_adi->Fields('noches').'</td>
											<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor3'),1,'.',',')).'</td>
											</tr>';
											$tot_adi+=$tarDest_adi->Fields('valor3');
								}
								if($tarDest_adi->Fields('hc_hab4')>0){
								$cpo_sincss.='<tr valign="baseline">
										<td>'.$tarDest_adi->Fields('hc_hab4').' '.$cua.'</td>
				 				<td>'.$tarDest_adi->Fields('noches').'</td>
				 				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($tarDest_adi->Fields('valor4'),1,'.',',')).'</td>
											</tr>';
											$tot_adi+=$tarDest_adi->Fields('valor4');
							}
								
								
							$tarDest_adi->MoveNext();}$tarDest_adi->MoveFirst();




							// echo $tothab1;
							$total1234 = $tot_adi;


							$cpo_sincss.='
							<tr valign="baseline">
							<td colspan="2">&nbsp;</td>
							<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($total1234,1,'.',',')).'</td>
							</tr>
							</table>';
				}
						$destinos->MoveNext();
					}$destinos->MoveFirst();
				}



				$cpo_sincss.='
				<table width="1000" class="programa">
				<tr>
				<th colspan="4">'.$operador.'</th>
				</tr>
				<tr>
				<td width="151" valign="top">N&deg; Correlativo :</td>
				<td width="266">'.$cot->Fields('cot_correlativo').'</td>
				<td width="115">'.$oper.'</td>
				<td width="368">'.$nom_oper.'</td>
				</tr>
				</table>
				<table width="100%" class="programa">
				<tr>
				<th colspan="2">'.$observa.'</th>
				</tr>
				<tr>
				<td width="153" valign="top">'.$observa.' :</td>
				<td width="755">'.$cot->Fields('cot_obs').'</td>
				</tr>
				</table>

				</center>
				<p align="left">
				* '.$confirma1.'<br>
				* '.$confirma2.'<br>
				* '.$confirma3.'<br>
				* '.$tributaria.'
				</p>
				</body>';


				///////////////////////////////////////////////////////////////////////////////////////////
				///////////////////////////////////////////////////////////////////////////////////////////
				///////////////////////////////////////////////////////////////////////////////////////////


				$subjectop="RESERVA CTS-ONLINE ID ".$id_cot;

				$message_htmlop='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
				<title>RESERVA CTS-ONLINE</title>
				<style type="text/css">
				<!--
				body {
				font-family: Arial;
				font-size: 12px;
				background-color: #FFFFFF;
				}
				table {
				font-size: 10px;
				border:0;
				}
				th {
				color:#FFFFFF;
				background-color: #595A7F;
				line-height: normal;
				font-size: 10px;
				}
				tr {
				color: #444444;
				line-height: 15px;
				}
				td {
				font-size: 14px;
				}
				.footer {	font-size: 9px;
				}
				-->
				</style>
				</head>
				<body>'.$cuerpoop.'</body>
				</html>';

				// 	  echo $message_htmlop."<hr>";

				$message_altop='Si ud no puede ver este email por favor contactese con CTS-ONLINE &copy; 2011-2012';
				require('Includes/mailing.php');

				//echo $mail_to;die();
				//$mail_toop = 'eugenio.allendes@vtsystems.cl';
						if(PerteneceCTS($cot->Fields('id_operador'))){
				 			if($uC->Fields ( 'usu_mail' ) != '')$copy_toop = $uC->Fields ( 'usu_mail' );
						}

				if($esVeciOnRequest){
					$mail_toop=$distantis_mail_veci;
				}elseif ($esOtsi) {	
					$mail_toop=$distantis_mail_otsi;
				}elseif ($esTouravion) {
					$mail_toop = $distantis_mail;	
				}else{
					//Si es de CTS agrega estos mail. FMEJIAS 04/04/2014
					$mail_toop=$distantis_mail;
					$mail_toop.=";mreyes@ctsturismo.cl;francisco.mejias@ctsturismo.cl;respaldo.mail.cts@gmail.com";
				}

				if(!PerteneceCTS($cot->Fields('id_operador'))){
					//VERIFICAMOS SI EL OPERADOR TIENE SUPERVISOR: $cot->Fields('id_operador')
					$opSup="select u.id_usuario,us.usu_mail from usuop u
					inner join usuarios us on ( us.id_usuario = u.id_usuario )
					where u.id_hotel =".$cot->Fields('id_operador');
					$rs_opSup = $db1->SelectLimit($opSup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

					if($rs_opSup->RecordCount()>0){
						while (!$rs_opSup->EOF) {
							$mail_toop.=";".$rs_opSup->Fields('usu_mail');
							$rs_opSup->MoveNext();
						}$rs_opSup->MoveFirst();
					}
					if($uC->Fields ( 'usu_mail' ) != '')$copy_toop = $uC->Fields ( 'usu_mail' );
				}

}


	 ?>