<?php	 	
//Connection statemente
require_once('Connections/db1.php');
include_once("Connections/db3.php");
include_once("Connections/db4.php");
include_once("Connections/db5.php");

//Aditional Functions
require_once('Includes/functions.inc.php');

//$permiso=706;
require('secure.php');
include('Includes/seteo_parametros.php');

require_once('lan/idiomas.php');
require_once('Includes/Control.php');

$query_cot = "SELECT 	*,
							DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde1,
							DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta1,
							h.hot_nombre as op2
							FROM		cot c
							INNER JOIN	seg s ON s.id_seg = c.id_seg
							INNER JOIN	hotel o ON o.id_hotel = c.id_operador
							LEFT JOIN	hotel h ON h.id_hotel = c.id_opcts
							WHERE	c.id_cot = ".$_GET['id_cot'];
$cot = $db_query->SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db_query->ErrorMsg());

$mon_nombre = MonedaNombre($db_query,$cot->Fields('id_mon'));

$query_destinos = "
	SELECT *,
			DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde,
			DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta,
			i.id_ciudad as id_ciudad
	FROM cotdes c 
	INNER JOIN hotel h ON h.id_hotel = c.id_hotel
	INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad 
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	LEFT JOIN cat a ON c.id_cat = a.id_cat
	WHERE id_cot = ".$_GET['id_cot']." AND c.id_cotdespadre = 0 AND c.cd_estado = 0 AND c.id_hotel = ".$id_hotel;
$destinos = $db_query->SelectLimit($query_destinos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db_query->ErrorMsg());
$totalRows_destinos = $destinos->RecordCount();

$query_pasajeros = "
	SELECT * FROM cotpas c 
	INNER JOIN pais p ON c.id_pais = p.id_pais
	INNER JOIN cotdes d ON c.id_cotdes = d.id_cotdes
	WHERE c.id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0 and d.id_hotel = ".$id_hotel;
$pasajeros = $db_query->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db_query->ErrorMsg());
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Distantis - <?= $_SESSION['hot_nombre'] ?></title>
    
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <meta name="keywords" content="turismo, b2b, cts" />
    <meta name="description" content="Gestor de planes de turismo B2B" />
    
    <meta http-equiv="imagetoolbar" content="no" />
    <!--<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />-->
    
    <!-- hojas de estilo -->    
    <link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
    <link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
    
    <link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />

    <!-- scripts varios -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/easy.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

	<script type="text/javascript" src="js/jquery_ui/jquery.blockUI.js"></script>
    
    <!-- LYTEBOX  -->
    <script type="text/javascript" language="javascript" src="lytebox_v5.5/lytebox.js"></script>
	<link rel="stylesheet" href="lytebox_v5.5/lytebox.css" type="text/css" media="screen" />
    
	<link rel="stylesheet" href="js/css/jquery.ui.all.css">
	<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
	<script src="js/jquery_ui/jquery.ui.core.js"></script>
	<script src="js/jquery_ui/jquery.ui.widget.js"></script>
	<script src="js/jquery_ui/jquery.ui.position.js"></script>
	<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
	<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
	<script src="js/jquery_ui/jquery.ui.tabs.js"></script>
	<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
</head>

<body>
    <div id="container" class="inner">
      <div id="header">
			<div style="float:right;margin-right:5px;">
				<div>
					<div style="float:left;margin-right:10px;">
						<?php	 	 include('nav-auxiliar.php'); ?>
					</div>
					<div style="float:right;">
						<?php	 	 	
							echo cbbCliente($db1, $id_hotel, $id_cliente);
							echo cbbHoteles($db_query, $_SESSION['id'], $id_hotel, $id_cliente); 
						?>
					</div>
				</div>
			</div>

			
            <h1 style="background: url(images/<? echo $_SESSION['logo'][$id_cliente]; ?>) no-repeat 0 0;">Distantis</h1>
            <ol id="pasos">
                <li class="paso1"><a href="disponibilidad.php?id_cliente=<?php	 	 echo $id_cliente ?>&MultiHotel=<?php	 	 echo $id_hotel ?>" title="Gestionar Disponibilidad">DISPONIBILIDAD</a></li>
                <li class="paso2"><a href="hot_conanu.php?id_cliente=<?php	 	 echo $id_cliente ?>&MultiHotel=<?php	 	 echo $id_hotel ?>" title="Reporte">REPORTES</a></li>
				<li class="paso3 activo"><a href="hot_produccion.php?id_cliente=<?php	 	 echo $id_cliente ?>&MultiHotel=<?php	 	 echo $id_hotel ?>" title="Informe">INFORME</a></li>
                <li class="paso4"><a href="hot_on_request.php?id_cliente=<?php	 	 echo $id_cliente ?>&MultiHotel=<?php	 	 echo $id_hotel ?>" title="Informe">ON REQUEST</a></li>
                <?php	 	
					if($id_cliente==1){
				?>
                	<li class="paso5"><a href="hot_nueva_tarifa.php?id_cliente=<?php	 	 echo $id_cliente ?>&MultiHotel=<?php	 	 echo $id_hotel ?>" title="Informe">NUEVA TARIFA</a></li>
                	<li class="paso6"><a href="ayuda.php?id_cliente=<?php	 	 echo $id_cliente ?>&MultiHotel=<?php	 	 echo $id_hotel ?>" title="Ayuda">Ayuda</a></li>
                <?php	 	
					}else{
				?>
	                <li class="paso5"><a href="ayuda.php?id_cliente=<?php	 	 echo $id_cliente ?>&MultiHotel=<?php	 	 echo $id_hotel ?>" title="Ayuda">Ayuda</a></li>
                <?php	 	
					}
				?>
			</ol>														   
        </div>
        <table width="100%" class="pasos">
          <tr valign="baseline">
            <td width="500" align="left">DETALLE PROGRAMA COT <? echo $_GET['id_cot'];?></td>
			<td width="500" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='hot_produccion.php?id_cliente=<?php	 	 echo $id_cliente ?>&MultiHotel=<?php	 	 echo $id_hotel ?>';">&nbsp;<? echo $volver;?></button></td>
          </tr>
        </table>
<table width="1000" class="programa">
	<tr>
    	<th colspan="2" width="1000" align="center"><?= $detvuelo ?> - <?= $pasajero ?></th>
    </tr>
<?
$z = 1;

while ( ! $pasajeros->EOF ) { ?>
	<tr valign="baseline">
		<td align="left" nowrap="nowrap" ><?= $pasajero ?> <?= $z ?></td>
		<td class="nombreusuario"><?= $pasajeros->Fields('cp_apellidos') ?>, <?= $pasajeros->Fields('cp_nombres') ?> (<?= $pasajeros->Fields('cp_dni') ?>)</td>
	</tr>
<?
	$z ++;
	$pasajeros->MoveNext ();
}
?>
</table>
<?
if ($totalRows_destinos > 0) {
	while (!$destinos->EOF){ ?>
<table width="920" class="programa">
	<tbody>
		<tr>
        	<th colspan="4"><?= $resumen ?> <?= $destinos->Fields('ciu_nombre') ?></th>
		</tr>
		<tr valign="baseline">
			<td width="110" align="left"><?= $hotel_nom ?></td>
			<td width="290"><?= $destinos->Fields('hot_nombre') ?></td>
			<td width="110" align="left"></td>
			<td width="290"></td>
	  </tr>
		<tr valign="baseline">
			<td align="left"><?= $fecha1 ?> :</td>
			<td><?= $destinos->Fields('cd_fecdesde') ?></td>
			<td><?= $fecha2 ?> :</td>
			<td><?= $destinos->Fields('cd_fechasta') ?></td>
		</tr>
<?
$coti = $_GET['id_cot'];
$query_valhab = "SELECT hd_sgl, hd_dbl, hd_tpl,
		DATEDIFF(cd_fechasta, cd_fecdesde) as pac_n, tt_nombre, count(*) as noches, th.th_nombre
	FROM cotdes c
	INNER JOIN hotocu o ON c.id_cotdes = o.id_cotdes
	INNER JOIN hotdet h ON o.id_hotdet = h.id_hotdet
	INNER JOIN tipotarifa t ON h.id_tipotarifa = t.id_tipotarifa
	INNER JOIN tipohabitacion th on h.id_tipohabitacion = th.id_tipohabitacion
	WHERE o.hc_mod=0  AND c.id_cotdes =  ".$destinos->Fields('id_cotdes');
if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0 ) $query_valhab.=" AND o.hc_estado = 0 AND c.cd_estado=0 ";
$query_valhab.=" AND c.id_cotdespadre=0 and o.id_cot = $coti GROUP BY o.id_hotdet";
//echo $query_valhab.'<br>';
$valhab = $db_query->SelectLimit( $query_valhab ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg () );

if ($destinos->Fields('cd_hab1') > 0) {
	$habitacion = $destinos->Fields('cd_hab1')." ".$sin." - ";
}
if ($destinos->Fields('cd_hab2') > 0) {
	$habitacion .= $destinos->Fields('cd_hab2') . " " . $dob . " - ";
}
if ($destinos->Fields('cd_hab3') > 0) {
	$habitacion .= $destinos->Fields('cd_hab3') . " " . $tri . " - ";
}
if ($destinos->Fields('cd_hab4') > 0) {
	$habitacion .= $destinos->Fields('cd_hab4') . " " . $cua;
}
?>
		<tr>
        	<td colspan="4">
            	<table width="100%" class="programa">
                	<tr>
                    	<th colspan="5" ><?= $tipohab ?></th>
					</tr>
					<tr valign="baseline">
						<td width="177">Tipo Habitacion</td>
						<td width="177">Tipo Tarifa</td>
						<td width="120">Tarifa Habitacion</td>
						<td width="177">Noches Totales</td>
						<td width="177">Total</td>
					</tr>
<?
$tothab1 = 0;
$tothab2 = 0;
$tothab3 = 0;
$tothab4 = 0;

while(!$valhab->EOF) {
	if ($destinos->Fields('cd_hab1') > 0) {
		$hab1 = (($valhab->Fields('hd_sgl') * 1) * $destinos->Fields('cd_hab1')) * $valhab->Fields('noches');
		$val=round($valhab->Fields('hd_sgl'), 0 );
?>
					<tr valign="baseline">
                    	<td><?= $destinos->Fields('cd_hab1') . ' ' . $sin . ' - '. $valhab->Fields('th_nombre') ?></td>
						<td><?= $valhab->Fields('tt_nombre') ?></td>
						<td><?= $mon_nombre ?> $<?= $val ?></td>
						<td><?= $valhab->Fields('noches') ?></td>
						<td><?= $mon_nombre ?> $<?= round($hab1,0) ?></td>
					</tr>
<?
		$tothab1 += (($valhab->Fields('hd_sgl') * 1) * $destinos->Fields('cd_hab1')) * $valhab->Fields('noches');
	}
	
	if ($destinos->Fields('cd_hab2') > 0) {
		$hab2 = (($valhab->Fields('hd_dbl')*2) * $destinos->Fields('cd_hab2')) * $valhab->Fields('noches');
		$val = round($valhab->Fields('hd_dbl')*2,0);
?>
					<tr valign="baseline">
                    	<td><?= $destinos->Fields('cd_hab2') . ' ' . $dob . ' - '. $valhab->Fields('th_nombre') ?></td>
						<td><?= $valhab->Fields('tt_nombre') ?></td>
						<td><?= $mon_nombre ?> $<?= $val ?></td>
						<td><?= $valhab->Fields('noches') ?></td>
                        <td><?= $mon_nombre ?> $<?= round($hab2,0) ?></td>
					</tr>
<?
		$tothab2 += (($valhab->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab2')) * $valhab->Fields('noches');
	}
	if ($destinos->Fields('cd_hab3') > 0) {
		$hab3 = (($valhab->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab3')) * $valhab->Fields('noches');
		$val=round ( $valhab->Fields('hd_dbl') * 2, 0 );
?>
					<tr valign="baseline">
						<td><?= $destinos->Fields('cd_hab3') . ' ' . $tri . ' - '. $valhab->Fields('th_nombre') ?></td>
						<td><?= $valhab->Fields('tt_nombre') ?></td>
						<td><?= $mon_nombre ?> $<?= $val ?></td>
						<td><?= $valhab->Fields('noches') ?></td>
						<td><?= $mon_nombre ?> $<?= round($hab3,0) ?></td>
				</tr>
<?
		$tothab3 += (($valhab->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab3')) * $valhab->Fields('noches');
	}
	
	if ($destinos->Fields('cd_hab4') > 0) {
		$hab4 = (($valhab->Fields('hd_tpl') * 3) * $destinos->Fields('cd_hab4')) * $valhab->Fields('noches');
		$val=round ( $valhab->Fields('hd_tpl') * 3, 0 );
?>
					<tr valign="baseline">
						<td><?= $destinos->Fields('cd_hab4') . ' ' . $cua . ' - '. $valhab->Fields('th_nombre') ?></td>
						<td><?= $valhab->Fields('tt_nombre') ?></td>
						<td><?= $mon_nombre ?> $<?= $val ?></td>
						<td><?= $valhab->Fields('noches') ?></td>
						<td><?= $mon_nombre ?> $<?= round($hab4,0) ?></td>
					</tr>
<?
		$tothab4 += (($valhab->Fields('hd_tpl') * 3) * $destinos->Fields('cd_hab4')) * $valhab->Fields('noches');
	}
	
	$valhab->MoveNext ();
}
$total1234 = $tothab1 + $tothab2 + $tothab3 + $tothab4;
?>
					<tr valign="baseline">
						<td colspan="4">&nbsp;</td>
						<td><?= $mon_nombre ?> $<?= round($total1234,0) ?></td>
					</tr>
				</table>
<?

$nochead = ConsultaNoxAdi($db_query,$id_cot,$destinos->Fields('id_cotdes'));
$totalRows_nochead = $nochead->RecordCount();

if($totalRows_nochead>0){

?>
            	<table width="100%" class="programa">
                	<tr>
						<th colspan="8"><?= $nochesad ?></th>
					</tr>
					<tbody>
                    <tr valign="baseline">
                    	<th width="141"><?= $numpas?></th>
                        <th><?= $fecha1?></th>
						<th><?= $fecha2?></th>
						<th><?= $sin?></th>
						<th><?= $dob?></th>
						<th align="center"><?= $tri?></th>
						<th width="86" align="center"><?= $cua?></th>
						<th><?= $valor?></th>
					</tr>
<?
	$c = 1;
	while (!$nochead->EOF) {
		$query_valhab_adi = "SELECT hd_sgl, hd_dbl, hd_tpl,
				DATEDIFF(cd_fechasta, cd_fecdesde) as pac_n, tt_nombre, count(*) as noches, th.th_nombre
			FROM cotdes c
			INNER JOIN hotocu o ON c.id_cotdes = o.id_cotdes
			INNER JOIN hotdet h ON o.id_hotdet = h.id_hotdet
			INNER JOIN tipotarifa t ON h.id_tipotarifa = t.id_tipotarifa
			INNER JOIN tipohabitacion th on h.id_tipohabitacion = th.id_tipohabitacion
			WHERE o.hc_mod=0  AND c.id_cotdes =  ".$nochead->Fields('id_cotdes');
		if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0)
		$query_valhab_adi.=" AND o.hc_estado = 0 AND c.cd_estado=0";
		$query_valhab_adi.=" AND c.id_cotdespadre!=0 and o.id_cot=$id_cot GROUP BY o.id_hotdet";
		
		$valhab_adi = $db_query->SelectLimit($query_valhab_adi) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db_query->ErrorMsg());
		
		$tothab1 = 0;$tothab2 = 0;$tothab3 = 0;$tothab4 = 0;
		
		while(!$valhab_adi->EOF){
			if ($destinos->Fields('cd_hab1') > 0) {
				$hab1 = (($valhab_adi->Fields('hd_sgl') * 1) * $destinos->Fields('cd_hab1')) * $valhab_adi->Fields('noches');
				$val=round ( $valhab_adi->Fields('hd_sgl'), 0 );
				$tothab1 += (($valhab_adi->Fields('hd_sgl') * 1) * $destinos->Fields('cd_hab1')) * $valhab_adi->Fields('noches');
			}

			if ($destinos->Fields('cd_hab2') > 0) {
				$hab2 = (($valhab_adi->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab2')) * $valhab_adi->Fields('noches');
				$val=round ( $valhab_adi->Fields('hd_dbl') * 2, 0 );
				$tothab2 += (($valhab_adi->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab2')) * $valhab_adi->Fields('noches');
			}
			
			if ($destinos->Fields('cd_hab3') > 0) {
				$hab3 = (($valhab_adi->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab3')) * $valhab_adi->Fields('noches');
				$val=round ( $valhab_adi->Fields('hd_dbl') * 2, 0 );
				$tothab3 += (($valhab->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab3')) * $valhab->Fields('noches');
			}
			
			if ($destinos->Fields('cd_hab4') > 0) {
				$hab4 = (($valhab->Fields('hd_tpl') * 3) * $destinos->Fields('cd_hab4')) * $valhab->Fields('noches');
				$val=round ( $valhab->Fields('hd_tpl') * 3, 0 );
				$tothab4 += (($valhab_adi->Fields('hd_tpl') * 3) * $destinos->Fields('cd_hab4')) * $valhab_adi->Fields('noches');
			}
			$valhab_adi->MoveNext();
		}
		$total1234 = $tothab1 + $tothab2 + $tothab3 + $tothab4;
?>
					<tr valign="baseline">
                    	<td align="center"><?= $nochead->Fields('cd_numpas')?></td>
                        <td width="118" align="center"><?= $nochead->Fields('cd_fecdesde1')?></td>
                        <td width="108" align="center"><?= $nochead->Fields('cd_fechasta1')?></td>
                        <td width="88" align="center"><?= $nochead->Fields('cd_hab1')?></td>
                        <td width="102" align="center"><?= $nochead->Fields('cd_hab2')?></td>
                        <td width="160" align="center"><?= $nochead->Fields('cd_hab3')?></td>
                        <td align="center"><?= $nochead->Fields('cd_hab4')?></td>
                        <td width="60"><?= $mon_nombre ?> $<?= $total1234?></td>
					</tr>
<?
		$nochead->MoveNext();
		}
?>
					</tbody>
			  </table>
		  </td>
		</tr>
	</tbody>
</table>
<?
}
$hab1 = '';$hab2 = '';$hab3 = '';$hab4 = '';
$destinos->MoveNext ();
}
}
?>
    </div>
</body>
</html>
