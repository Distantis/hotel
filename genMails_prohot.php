<?php	 	 
// require_once('Connections/db1.php');
require_once ('Includes/mailing.php');
require_once ('Includes/Control.php');

if(!function_exists('estaper')) include_once('secure.php');
require('lan/idiomas.php');



// ///////////////////MAIL AL HOTEL///////////////////////////////////////
 
// $id_cot=14253;
// $id_prueba=1;

$query_cot = "SELECT 	*,usu.usu_mail,
								(p.pac_cantdes-1) as cantdes,
								c.cot_numvuelo as cot_numvuelo,
								DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde1,
								DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta1,
								h.hot_nombre as op2,
								c.id_operador as id_operador,
								c.id_opcts as id_opcts,
								(SELECT mon_nombre FROM mon WHERE id_mon = c.id_mon) as moneda
								FROM		cot c
								INNER JOIN	seg s ON s.id_seg = c.id_seg
								INNER JOIN	hotel o ON o.id_hotel = c.id_operador
								LEFT JOIN	hotel h ON h.id_hotel = c.id_opcts
								LEFT JOIN pack p ON c.id_pack = p.id_pack
								LEFT JOIN ciudad u ON p.id_ciudad = u.id_ciudad
								LEFT JOIN pais i ON u.id_pais = i.id_pais
								LEFT JOIN packdet d ON p.id_pack = d.id_pack
								inner join usuarios usu on (c.id_usuario = usu.id_usuario)
								WHERE	c.id_cot = " . $id_cot;
						
$cot = $db1->SelectLimit ( $query_cot ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg () );
// echo $query_cot . "<hr>";die('aqui');

$titulo = $cot->Fields ( 'pac_nompo' );
$mon_nombre = $cot->Fields ( 'moneda' );
$query_destinos = "
SELECT *,
DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde,
DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta
FROM cotdes c INNER JOIN hotel h ON h.id_hotel = c.id_hotel
INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad
LEFT JOIN comuna o ON c.id_comuna = o.id_comuna
LEFT JOIN cat a ON c.id_cat = a.id_cat
WHERE id_cot = " . $id_cot . " AND c.id_cotdespadre=0 AND c.id_hotel = ".$id_hot;

if($cot->Fields('cot_estado')==0) $query_destinos.=" AND c.cd_estado = 0 ";

if ($cot -> Fields('cantdes') > "0")
	$query_destinos = sprintf("%s LIMIT %s", $query_destinos, $cot -> Fields('cantdes'));
// echo $query_destinos.'<hr>';

$destinos = $db1 -> SelectLimit($query_destinos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1 -> ErrorMsg());
$totalRows_destinos = $destinos->RecordCount();

$query_pasajeros = "
SELECT * FROM cotpas c
INNER JOIN pais p ON c.id_pais = p.id_pais
WHERE id_cot = " . $id_cot . " AND c.cp_estado = 0";
$pasajeros = $db1 -> SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1 -> ErrorMsg());
// echo $query_pasajeros . "<hr>";

//USUARIO CREADOR
$uC_sql="select*from usuarios where id_usuario=".$cot->Fields('id_usuario');
$uC = $db1->SelectLimit ( $uC_sql ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg ( "ERROR2" ) );

if($esVeciOnRequest){
$cuerpo = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VECI Turismo</title>
	
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	
<!-- hojas de estilo -->
<link rel="stylesheet" href="http://cts.distantis.com/veci/css/easy.css" media="screen, all" type="text/css" />
<link rel="stylesheet" href="http://cts.distantis.com/veci/css/easyprint.css" media="print" type="text/css" />
<link rel="stylesheet" href="http://cts.distantis.com/veci/css/screen-sm.css" media="screen, print, all" type="text/css" />
	
</head>
<ul> 
  <li>Reserva generada en VECI-ONLINE.</li>'; 
  //<li>No es necesario enviar un mail de confirmaci&oacute;n a VECI, Favor dirigirse a este <a href="http://hoteles.distantis.com/hot_produccion.php?id_cliente=3" target="_blank">link</a> e ingresar en la pesta&ntilde;a Informes, para ingresar el N&uacute;mero de Confirmaci&oacute;n. VECI-ONLINE informara autom&aacute;ticamente al operador dicho n&uacute;mero de confirmaci&oacute;n. </li> 
$cuerpo.= '</ul> 
<body>
<center>
<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%"><img src="http://cts.distantis.com/veci/images/logo-cts.png" alt="" width="211" height="70" /><br>
<center>RESERVA VECI-ONLINE</center></td>
<td width="74%"><table align="center" width="95%" class="programa">
<tr>
<th colspan="2" align="center" >Detalle</th>
</tr>
<tr>
<td align="left">&nbsp;'.$numpas.' :</td>
<td colspan="3" >'.$cot->Fields ( 'cot_numpas' ).'</td>
</tr>
<tr>
<td align="left">COT ID: </td>
<td colspan="3" >'.$cot->Fields ( 'id_cot' ).' - '.$tipoMailEncabezado.' '.$_POST ["num_" . $i].' '.$destinos->Fields('cd_numreserva').'</td>
</tr>		';

}elseif ($esOtsi) {

$cuerpo = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>OTSi Online</title>
	
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	
<!-- hojas de estilo -->
<link rel="stylesheet" href="http://otsi.distantis.com/otsi/css/easy.css" media="screen, all" type="text/css" />
<link rel="stylesheet" href="http://otsi.distantis.com/otsi/css/easyprint.css" media="print" type="text/css" />
<link rel="stylesheet" href="http://otsi.distantis.com/otsi/css/screen-sm.css" media="screen, print, all" type="text/css" />
	
</head>
<ul> 
  <li>Reserva generada en OTSi-ONLINE.</li> ';
  //<li>No es necesario enviar un mail de confirmaci&oacute;n a OTSi, Favor dirigirse a este <a href="http://hoteles.distantis.com/hot_produccion.php?id_cliente=3" target="_blank">link</a> e ingresar en la pesta&ntilde;a Informes, para ingresar el N&uacute;mero de Confirmaci&oacute;n. OTSi-ONLINE informara autom&aacute;ticamente al operador dicho n&uacute;mero de confirmaci&oacute;n. </li> 
$cuerpo.= '</ul> 
<body>
<center>
<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%"><img src="http://otsi.distantis.com/otsi/images/LOGOS_WHITE_OTSI.jpg" alt="" width="211" height="70" /><br>
<center>RESERVA OTSi-ONLINE</center></td>
<td width="74%"><table align="center" width="95%" class="programa">
<tr>
<th colspan="2" align="center" >Detalle</th>
</tr>
<tr>
<td align="left">&nbsp;'.$numpas.' :</td>
<td colspan="3" >'.$cot->Fields ( 'cot_numpas' ).'</td>
</tr>
<tr>
<td align="left">COT ID: </td>
<td colspan="3" >'.$cot->Fields ( 'id_cot' ).' - '.$tipoMailEncabezado.' '.$_POST ["num_" . $i].' '.$destinos->Fields('cd_numreserva').'</td>
</tr>		';
}elseif ($esTouravion) {

$cuerpo = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Turavion Online</title>
	
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	
<!-- hojas de estilo -->
<link rel="stylesheet" href="http://turavion.distantis.com/turavion/css/easy.css" media="screen, all" type="text/css" />
<link rel="stylesheet" href="http://turavion.distantis.com/turavion/css/easyprint.css" media="print" type="text/css" />
<link rel="stylesheet" href="http://turavion.distantis.com/turavion/css/screen-sm.css" media="screen, print, all" type="text/css" />
	
</head>
<ul> 
  <li>Reserva generada en Turavion-ONLINE.</li> ';
  //<li>No es necesario enviar un mail de confirmaci&oacute;n a Turavion, Favor dirigirse a este <a href="http://hoteles.distantis.com/hot_produccion.php?id_cliente=3" target="_blank">link</a> e ingresar en la pesta&ntilde;a Informes, para ingresar el N&uacute;mero de Confirmaci&oacute;n. Turavion-ONLINE informara autom&aacute;ticamente al operador dicho n&uacute;mero de confirmaci&oacute;n. </li> 
$cuerpo.= '</ul> 
<body>
<center>
<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%"><img src="http://turavion.distantis.com/turavion/images/logoturavionconfondo.JPG" alt="" width="233" height="160" /><br>
<center>RESERVA Turavion-ONLINE</center></td>
<td width="74%"><table align="center" width="95%" class="programa">
<tr>
<th colspan="2" align="center" >Detalle</th>
</tr>
<tr>
<td align="left">&nbsp;'.$numpas.' :</td>
<td colspan="3" >'.$cot->Fields ( 'cot_numpas' ).'</td>
</tr>
<tr>
<td align="left">COT ID: </td>
<td colspan="3" >'.$cot->Fields ( 'id_cot' ).' - '.$tipoMailEncabezado.' '.$_POST ["num_" . $i].' '.$destinos->Fields('cd_numreserva').'</td>
</tr>		';

}else{
$cuerpo = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>CTS Turismo</title>
	
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	
<!-- hojas de estilo -->
<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easy.css" media="screen, all" type="text/css" />
<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easyprint.css" media="print" type="text/css" />
	
<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/screen-sm.css" media="screen, print, all" type="text/css" />
	
</head>
<ul> 
  <li>Reserva generada en CTS-ONLINE.</li> ';
  //<li>No es necesario enviar un mail de confirmaci&oacute;n a CTS, Favor dirigirse a este <a href="http://hoteles.distantis.com/distantis/hot_produccion.php?id_cliente=3" target="_blank">link</a> e ingresar en la pesta�a Informes, para ingresar el N&uacute;mero de Confirmaci&oacute;n. CTS-ONLINE informara autom&aacute;ticamente al operador dicho n&uacute;mero de confirmaci&oacute;n. </li> 
$cuerpo.= '</ul> 
<body>
<center>
<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%"><img src="http://cts.distantis.com/distantis/images/logo-cts.png" alt="" width="211" height="70" /><br>
<center>RESERVA CTS-ONLINE</center></td>
<td width="74%"><table align="center" width="95%" class="programa">
<tr>
<th colspan="2" align="center" >Detalle</th>
</tr>
<tr>
<td align="left">&nbsp;'.$numpas.' :</td>
<td colspan="3" >'.$cot->Fields ( 'cot_numpas' ).'</td>
</tr>
<tr>
<td align="left">COT ID: </td>
<td colspan="3" >'.$cot->Fields ( 'id_cot' ).' - '.$tipoMailEncabezado.' '.$_POST ["num_" . $i].' '.$destinos->Fields('cd_numreserva').'</td>
</tr>		';
}
if(count($_SESSION['mod_programas']) !=0){
	$tpmod="";
	array_unique($_SESSION['mod_programas']);
	for ($i = 0; $i <count($_SESSION['mod_programas']) ; $i++) {
		if($_SESSION['mod_programas'][$i]==1 ) $tpmod.="MODIFICA PASAJEROS<br>";
		if($_SESSION['mod_programas'][$i]==2 ) $tpmod.="MODIFICA FECHAS<br>";
		if($_SESSION['mod_programas'][$i]==3 ) $tpmod.="MODIFICA HABITACION<br>";
		if($_SESSION['mod_programas'][$i]==4 ) $tpmod.="MODIFICA HOTEL<br>";
	}
	
	$cuerpo.='
	<tr>
		<td>Tipo Modificaci�n:</td>
		<td colspan="2">'.$tpmod.'</td>
	</tr>';
		
}

$cuerpo.='
		
<tr>
<td align="left">COT REF:</td>
<td colspan="3" >'.$cot->Fields('id_cotref').'</td>
</tr>
			<tr>
				<td align="left">Usuario Creador:</td>
				<td colspan="3" >'.$uC->Fields('usu_nombre').' '.$uC->Fields('usu_pat').'</td> 
			  </tr>';
if($id_prueba==1){
	$cuerpo.='<tr>
				
				<td colspan="4" align="center">E-MAIL PRUEBA, NO VALIDO</td>	
	</tr>';
	
}

$cuerpo.='
</table>
</td>
</tr>
</table>
<table>
<th colspan="2" align="center" >'.$detvuelo.' - '.$pasajero.'</th>
<tr valign="baseline">
<td width="174" align="left" nowrap="nowrap" >&nbsp;'.$vuelo.' :</td>
<td width="734" class="nombreusuario">'.$cot->Fields ( 'cot_numvuelo' ).'</td>
</tr>';
$z = 1;

while ( ! $pasajeros->EOF ) {
	$cuerpo .= '
	<tr valign="baseline">
	<td width="174" align="left" nowrap="nowrap" >&nbsp;'.$pasajero.' '.$z.'.</td>
	<td width="734" class="nombreusuario">'.$pasajeros->Fields ( 'cp_apellidos' ).', '.$pasajeros->Fields ( 'cp_nombres' ).' ('.$pasajeros->Fields ( 'cp_dni' ).')</td>
	</tr>';
	$z ++;
	$pasajeros->MoveNext ();
}
$pasajeros->MoveFirst ();

$cuerpo .= '<tr valign="baseline"></tr>
</table>';
if ($totalRows_destinos > 0) {
	while ( ! $destinos->EOF ) {
		$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = " . $destinos->Fields ( 'id_cotdes' );
		$hab = $db1->SelectLimit ( $query_hab ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg () );
		
		$cuerpo .= '<table width="100%" class="programa">
		<tbody>
		<tr>
		<th colspan="4" >'.$resumen.' '.$destinos->Fields ( 'ciu_nombre' ).'.</th>
		</tr>
		<tr valign="baseline">
		<td width="16%" align="left">'.$hotel_nom.' :</td>
		<td width="36%">'.$destinos->Fields ( 'hot_nombre' ).'</td>
		<td colspan="2">&nbsp;</td>
		</tr>
		<tr valign="baseline">
		<td align="left">'.$fecha1.' :</td>
		<td>'.$destinos->Fields ( 'cd_fecdesde' ).'</td>
		<td>'.$fecha2.' :</td>
		<td>'.$destinos->Fields ( 'cd_fechasta' ).'</td>
		</tr>
		<tr><td colspan="">'.$direccion.':</td><td colspan="">'.$destinos->Fields("hot_direccion").'</td></tr>
				
				<tr>
				<td colspan="4">
				';
		$query_valhab = "SELECT hd_sgl, hd_dbl, hd_tpl,
		DATEDIFF(cd_fechasta, cd_fecdesde) as pac_n, tt_nombre, count(*) as noches, th.th_nombre
		FROM cotdes c
		INNER JOIN hotocu o ON c.id_cotdes = o.id_cotdes
		INNER JOIN hotdet h ON o.id_hotdet = h.id_hotdet
		INNER JOIN tipotarifa t ON h.id_tipotarifa = t.id_tipotarifa
		INNER JOIN tipohabitacion th on h.id_tipohabitacion = th.id_tipohabitacion
		WHERE o.hc_mod=0  AND c.id_cotdes =  " . $destinos->Fields ( 'id_cotdes' ) ;
		if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0 ) $query_valhab.=" AND o.hc_estado = 0 AND c.cd_estado=0 ";
		$query_valhab.=" AND c.id_cotdespadre=0 and o.id_cot=$id_cot
		GROUP BY o.id_hotdet";
		//  		echo $query_valhab.'<hr>';
		$valhab = $db1->SelectLimit ( $query_valhab ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg () );
		if ($hab->Fields ( 'cd_hab1' ) > 0) {
				$habitacion = $hab->Fields ( 'cd_hab1' ) . " " . $sin . " - ";
		}
		if ($hab->Fields ( 'cd_hab2' ) > 0) {
						$habitacion .= $hab->Fields ( 'cd_hab2' ) . " " . $dob . " - ";
		}
		if ($hab->Fields ( 'cd_hab3' ) > 0) {
				$habitacion .= $hab->Fields ( 'cd_hab3' ) . " " . $tri . " - ";
		}
		if ($hab->Fields ( 'cd_hab4' ) > 0) {
			$habitacion .= $hab->Fields ( 'cd_hab4' ) . " " . $cua;
		}
		$cuerpo .= '
		<table width="100%" class="programa">
		<tr>
		<th colspan="5" >'.$tipohab.'</th>
				</tr>
				<tr valign="baseline">
		<td>Tipo Habitacion</td>
		<td>Tipo Tarifa</td>
		<td>Tarifa Habitacion</td>
		<td>Noches Totales</td>
		<td>Total</td>
		</tr>';
		
		$tothab1 = 0;
						$tothab2 = 0;
						$tothab3 = 0;
						$tothab4 = 0;
		
						$habhot1 = $hab->Fields ( 'cd_hab1' ) . " " . $sin;
				while ( ! $valhab->EOF ) {
		$glosaiva="";
					if($cot->Fields('id_area')==2){
						$glosaiva='<font size="1" color="red"> +iva</font>';
						$fiva=true;
					}
				if ($hab->Fields ( 'cd_hab1' ) > 0) {
					
				if($ski_week==1){
						$hab1 = (($valhab->Fields ( 'hd_sgl' ) * 1*0.8) * $destinos->Fields ( 'cd_hab1' )) * $valhab->Fields ( 'noches' );
						$val=round ( $valhab->Fields ( 'hd_sgl' )*0.8, 0 );
		}
		else{
					$hab1 = (($valhab->Fields ( 'hd_sgl' ) * 1) * $destinos->Fields ( 'cd_hab1' )) * $valhab->Fields ( 'noches' );
									$val=round ( $valhab->Fields ( 'hd_sgl' ), 0 );
		}
		// $tothab1 =
				// ($valhab->Fields('hd_sgl')*$valhab->Fields('pac_n'))*$hab->Fields('cd_hab1');
		$cuerpo .= '<tr valign="baseline">
		<td>'.$hab->Fields ( 'cd_hab1' ).' '.$sin.' - '.$valhab->Fields('th_nombre').'</td>
		<td>'.$valhab->Fields ( 'tt_nombre' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).' '.$glosaiva.'</td>
				<td>'.$valhab->Fields ( 'noches' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab1,1),1,'.',',')).' '.$glosaiva.'</td>
						</tr>';
				if($ski_week==1){
							
						$tothab1 += (($valhab->Fields ( 'hd_sgl' ) * 1*0.8) * $destinos->Fields ( 'cd_hab1' )) * $valhab->Fields ( 'noches' );
		}else{
					$tothab1 += (($valhab->Fields ( 'hd_sgl' ) * 1) * $destinos->Fields ( 'cd_hab1' )) * $valhab->Fields ( 'noches' );
		}
						}
							
						if ($hab->Fields ( 'cd_hab2' ) > 0) {
		
						if($ski_week==1){
						$hab2 = (($valhab->Fields ( 'hd_dbl' )*0.8 * 2) * $destinos->Fields ( 'cd_hab2' )) * $valhab->Fields ( 'noches' );
						$val=round ( $valhab->Fields ( 'hd_dbl' )*0.8 * 2, 0 );
						}else{
						$hab2 = (($valhab->Fields ( 'hd_dbl' ) * 2) * $destinos->Fields ( 'cd_hab2' )) * $valhab->Fields ( 'noches' );
						$val=round ( $valhab->Fields ( 'hd_dbl' ) * 2, 0 );
						}
		
		
						// $tothab2 =
						// ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab2');
						$cuerpo .= '<tr valign="baseline">
						<td>'.$hab->Fields ( 'cd_hab2' ).' '.$dob.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields ( 'tt_nombre' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).' '.$glosaiva.'</td>
						<td>'.$valhab->Fields ( 'noches' ).'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab2,1),1,'.',',')).' '.$glosaiva.'</td>
								</tr>';
										if($ski_week==1){
												$tothab2 += (($valhab->Fields ( 'hd_dbl' ) * 2*0.8) * $destinos->Fields ( 'cd_hab2' )) * $valhab->Fields ( 'noches' );
												}else{
														$tothab2 += (($valhab->Fields ( 'hd_dbl' ) * 2) * $destinos->Fields ( 'cd_hab2' )) * $valhab->Fields ( 'noches' );
				}
		
												}
																if ($hab->Fields ( 'cd_hab3' ) > 0) {
																if($ski_week==1){
																$hab3 = (($valhab->Fields ( 'hd_dbl' ) * 2*0.8) * $destinos->Fields ( 'cd_hab3' )) * $valhab->Fields ( 'noches' );
					$val=round ( $valhab->Fields ( 'hd_dbl' ) * 2*0.8, 0 );
																}
																else{
																	$hab3 = (($valhab->Fields ( 'hd_dbl' ) * 2) * $destinos->Fields ( 'cd_hab3' )) * $valhab->Fields ( 'noches' );
																	$val=round ( $valhab->Fields ( 'hd_dbl' ) * 2, 0 );
																}
		
																// $tothab3 =
																// ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab3');
																$cuerpo .= '<tr valign="baseline">
				<td>'.$hab->Fields ( 'cd_hab3' ).' '.$tri.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields ( 'tt_nombre' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).' '.$glosaiva.'</td>
				<td>'.$valhab->Fields ( 'noches' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab3,1),1,'.',',')).' '.$glosaiva.'</td>
				</tr>';
																if($ski_week==1){
																	$tothab3 += (($valhab->Fields ( 'hd_dbl' ) * 2*0.8) * $destinos->Fields ( 'cd_hab3' )) * $valhab->Fields ( 'noches' );
																}
																else{
																	$tothab3 += (($valhab->Fields ( 'hd_dbl' ) * 2) * $destinos->Fields ( 'cd_hab3' )) * $valhab->Fields ( 'noches' );
																}
		
												}
													
												if ($hab->Fields ( 'cd_hab4' ) > 0) {
													if($ski_week==1){
														$hab4 = (($valhab->Fields ( 'hd_tpl' ) * 3*0.8) * $destinos->Fields ( 'cd_hab4' )) * $valhab->Fields ( 'noches' );
														$val=round ( $valhab->Fields ( 'hd_tpl' ) * 3*0.8, 0 );
													}
													else{
														$hab4 = (($valhab->Fields ( 'hd_tpl' ) * 3) * $destinos->Fields ( 'cd_hab4' )) * $valhab->Fields ( 'noches' );
														$val=round ( $valhab->Fields ( 'hd_tpl' ) * 3, 0 );
													}
		
													// $tothab4 =
													// ($valhab->Fields('hd_tpl')*($valhab->Fields('pac_n')*3))*$hab->Fields('cd_hab4');
													$cuerpo .= '<tr valign="baseline">
				<td>'.$hab->Fields ( 'cd_hab4' ).' '.$cua.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields ( 'tt_nombre' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).' '.$glosaiva.'</td>
				<td>'.$valhab->Fields ( 'noches' ).'</td>
				<td>'.$mon_nombre.' $' .str_replace(".0","",number_format(round($hab4,1),1,'.',',')).' '.$glosaiva.'</td>
				</tr>';
													if($ski_week==1){
														$tothab4 += (($valhab->Fields ( 'hd_tpl' ) * 3*0.8) * $destinos->Fields ( 'cd_hab4' )) * $valhab->Fields ( 'noches' );
													}
													else{
														$tothab4 += (($valhab->Fields ( 'hd_tpl' ) * 3) * $destinos->Fields ( 'cd_hab4' )) * $valhab->Fields ( 'noches' );
													}
		
												}
												$valhab->MoveNext ();
						}
						$valhab->MoveFirst ();
		
						$total1234 = $tothab1 + $tothab2 + $tothab3 + $tothab4;
						$cuerpo .= '
		<tr valign="baseline">
		<td colspan="4">&nbsp;</td>
		<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($total1234,1),1,'.',',')).' '.$glosaiva.'</td>
		</tr>
		</table>';
		
		$cuerpo.='
				
		</td></tr></tbody>
		</table>';
		
		$nochead = ConsultaNoxAdi($db1,$id_cot,$destinos->Fields('id_cotdes'));
		$totalRows_nochead = $nochead->RecordCount();
		if($totalRows_nochead>0):
		$cuerpo.='
		
		<table><tbody>
		<tr valign="baseline">
			<td colspan="4">
				 <table width="100%" class="programa">
      <tr>
        <th colspan="8"> '.$nochesad.'</th>
      </tr>
      <tbody>
        <tr valign="baseline">
          <th width="141">'.$numpas.'</th>
          <th>'.$fecha1.'</th>
          <th>'.$fecha2.'</th>
          <th>'.$sin.'</th>
          <th>'.$dob.'</th>
          <th align="center">'.$tri.'></th>
          <th width="86" align="center">'.$cua.'</th>
          		<th>'.$valor.'</th>
        </tr>';
		
		$c = 1;
		while (!$nochead->EOF) {
			$query_valhab_adi = "SELECT hd_sgl, hd_dbl, hd_tpl,
		DATEDIFF(cd_fechasta, cd_fecdesde) as pac_n, tt_nombre, count(*) as noches, th.th_nombre
		FROM cotdes c
		INNER JOIN hotocu o ON c.id_cotdes = o.id_cotdes
		INNER JOIN hotdet h ON o.id_hotdet = h.id_hotdet
		INNER JOIN tipotarifa t ON h.id_tipotarifa = t.id_tipotarifa
		INNER JOIN tipohabitacion th on h.id_tipohabitacion = th.id_tipohabitacion
		WHERE o.hc_mod=0  AND c.id_cotdes =  " . $nochead->Fields ( 'id_cotdes' ) ;
			if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0 ) $query_valhab_adi.=" AND o.hc_estado = 0 AND c.cd_estado=0 ";
			$query_valhab_adi.=" AND c.id_cotdespadre!=0 and o.id_cot=$id_cot
			GROUP BY o.id_hotdet";
			//echo $query_valhab_adi;
			$valhab_adi = $db1->SelectLimit ( $query_valhab_adi ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg () );
			 
		
			$tothab1 = 0;
			$tothab2 = 0;
			$tothab3 = 0;
			$tothab4 = 0;
			 
			$habhot1 = $hab->Fields ( 'cd_hab1' ) . " " . $sin;
			while ( ! $valhab_adi->EOF ) {
		$glosaiva="";
				if($cot->Fields('id_area')==2){
					$glosaiva='<font size="1" color="red"> +iva</font>';
					$fiva=true;
				}
				if ($hab->Fields ( 'cd_hab1' ) > 0) {
		
				if($ski_week==1){
				$hab1 = (($valhab_adi->Fields ( 'hd_sgl' ) * 1*0.8) * $destinos->Fields ( 'cd_hab1' )) * $valhab_adi->Fields ( 'noches' );
					$val=round ( $valhab_adi->Fields ( 'hd_sgl' )*0.8, 0 );
					}
						else{
						$hab1 = (($valhab_adi->Fields ( 'hd_sgl' ) * 1) * $destinos->Fields ( 'cd_hab1' )) * $valhab_adi->Fields ( 'noches' );
						$val=round ( $valhab_adi->Fields ( 'hd_sgl' ), 0 );
					}
								// $tothab1 =
						// ($valhab->Fields('hd_sgl')*$valhab->Fields('pac_n'))*$hab->Fields('cd_hab1');
						 
						if($ski_week==1){
						 
						$tothab1 += (($valhab_adi->Fields ( 'hd_sgl' ) * 1*0.8) * $destinos->Fields ( 'cd_hab1' )) * $valhab_adi->Fields ( 'noches' );
						}else{
						$tothab1 += (($valhab_adi->Fields ( 'hd_sgl' ) * 1) * $destinos->Fields ( 'cd_hab1' )) * $valhab_adi->Fields ( 'noches' );
						}
						}
								 
								if ($hab->Fields ( 'cd_hab2' ) > 0) {
							 
							if($ski_week==1){
							$hab2 = (($valhab_adi->Fields ( 'hd_dbl' )*0.8 * 2) * $destinos->Fields ( 'cd_hab2' )) * $valhab_adi->Fields ( 'noches' );
							$val=round ( $valhab_adi->Fields ( 'hd_dbl' )*0.8 * 2, 0 );
							}else{
							$hab2 = (($valhab_adi->Fields ( 'hd_dbl' ) * 2) * $destinos->Fields ( 'cd_hab2' )) * $valhab_adi->Fields ( 'noches' );
							$val=round ( $valhab_adi->Fields ( 'hd_dbl' ) * 2, 0 );
							}
								 
								 
								// $tothab2 =
								// ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab2');
								 
								if($ski_week==1){
								$tothab2 += (($valhab_adi->Fields ( 'hd_dbl' ) * 2*0.8) * $destinos->Fields ( 'cd_hab2' )) * $valhab_adi->Fields ( 'noches' );
								}else{
								$tothab2 += (($valhab_adi->Fields ( 'hd_dbl' ) * 2) * $destinos->Fields ( 'cd_hab2' )) * $valhab_adi->Fields ( 'noches' );
								}
								 
								}
								if ($hab->Fields ( 'cd_hab3' ) > 0) {
								if($ski_week==1){
									$hab3 = (($valhab_adi->Fields ( 'hd_dbl' ) * 2*0.8) * $destinos->Fields ( 'cd_hab3' )) * $valhab_adi->Fields ( 'noches' );
									$val=round ( $valhab_adi->Fields ( 'hd_dbl' ) * 2*0.8, 0 );
								}
								else{
									$hab3 = (($valhab_adi->Fields ( 'hd_dbl' ) * 2) * $destinos->Fields ( 'cd_hab3' )) * $valhab_adi->Fields ( 'noches' );

									$val=round ( $valhab_adi->Fields ( 'hd_dbl' ) * 2, 0 );
								/*
								echo $valhab_adi->Fields ( 'hd_dbl' );
								echo "<br>";
								echo $destinos->Fields ( 'cd_hab3' );
								echo "<br>";	
								echo $valhab_adi->Fields ( 'noches' )								;								*/
								}
									 
									// $tothab3 =
									// ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab3');
		
											if($ski_week==1){
											$tothab3 += (($valhab->Fields ( 'hd_dbl' ) * 2*0.8) * $destinos->Fields ( 'cd_hab3' )) * $valhab->Fields ( 'noches' );
											}
												else{
												$tothab3 += (($valhab->Fields ( 'hd_dbl' ) * 2) * $destinos->Fields ( 'cd_hab3' )) * $valhab->Fields ( 'noches' );
											}
											 
											}
											 
											if ($hab->Fields ( 'cd_hab4' ) > 0) {
											if($ski_week==1){
											$hab4 = (($valhab->Fields ( 'hd_tpl' ) * 3*0.8) * $destinos->Fields ( 'cd_hab4' )) * $valhab->Fields ( 'noches' );
											$val=round ( $valhab->Fields ( 'hd_tpl' ) * 3*0.8, 0 );
											}
													else{
											$hab4 = (($valhab->Fields ( 'hd_tpl' ) * 3) * $destinos->Fields ( 'cd_hab4' )) * $valhab->Fields ( 'noches' );
											$val=round ( $valhab->Fields ( 'hd_tpl' ) * 3, 0 );
											}
											 
											// $tothab4 =
											// ($valhab->Fields('hd_tpl')*($valhab->Fields('pac_n')*3))*$hab->Fields('cd_hab4');
													 
													if($ski_week==1){
															$tothab4 += (($valhab_adi->Fields ( 'hd_tpl' ) * 3*0.8) * $destinos->Fields ( 'cd_hab4' )) * $valhab_adi->Fields ( 'noches' );
															}
															else{
															$tothab4 += (($valhab_adi->Fields ( 'hd_tpl' ) * 3) * $destinos->Fields ( 'cd_hab4' )) * $valhab_adi->Fields ( 'noches' );
							}
																	 
															}
															$valhab_adi->MoveNext ();
															}
															$valhab_adi->MoveFirst ();
															//echo $tothab1."|".$tothab2."|".$tothab3."|".$tothab4;
															 		$total1234 = $hab1 + $hab2 + $hab3 + $hab4;
															 		 
															 		$cuerpo.='<tr valign="baseline">
															<td align="center">'.$nochead->Fields('cd_numpas').'</td>
																	<td width="118" align="center">'.$nochead->Fields('cd_fecdesde1').'</td>
															<td width="108" align="center">'. $nochead->Fields('cd_fechasta1').'</td>
															<td width="88" align="center">'.$nochead->Fields('cd_hab1').'</td>
															<td width="102" align="center">'.$nochead->Fields('cd_hab2').'</td>
															<td width="160" align="center">'.$nochead->Fields('cd_hab3').'</td>
															<td align="center">'.$nochead->Fields('cd_hab4').'</td>
																	<td width="60">'.$mon_nombre.' $'.str_replace(".0","",number_format($total1234,1,'.',','));
				if($fiva==0){
					$cuerpo.='<font size="1" color="red"> +iva</font>';
				}
				$cuerpo.='</td>
															</tr>';
		
		
															$nochead->MoveNext();
															}
		
															$cuerpo.='</tbody>
															</table>
															</td>
																
															</tr></tbody>	</table>
															';
		
															endif;
		
		
		
		
		
		$hab1 = '';
		$hab2 = '';
		$hab3 = '';
		$hab4 = '';
		$destinos->MoveNext ();
	}$destinos->MoveFirst ();
}
$cuerpo .= '
	<table width="1000" class="programa">
                <tr>
                  <th colspan="4">'.$operador.'</th>
                </tr>
                <tr>
                  <td width="151" valign="top">N&deg; Correlativo :</td>
                  <td width="266">'.$cot->Fields('cot_correlativo').'</td>
                  <td width="115"></td>
                  <td width="368"></td>
                </tr>
              </table>
				<table width="100%" class="programa">
				  <tr>
					<th colspan="2">'.$observa.'</th>
				  </tr>
				  <tr>
					<td width="153" valign="top">'.$observa.' :</td>
					<td width="755">'.$cot->Fields('cot_obs').'</td>
				  </tr>
				</table>   
</center>


</body>
		
		
		
</html>
';
if($esVeciOnRequest){
$cuerpo .= "
<br>
<center><font size='1'>
Documento enviado desde VECI ONLINE 2011.
</center></font>";

$subject = "RESERVA VECI-ONLINE ID " . $id_cot;

$message_html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>RESERVA VECI-ONLINE</title>
<style type="text/css">
<!--
body {
font-family: Arial;
font-size: 12px;
background-color: #FFFFFF;
}
table {
font-size: 10px;
border:0;
}
th {
color:#FFFFFF;
background-color: #595A7F;
line-height: normal;
font-size: 10px;
}
tr {
color: #444444;
line-height: 15px;
}
td {
font-size: 14px;
}
.footer {	font-size: 9px;
}
-->
</style>
</head>
<body>'.$cuerpo.'</body>
</html>';

}elseif ($esOtsi) {

$cuerpo .= "
<br>
<center><font size='1'>
Documento enviado desde OTSI ONLINE 2011.
</center></font>";

$subject = "RESERVA OTSI-ONLINE ID " . $id_cot;

$message_html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>RESERVA OTSI-ONLINE</title>
<style type="text/css">
<!--
body {
font-family: Arial;
font-size: 12px;
background-color: #FFFFFF;
}
table {
font-size: 10px;
border:0;
}
th {
color:#FFFFFF;
background-color: #595A7F;
line-height: normal;
font-size: 10px;
}
tr {
color: #444444;
line-height: 15px;
}
td {
font-size: 14px;
}
.footer {	font-size: 9px;
}
-->
</style>
</head>
<body>'.$cuerpo.'</body>
</html>';

}elseif ($esTouravion) {

$cuerpo .= "
<br>
<center><font size='1'>
Documento enviado desde Turavion ONLINE 2011.
</center></font>";

$subject = "RESERVA Turavion-ONLINE ID " . $id_cot;

$message_html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>RESERVA Turavion-ONLINE</title>
<style type="text/css">
<!--
body {
font-family: Arial;
font-size: 12px;
background-color: #FFFFFF;
}
table {
font-size: 10px;
border:0;
}
th {
color:#FFFFFF;
background-color: #595A7F;
line-height: normal;
font-size: 10px;
}
tr {
color: #444444;
line-height: 15px;
}
td {
font-size: 14px;
}
.footer {	font-size: 9px;
}
-->
</style>
</head>
<body>'.$cuerpo.'</body>
</html>';

}else{
$cuerpo .= "
<br>
<center><font size='1'>
Documento enviado desde CTS ONLINE 2011.
</center></font>";

$subject = "RESERVA CTS-ONLINE ID " . $id_cot;
$message_html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>RESERVA CTS-ONLINE</title>
<style type="text/css">
<!--
body {
font-family: Arial;
font-size: 12px;
background-color: #FFFFFF;
}
table {
font-size: 10px;
border:0;
}
th {
color:#FFFFFF;
background-color: #595A7F;
line-height: normal;
font-size: 10px;
}
tr {
color: #444444;
line-height: 15px;
}
td {
font-size: 14px;
}
.footer {	font-size: 9px;
}
-->
</style>
</head>
<body>'.$cuerpo.'</body>
</html>';

}



//echo $message_html. "<hr>";die();
///////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////MAIL SIN CSS///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

if($esVeciOnRequest){
$cpo_sincss = '
<table width="75%" border="0" cellspacing="0" cellpadding="0">
<tr><td>* Reserva generada en VECI-ONLINE.</td></tr>';
//<tr><td>* No es necesario enviar un mail de confirmaci&oacute;n a VECI, Favor dirigirse a este <a href="http://hoteles.distantis.com/hot_produccion.php" target="_blank">link</a> e <br> ingresar en la pesta�a Informes, para ingresar el N&uacute;mero de Confirmaci&oacute;n. <br>CTS-ONLINE informara autom&aacute;ticamente al operador dicho n&uacute;mero de confirmaci&oacute;n.</td></tr>
$cpo_sincss.= '</table> ';
$cpo_pdf = 
'		<center>
<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%"><img src="http://cts.distantis.com/veci/images/logo-cts.png" alt="" width="211" height="70" /><br>
<center>RESERVA VECI-ONLINE</center></td>
<td width="74%"><table align="center" width="95%" class="programa">
<tr>
<th colspan="2" align="center" >Detalle</th>
</tr>
<tr>
<td align="left">&nbsp;'.$numpas.' :</td>
<td >'.$cot->Fields ( 'cot_numpas' ).'</td>
</tr>
<tr>
<td align="left">COT ID: </td>
<td  >'.$cot->Fields ( 'id_cot' ).' - '.$tipoMailEncabezado.' '.$_POST ["num_" . $i].'</td>
</tr>
<tr>
<td align="left">COT REF:</td>
<td  >'.$cot->Fields('id_cotref').'</td>
</tr>
			<tr>
				<td align="left">Usuario Creador:</td>
				<td colspan="3" >'.$uC->Fields('usu_nombre').' '.$uC->Fields('usu_pat').'</td>
			  </tr>';

}elseif ($esOtsi) {

$cpo_sincss = '
<table width="75%" border="0" cellspacing="0" cellpadding="0">
<tr><td>* Reserva generada en OTSi-ONLINE.</td></tr>';
//<tr><td>* No es necesario enviar un mail de confirmaci&oacute;n a OTSi, Favor dirigirse a este <a href="http://hoteles.distantis.com/hot_produccion.php" target="_blank">link</a> e <br> ingresar en la pesta�a Informes, para ingresar el N&uacute;mero de Confirmaci&oacute;n. <br>OTSi-ONLINE informara autom&aacute;ticamente al operador dicho n&uacute;mero de confirmaci&oacute;n.</td></tr>
$cpo_sincss.= '</table> ';
$cpo_pdf = 
'		<center>
<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%"><img src="http://otsi.distantis.com/otsi/images/LOGOS_WHITE_OTSI.jpg" alt="" width="211" height="70" /><br>
<center>RESERVA OTSi-ONLINE</center></td>
<td width="74%"><table align="center" width="95%" class="programa">
<tr>
<th colspan="2" align="center" >Detalle</th>
</tr>
<tr>
<td align="left">&nbsp;'.$numpas.' :</td>
<td >'.$cot->Fields ( 'cot_numpas' ).'</td>
</tr>
<tr>
<td align="left">COT ID: </td>
<td  >'.$cot->Fields ( 'id_cot' ).' - '.$tipoMailEncabezado.' '.$_POST ["num_" . $i].'</td>
</tr>
<tr>
<td align="left">COT REF:</td>
<td  >'.$cot->Fields('id_cotref').'</td>
</tr>
			<tr>
				<td align="left">Usuario Creador:</td>
				<td colspan="3" >'.$uC->Fields('usu_nombre').' '.$uC->Fields('usu_pat').'</td>
			  </tr>';

}elseif ($esTouravion) {

$cpo_sincss = '
<table width="75%" border="0" cellspacing="0" cellpadding="0">
<tr><td>* Reserva generada en Turavion-ONLINE.</td></tr>';
//<tr><td>* No es necesario enviar un mail de confirmaci&oacute;n a Turavion, Favor dirigirse a este <a href="http://hoteles.distantis.com/hot_produccion.php" target="_blank">link</a> e <br> ingresar en la pesta�a Informes, para ingresar el N&uacute;mero de Confirmaci&oacute;n. <br>Turavion-ONLINE informara autom&aacute;ticamente al operador dicho n&uacute;mero de confirmaci&oacute;n.</td></tr>
$cpo_sincss.= '</table> ';
$cpo_pdf = 
'		<center>
<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%"><img src="http://turavion.distantis.com/turavion/images/logoturavionconfondo.JPG" alt="" width="233" height="160" /><br>
<center>RESERVA Turavion-ONLINE</center></td>
<td width="74%"><table align="center" width="95%" class="programa">
<tr>
<th colspan="2" align="center" >Detalle</th>
</tr>
<tr>
<td align="left">&nbsp;'.$numpas.' :</td>
<td >'.$cot->Fields ( 'cot_numpas' ).'</td>
</tr>
<tr>
<td align="left">COT ID: </td>
<td  >'.$cot->Fields ( 'id_cot' ).' - '.$tipoMailEncabezado.' '.$_POST ["num_" . $i].'</td>
</tr>
<tr>
<td align="left">COT REF:</td>
<td  >'.$cot->Fields('id_cotref').'</td>
</tr>
			<tr>
				<td align="left">Usuario Creador:</td>
				<td colspan="3" >'.$uC->Fields('usu_nombre').' '.$uC->Fields('usu_pat').'</td>
			  </tr>';

}else{
$cpo_sincss = '
<table width="75%" border="0" cellspacing="0" cellpadding="0">
<tr><td>* Reserva generada en CTS-ONLINE.</td></tr>';
//<tr><td>* No es necesario enviar un mail de confirmaci&oacute;n a CTS, Favor dirigirse a este <a href="http://cts.distantis.com/distantis/hot_produccion.php" target="_blank">link</a> e <br> ingresar en la pesta�a Informes, para ingresar el N&uacute;mero de Confirmaci&oacute;n. <br>CTS-ONLINE informara autom&aacute;ticamente al operador dicho n&uacute;mero de confirmaci&oacute;n.</td></tr>
$cpo_sincss.= '</table> ';
$cpo_pdf = 
'		<center>
<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%"><img src="http://cts.distantis.com/distantis/images/logo-cts.png" alt="" width="211" height="70" /><br>
<center>RESERVA CTS-ONLINE</center></td>
<td width="74%"><table align="center" width="95%" class="programa">
<tr>
<th colspan="2" align="center" >Detalle</th>
</tr>
<tr>
<td align="left">&nbsp;'.$numpas.' :</td>
<td >'.$cot->Fields ( 'cot_numpas' ).'</td>
</tr>
<tr>
<td align="left">COT ID: </td>
<td  >'.$cot->Fields ( 'id_cot' ).' - '.$tipoMailEncabezado.' '.$_POST ["num_" . $i].'</td>
</tr>
<tr>
<td align="left">COT REF:</td>
<td  >'.$cot->Fields('id_cotref').'</td>
</tr>
			<tr>
				<td align="left">Usuario Creador:</td>
				<td colspan="3" >'.$uC->Fields('usu_nombre').' '.$uC->Fields('usu_pat').'</td>
			  </tr>';
}
if($id_prueba==1){
	$cpo_pdf.='<tr>

				<td colspan="4" align="center">E-MAIL PRUEBA, NO VALIDO</td>
	</tr>';

}

$cpo_pdf.='
</table>
</td>
</tr>
</table>
<table>
<th colspan="2" align="center" >'.$detvuelo.' - '.$pasajero.'</th>
<tr valign="baseline">
<td width="174" align="left" nowrap="nowrap" >&nbsp;'.$vuelo.' :</td>
<td width="734" class="nombreusuario">'.$cot->Fields ( 'cot_numvuelo' ).'</td>
</tr>';
$z = 1;

while ( ! $pasajeros->EOF ) {
	$cpo_pdf .= '
	<tr valign="baseline">
	<td width="174" align="left" nowrap="nowrap" >&nbsp;'.$pasajero.' '.$z.'.</td>
	<td width="734" class="nombreusuario">'.$pasajeros->Fields ( 'cp_apellidos' ).', '.$pasajeros->Fields ( 'cp_nombres' ).' ('.$pasajeros->Fields ( 'cp_dni' ).')</td>
	</tr>';
	$z ++;
	$pasajeros->MoveNext ();
}
$pasajeros->MoveFirst ();

$cpo_pdf .= '<tr valign="baseline"></tr>
</table>';
if ($totalRows_destinos > 0) {
	while ( ! $destinos->EOF ) {
		$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = " . $destinos->Fields ( 'id_cotdes' );
		$hab = $db1->SelectLimit ( $query_hab ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg () );

		$cpo_pdf .= '<table width="100%" class="programa">
		<tbody>
		<tr>
		<th colspan="4" >'.$resumen.' '.$destinos->Fields ( 'ciu_nombre' ).'.</th>
		</tr>
		<tr valign="baseline">
		<td width="16%" align="left">'.$hotel_nom.' :</td>
		<td width="36%">'.$destinos->Fields ( 'hot_nombre' ).'</td>
		<td colspan="2">&nbsp;</td>
		</tr>
		<tr valign="baseline">
		<td align="left">'.$fecha1.' :</td>
		<td>'.$destinos->Fields ( 'cd_fecdesde' ).'</td>
		<td>'.$fecha2.' :</td>
		<td>'.$destinos->Fields ( 'cd_fechasta' ).'</td>				
		</tr>
		<tr><td colspan="">'.$direccion.':</td><td colspan="">'.$destinos->Fields("hot_direccion").'</td></tr>
				';
		
		$nochead = ConsultaNoxAdi($db1,$id_cot,$destinos->Fields('id_cotdes'));
		$totalRows_nochead = $nochead->RecordCount();
		if($totalRows_nochead>0):
		$cpo_pdf.='
				

		<tr valign="baseline">
			<td colspan="4">
				 <table width="100%" class="programa">
      <tr>
        <th colspan="8"> '.$nochesad.'</th>
      </tr>
      <tbody>
        <tr valign="baseline">
          <th width="141">'.$numpas.'</th>
          <th>'.$fecha1.'</th>
          <th>'.$fecha2.'</th>
          <th>'.$sin.'</th>
          <th>'.$dob.'</th>
          <th align="center">'.$tri.'></th>
          <th width="86" align="center">'.$cua.'</th>
          <th>'.$valor.'</th>
        </tr>';
      
                $c = 1;
                while (!$nochead->EOF) {
    
        $cpo_pdf.='<tr valign="baseline">
          <td align="center">'.$nochead->Fields('cd_numpas').'</td>
          <td width="118" align="center">'.$nochead->Fields('cd_fecdesde1').'</td>
          <td width="108" align="center">'. $nochead->Fields('cd_fechasta1').'</td>
          <td width="88" align="center">'.$nochead->Fields('cd_hab1').'</td>
          <td width="102" align="center">'.$nochead->Fields('cd_hab2').'</td>
          <td width="160" align="center">'.$nochead->Fields('cd_hab3').'</td>
          <td align="center">'.$nochead->Fields('cd_hab4').'</td>
          <td>$'.$nochead->Fields('cd_valor').'</td>
        </tr>';
        
        
                    $nochead->MoveNext(); 
                    }
        
      $cpo_pdf.='</tbody>
    </table>
				</td>
					
		</tr>
				';
		
		endif;
		$cpo_pdf.='
				
		</tbody>
		</table>';
		$query_valhab = "SELECT hd_sgl, hd_dbl, hd_tpl,
		DATEDIFF(cd_fechasta, cd_fecdesde) as pac_n, tt_nombre, count(*) as noches, th.th_nombre
		FROM cotdes c
		INNER JOIN hotocu o ON c.id_cotdes = o.id_cotdes
		INNER JOIN hotdet h ON o.id_hotdet = h.id_hotdet
		INNER JOIN tipotarifa t ON h.id_tipotarifa = t.id_tipotarifa
		INNER JOIN tipohabitacion th on h.id_tipohabitacion = th.id_tipohabitacion
		WHERE o.hc_mod=0 AND c.id_cotdes =  " . $destinos->Fields ( 'id_cotdes' ) ;
		if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0 )  $query_valhab.=" AND o.hc_estado = 0 AND c.cd_estado=0 ";
		$query_valhab.=" AND c.id_cotdespadre=0 and o.id_cot=$id_cot
		GROUP BY o.id_hotdet";
// 		echo $query_valhab.'<hr>';
		$valhab = $db1->SelectLimit ( $query_valhab ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg () );
		if ($hab->Fields ( 'cd_hab1' ) > 0) {
			$habitacion = $hab->Fields ( 'cd_hab1' ) . " " . $sin . " - ";
		}
		if ($hab->Fields ( 'cd_hab2' ) > 0) {
			$habitacion .= $hab->Fields ( 'cd_hab2' ) . " " . $dob . " - ";
		}
		if ($hab->Fields ( 'cd_hab3' ) > 0) {
			$habitacion .= $hab->Fields ( 'cd_hab3' ) . " " . $tri . " - ";
		}
		if ($hab->Fields ( 'cd_hab4' ) > 0) {
			$habitacion .= $hab->Fields ( 'cd_hab4' ) . " " . $cua;
		}
		$cpo_pdf .= '
		<table width="100%" class="programa">
		<tr>
		<th colspan="8" >'.$tipohab.'</th>
		</tr>
		<tr valign="baseline">
		<td>Tipo Habitacion</td>
		<td>Tipo Tarifa</td>
		<td>Tarifa Habitacion</td>
		<td>Noches Totales</td>
		<td>Total</td>
		</tr>';
		
		$tothab1 = 0;
		$tothab2 = 0;
		$tothab3 = 0;
		$tothab4 = 0;

		$habhot1 = $hab->Fields ( 'cd_hab1' ) . " " . $sin;

		$total1234=0;
		while ( ! $valhab->EOF ) {
$glosaiva="";
			if($cot->Fields('id_area')==2){
				$glosaiva='<font size="1" color="red"> +iva</font>';
				$fiva=true;
			}
			if ($hab->Fields ( 'cd_hab1' ) > 0) {
				
				if($ski_week==1){
					$hab1 = (($valhab->Fields ( 'hd_sgl' ) * 1*0.8) * $destinos->Fields ( 'cd_hab1' )) * $valhab->Fields ( 'noches' );
					$val=round ( $valhab->Fields ( 'hd_sgl' )*0.8, 0 );
				}
				else{
					$hab1 = (($valhab->Fields ( 'hd_sgl' ) * 1) * $destinos->Fields ( 'cd_hab1' )) * $valhab->Fields ( 'noches' );
					$val=round ( $valhab->Fields ( 'hd_sgl' ), 0 );
				}
				
				// $tothab1 =
				// ($valhab->Fields('hd_sgl')*$valhab->Fields('pac_n'))*$hab->Fields('cd_hab1');
				$cpo_pdf .= '<tr valign="baseline">
				<td>'.$hab->Fields ( 'cd_hab1' ).' '.$sin.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields ( 'tt_nombre' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).' '.$glosaiva.'</td>
				<td>'.$valhab->Fields ( 'noches' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab1,1),1,'.',',')).' '.$glosaiva.'</td>
				</tr>';
				if($ski_week==1){
					
					$tothab1 += (($valhab->Fields ( 'hd_sgl' ) * 1*0.8) * $destinos->Fields ( 'cd_hab1' )) * $valhab->Fields ( 'noches' );
				}else{
					$tothab1 += (($valhab->Fields ( 'hd_sgl' ) * 1) * $destinos->Fields ( 'cd_hab1' )) * $valhab->Fields ( 'noches' );
				}
				
			}
				
			if ($hab->Fields ( 'cd_hab2' ) > 0) {
				if($ski_week==1){
					$hab2 = (($valhab->Fields ( 'hd_dbl' )*0.8 * 2) * $destinos->Fields ( 'cd_hab2' )) * $valhab->Fields ( 'noches' );
					$val=round ( $valhab->Fields ( 'hd_dbl' )*0.8 * 2, 0 );
				}else{
					$hab2 = (($valhab->Fields ( 'hd_dbl' ) * 2) * $destinos->Fields ( 'cd_hab2' )) * $valhab->Fields ( 'noches' );
					$val=round ( $valhab->Fields ( 'hd_dbl' ) * 2, 0 );
				}
				
				
				// $tothab2 =
				// ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab2');
				
				$cpo_pdf .= '<tr valign="baseline">
				<td>'.$hab->Fields ( 'cd_hab2' ).' '.$dob.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields ( 'tt_nombre' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).' '.$glosaiva.'</td>
				<td>'.$valhab->Fields ( 'noches' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab2,1),1,'.',',')).' '.$glosaiva.'</td>
				</tr>';
				if($ski_week==1){
					$tothab2 += (($valhab->Fields ( 'hd_dbl' ) * 2*0.8) * $destinos->Fields ( 'cd_hab2' )) * $valhab->Fields ( 'noches' );
				}else{
					$tothab2 += (($valhab->Fields ( 'hd_dbl' ) * 2) * $destinos->Fields ( 'cd_hab2' )) * $valhab->Fields ( 'noches' );
				}
				
			}
			if ($hab->Fields ( 'cd_hab3' ) > 0) {
				if($ski_week==1){
					$hab3 = (($valhab->Fields ( 'hd_dbl' ) * 2*0.8) * $destinos->Fields ( 'cd_hab3' )) * $valhab->Fields ( 'noches' );
					$val=round ( $valhab->Fields ( 'hd_dbl' ) * 2*0.8, 0 );
				}
				else{
					$hab3 = (($valhab->Fields ( 'hd_dbl' ) * 2) * $destinos->Fields ( 'cd_hab3' )) * $valhab->Fields ( 'noches' );
					$val=round ( $valhab->Fields ( 'hd_dbl' ) * 2, 0 );
					
				}
				
				// $tothab3 =
				// ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab3');
				$cpo_pdf .= '<tr valign="baseline">
				<td>'.$hab->Fields ( 'cd_hab3' ).' '.$tri.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields ( 'tt_nombre' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).' '.$glosaiva.'</td>
				<td>'.$valhab->Fields ( 'noches' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab3,1),1,'.',',')).' '.$glosaiva.'</td>
				</tr>';
				if($ski_week==1){
					$tothab3 += (($valhab->Fields ( 'hd_dbl' ) * 2*0.8) * $destinos->Fields ( 'cd_hab3' )) * $valhab->Fields ( 'noches' );
				}else{
					$tothab3 += (($valhab->Fields ( 'hd_dbl' ) * 2) * $destinos->Fields ( 'cd_hab3' )) * $valhab->Fields ( 'noches' );
				}
				
			}
				
			if ($hab->Fields ( 'cd_hab4' ) > 0) {
				
				if($ski_week==1){
					$hab4 = (($valhab->Fields ( 'hd_tpl' ) * 3*0.8) * $destinos->Fields ( 'cd_hab4' )) * $valhab->Fields ( 'noches' );
					$val=round ( $valhab->Fields ( 'hd_tpl' ) * 3*0.8, 0 );
				}
				else{
					$hab4 = (($valhab->Fields ( 'hd_tpl' ) * 3) * $destinos->Fields ( 'cd_hab4' )) * $valhab->Fields ( 'noches' );
					$val=round ( $valhab->Fields ( 'hd_tpl' ) * 3, 0 );
				}
				
				
				// $tothab4 =
				// ($valhab->Fields('hd_tpl')*($valhab->Fields('pac_n')*3))*$hab->Fields('cd_hab4');
				$cpo_pdf .= '<tr valign="baseline">
				<td>'.$hab->Fields ( 'cd_hab4' ).' '.$cua.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields ( 'tt_nombre' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).' '.$glosaiva.'</td>
				<td>'.$valhab->Fields ( 'noches' ).'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab4,1),1,'.',',')).' '.$glosaiva.'</td>
				</tr>';
				if($ski_week==1){
					$tothab4 += (($valhab->Fields ( 'hd_tpl' ) * 3*0.8) * $destinos->Fields ( 'cd_hab4' )) * $valhab->Fields ( 'noches' );
				}
				else{
					$tothab4 += (($valhab->Fields ( 'hd_tpl' ) * 3) * $destinos->Fields ( 'cd_hab4' )) * $valhab->Fields ( 'noches' );
				}
				
			}
			$valhab->MoveNext ();
		}
		$valhab->MoveFirst ();

		$total1234 = $tothab1 + $tothab2 + $tothab3 + $tothab4;
// 		echo $ski_week;die();
	
		$cpo_pdf .= '
		<tr valign="baseline">
		<td colspan="4">&nbsp;</td>
		<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($total1234,1),1,'.',',')).' '.$glosaiva.'</td>
		</tr>
		</table>';

		$hab1 = '';
		$hab2 = '';
		$hab3 = '';
		$hab4 = '';
		$destinos->MoveNext (); 
	}$destinos->MoveFirst ();
}
$cpo_pdf .= '
			<table width="1000" class="programa">
                <tr>
                  <th colspan="4">'.$operador.'</th>
                </tr>
                <tr>
                  <td width="151" valign="top">N&deg; Correlativo :</td>
                  <td width="266">'.$cot->Fields('cot_correlativo').'</td>
                  <td width="115"></td>
                  <td width="368"></td>
                </tr>
              </table>
				<table width="100%" class="programa">
				  <tr>
					<th colspan="2">'.$observa.'</th>
				  </tr>
				  <tr>
					<td width="153" valign="top">'.$observa.' :</td>
					<td width="755">'.$cot->Fields('cot_obs').'</td>
				  </tr>
				</table>   

</center>';

$cpo_sincss.=$cpo_pdf;

if($esVeciOnRequest){
$cpo_sincss.='<table width="75%" border="0" cellspacing="0" cellpadding="0">
<tr><td>* '.$confirma_veci1.'</td></tr>
<tr><td>* '.$confirma_veci2.'</td></tr>
<tr><td>* '.$confirma_veci3.'</td></tr>
</table>

';

}elseif ($esOtsi) {

$cpo_sincss.='<table width="75%" border="0" cellspacing="0" cellpadding="0">
<tr><td>* '.$confirma_otsi1.'</td></tr>
<tr><td>* '.$confirma_otsi2.'</td></tr>
<tr><td>* '.$confirma_otsi3.'</td></tr>
</table>

';
}elseif ($esTouravion) {

$cpo_sincss.='<table width="75%" border="0" cellspacing="0" cellpadding="0">
<tr><td>* '.$confirma_ta1.'</td></tr>
<tr><td>* '.$confirma_ta2.'</td></tr>
<tr><td>* '.$confirma_ta3.'</td></tr>
</table>

';	
}else{
$cpo_sincss.='<table width="75%" border="0" cellspacing="0" cellpadding="0">
<tr><td>* '.$confirma1.'</td></tr>
<tr><td>* '.$confirma2.'</td></tr>
<tr><td>* '.$confirma3.'</td></tr>
</table>

';
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////








if($esVeciOnRequest){
$message_alt = 'Si ud no puede ver este email por favor contactese con VECI ONLINE &copy; 2011-2013';
}elseif ($esOtsi) {
$message_alt = 'Si ud no puede ver este email por favor contactese con OTSi ONLINE &copy; 2011-2013';
}elseif ($esTouravion) {
$message_alt = 'Si ud no puede ver este email por favor contactese con TURAVION ONLINE &copy; 2011-2013';
}else{
$message_alt = 'Si ud no puede ver este email por favor contactese con CTS ONLINE &copy; 2011-2013';
}

//EMAIL DE ENVIO
$query_mails = "SELECT u.usu_mail FROM cotdes c INNER JOIN usuarios u ON u.id_empresa = c.id_hotel AND u.usu_enviar_correo=1 WHERE u.id_empresa=$id_hot AND  c.id_cot = ".$id_cot." AND u.usu_mail is not null AND u.usu_estado=0 and u.id_tipo=2
union
select ifnull(hot_email, 'juan@distantis.com') from hotel where id_hotel=$id_hot";
$mails = $db1->SelectLimit($query_mails) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
 unset($array_mails);
while (!$mails->EOF) {
	$array_mails[] = $mails->Fields('usu_mail');

	$mails->MoveNext();
}$mails->MoveFirst();
 
if(count($array_mails)>0)$mail_hotel = implode("; ",$array_mails);
// echo $mail_to;die();
//$mail_toop = 'dsalazar@vtsystems.cl';
// $mail_to = 'eugenio.allendes@vtsystems.cl; dsalazar@vtsystems.cl; gonzalo@distantis.com; matias@distantis.com; sguzman@vtsystems.cl';
if($esVeciOnRequest){
	$mail_to=$distantis_mail_veci;
}elseif ($esOtsi) {	
	$mail_to=$distantis_mail_otsi;
}elseif ($esTouravion) {	
	$mail_to=$distantis_mail;
}else{
	//Si es de CTS agrega estos mail. FMEJIAS 04/04/2014
	$mail_to=$distantis_mail;
	$mail_to.=";mreyes@ctsturismo.cl;francisco.mejias@ctsturismo.cl;respaldo.mail.cts@gmail.com";
}
// echo $mail_to;die();
// $mail_toop = 'eugenio.allendes@vtsystems.cl';

if(count($array_mails) > 0)$copy_to = $mail_hotel;
//buscamos usuario creador

// $mail_to .= ";" . $uC->Fields ( 'usu_mail' );

?>